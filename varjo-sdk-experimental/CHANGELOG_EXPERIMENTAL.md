# Varjo Experimental SDK Changelog

This file describes changes in Varjo Experimental SDK.

Please note that each Varjo Experimental SDK is guaranteed to work
only with the exact matching version of Varjo software stack release.
Forward compatiblity is not provided for any experimental features.

## 3.3.0

### Changed

- BlendControlMask view extension moved to public SDK Layers API.
  MaskingTool example is moved as well, but it still has depth test
  related experimental features that are only available when the
  example is built from Experimental SDK.
- Reworked 3D reconstruction API and MeshingExample

### Removed

- Session type API was removed as unnecessary. MaskingTool example is now
  modified to use normal session with higher priority to keep it on top.


## 3.2.0

### Added

- Blend control mask support through Layers API view extensions.
  See varjo_BlendControlMaskingMode and varjo_ViewExtensionBlendControl
  for details.

- Global video depth test mode and range settings through MR API.

- MaskingTool example to showcase how to use new blend control mask and
  global video depth test mode and range settings.


## 3.1.0

### Changed

- ImGui UI stuff moved from experimental to regular SDK


## 3.0.0

### Added

- New input layout (`varjo_ShaderInputLayout_VideoPostProcess_V2`)
  for VPT post-process shaders. V2 layout adds additional info needed for
  proper focus view rendering for use cases that depend on the focus view
  sizes and/or scale related to VR rendering and VPT context rendering.
  Please see the examples on how to use these.
- 3D reconstruction API. Examples will be added in the later releases.

### Removed

- Move foveation API from experimental to main API (with some changes);
  see `Varjo.h` for more information
- Removed V1 layout (`varjo_ShaderInputLayout_VideoPostProcess_V1`)
  for VPT post process shaders.
  Migration from V1 to V2:
    - Change constant: varjo_ShaderInputLayout_VideoPostProcess_V1
      to varjo_ShaderInputLayout_VideoPostProcess_V2.
    - Change shader input layout to correspond V2. New layout only
      adds data, so new values can be ignored if not needed.

### Changed

- Update license


## 2.6.0

### Changed

- Examples are compiled with C++17.


## 2.5.0

### Added

- ChromaKeyMaskTool example added.
- JSON library added.


## 2.4.0

### Added

- Experimental SDK introduced
- VideoPostProcessExample added to demostrate post process API usage.
