var searchData=
[
  ['nearz_105',['nearZ',['../_varjo__types__layers_8h.html#a0d796e06314a4456582ef14531cef74e',1,'varjo_ViewExtensionDepth::nearZ()'],['../_varjo__types__layers_8h.html#a1b11d6ab194dbad0f48f88b4a7ae8ac1',1,'varjo_ViewExtensionDepthTestRange::nearZ()']]],
  ['next_106',['next',['../_varjo__types_8h.html#ab8c1de8f0238373955a47077912ea4f8',1,'varjo_StructureExtension::next()'],['../_varjo__types_8h.html#ac29b6ba0d96f48eb61fb434acc155d9e',1,'varjo_VariableRateShadingConfig::next()'],['../_varjo__types__layers_8h.html#acc270bf3b9a1de6f0640a651ccb9b3e1',1,'varjo_ViewExtension::next()']]],
  ['normal_107',['normal',['../_varjo__types__mr__experimental_8h.html#a9f0d1294f5df9159cbca14cb805b023f',1,'varjo_VertexFormat']]],
  ['normals32f_108',['normals32f',['../_varjo__types__mr__experimental_8h.html#a0e224b3e9b77709ff32075539fab3169',1,'varjo_MeshVertexNormalArray']]],
  ['normalxy_109',['normalXY',['../_varjo__types__mr__experimental_8h.html#a08327cbf0e4d9547f7243fe8ae064f50',1,'varjo_PointCloudPoint']]],
  ['normalzcolorr_110',['normalZcolorR',['../_varjo__types__mr__experimental_8h.html#a76d29c376112b2583a699b0400117a48',1,'varjo_PointCloudPoint']]],
  ['numberoftextures_111',['numberOfTextures',['../_varjo__types__layers_8h.html#a7170f779f0d95a6bf4a89b0c8b68da22',1,'varjo_SwapChainConfig2']]]
];
