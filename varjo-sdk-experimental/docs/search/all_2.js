var searchData=
[
  ['cameracalibrationconstant_10',['cameraCalibrationConstant',['../_varjo__types__datastream_8h.html#a24b66db660ff70ce0587c7e98fdb5b2c',1,'varjo_DistortedColorFrameMetadata']]],
  ['capturetime_11',['captureTime',['../_varjo__types_8h.html#a5a618c82277f939b2aa14c97d254d69c',1,'varjo_Gaze::captureTime()'],['../_varjo__types_8h.html#adba83b414e2df04bf70b7f6b36a923b4',1,'varjo_EyeMeasurements::captureTime()']]],
  ['ccm_12',['ccm',['../_varjo__types__datastream_8h.html#abcaecbe7817534733789d4ac8e55ee87',1,'varjo_WBNormalizationData']]],
  ['changedpointcount_13',['changedPointCount',['../_varjo__types__mr__experimental_8h.html#ae71c853084e25bd9ab94a3db92983c1e',1,'varjo_PointCloudDeltaContent']]],
  ['changedpoints_14',['changedPoints',['../_varjo__types__mr__experimental_8h.html#a24f200d12cb6f6fd54be01d5b3236993',1,'varjo_PointCloudDeltaContent']]],
  ['channelflags_15',['channelFlags',['../_varjo__types__datastream_8h.html#a167d52c0db19265bda7a2329cc4ff723',1,'varjo_StreamConfig']]],
  ['channels_16',['channels',['../_varjo__types__datastream_8h.html#a61c8f5a0161d9358c7652eefaafe8b0b',1,'varjo_StreamFrame']]],
  ['chunkspermeter_17',['chunksPerMeter',['../_varjo__types__mr__experimental_8h.html#a9ee07802694236718efa086739efbe2e',1,'varjo_MeshReconstructionConfig']]],
  ['color_18',['color',['../_varjo__types__mr__experimental_8h.html#ac70451efcfaee89670414848f5baca1a',1,'varjo_VertexFormat']]],
  ['colorbg_19',['colorBG',['../_varjo__types__mr__experimental_8h.html#aeaabdd35f405f0591f070d0a8525b752',1,'varjo_PointCloudPoint']]],
  ['colors32f_20',['colors32f',['../_varjo__types__mr__experimental_8h.html#a26f1f769bbc2bb0631e72a18561b4815',1,'varjo_MeshVertexColorArray']]],
  ['computeblocksize_21',['computeBlockSize',['../_varjo__types__mr__experimental_8h.html#a52e349e5710b05263c1ae6111a512c5b',1,'varjo_ShaderParams_VideoPostProcess']]],
  ['confidence_22',['confidence',['../_varjo__types__world_8h.html#a59699c93da9867a46416f85b1aefe74a',1,'varjo_WorldPoseComponent']]],
  ['constantbuffersize_23',['constantBufferSize',['../_varjo__types__mr__experimental_8h.html#a24f71ce7a4f25e86bc1842befe8724db',1,'varjo_ShaderParams_VideoPostProcess']]]
];
