var searchData=
[
  ['baseformat_1168',['baseFormat',['../_varjo__types__mr__experimental_8h.html#a7ac2c03b62e1a3c09b9cb414c8dc313c',1,'varjo_GLTextureFormat']]],
  ['boolvalue_1169',['boolValue',['../_varjo__types__mr_8h.html#aa4d245ea8331ae5d03ca7f98f83f83b6',1,'varjo_CameraValue']]],
  ['bottom_1170',['bottom',['../_varjo__types_8h.html#a9c93bbb34a5b1fafb016d63784745c39',1,'varjo_FovTangents']]],
  ['buffertype_1171',['bufferType',['../_varjo__types__datastream_8h.html#a458c0ea6e650a86b3cebbc8b4ca5d20e',1,'varjo_StreamConfig']]],
  ['button_1172',['button',['../_varjo__events_8h.html#ab1882268fbbe765918b3acd42a505f31',1,'varjo_EventData']]],
  ['buttonid_1173',['buttonId',['../_varjo__events_8h.html#ad53a8d9101ed2a2c36c4df4aa72af8a5',1,'varjo_EventButton']]],
  ['bytesize_1174',['byteSize',['../_varjo__types__datastream_8h.html#a8b641df4421d72b23185c92301e614c8',1,'varjo_BufferMetadata']]]
];
