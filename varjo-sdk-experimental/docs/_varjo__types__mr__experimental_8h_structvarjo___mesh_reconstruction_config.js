var _varjo__types__mr__experimental_8h_structvarjo___mesh_reconstruction_config =
[
    [ "chunksPerMeter", "_varjo__types__mr__experimental_8h.html#a9ee07802694236718efa086739efbe2e", null ],
    [ "maxChunks", "_varjo__types__mr__experimental_8h.html#a1595e787b7c7d495fc1bd6cb8f8bddc0", null ],
    [ "maxTrianglesPerChunk", "_varjo__types__mr__experimental_8h.html#ae9e17bae76c6ce16d2ebfb6557716308", null ],
    [ "maxVerticesPerChunk", "_varjo__types__mr__experimental_8h.html#a41486f1a316cc147a8c66d9608358000", null ],
    [ "reserved", "_varjo__types__mr__experimental_8h.html#a1e147afdfae578717f101b0ca0158197", null ],
    [ "vertexFormat", "_varjo__types__mr__experimental_8h.html#a626f55f55e2ded515ffad0d475627aae", null ]
];