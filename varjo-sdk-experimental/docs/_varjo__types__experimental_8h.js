var _varjo__types__experimental_8h =
[
    [ "varjo_Quaternion", "_varjo__types__experimental_8h.html#structvarjo___quaternion", [
      [ "w", "_varjo__types__experimental_8h.html#af45412d094a3e28d0b1543ccaa0825a7", null ],
      [ "x", "_varjo__types__experimental_8h.html#a4f2723d470ccaeda03695ab385687703", null ],
      [ "y", "_varjo__types__experimental_8h.html#aa7e68a61a95fea22b8a13846a1336e01", null ],
      [ "z", "_varjo__types__experimental_8h.html#acbc00a44740e062242bd72d08c125162", null ]
    ] ],
    [ "varjo_LayerQuad", "_varjo__types__experimental_8h.html#structvarjo___layer_quad", [
      [ "eyeVisibility", "_varjo__types__experimental_8h.html#a2f292bf38dfec402f54d4cd53b251582", null ],
      [ "header", "_varjo__types__experimental_8h.html#af91ff54b7766bdefaf7890e91ef0afdd", null ],
      [ "height", "_varjo__types__experimental_8h.html#aa802e9b135c1f12aea78f262f0374c6b", null ],
      [ "orientation", "_varjo__types__experimental_8h.html#a5becf44b3bb1c169b7fdcc1c8622ef92", null ],
      [ "position", "_varjo__types__experimental_8h.html#a061f1b843e5673ebc153be06ae5fd48e", null ],
      [ "space", "_varjo__types__experimental_8h.html#a8b731f7a2dee9df3eeba99a012ad7ee7", null ],
      [ "viewport", "_varjo__types__experimental_8h.html#a0a6f0afdb4b454bdd6f4128981da3f81", null ],
      [ "width", "_varjo__types__experimental_8h.html#af93ce1252f81b4621ee095eb114d1654", null ]
    ] ],
    [ "varjo_EyeVisibility", "_varjo__types__experimental_8h.html#af5c1c4c0ed06faf119b4705410ef5c81", null ],
    [ "varjo_EyeVisibilityBoth", "_varjo__types__experimental_8h.html#ac4f9c7ccec925edbdba3b08263750b33", null ],
    [ "varjo_EyeVisibilityLeft", "_varjo__types__experimental_8h.html#ad149883fdc7c1cb2ca86b60b063d83df", null ],
    [ "varjo_EyeVisibilityRight", "_varjo__types__experimental_8h.html#ad115cedd6bbe2ba445d5f24c363430fd", null ],
    [ "varjo_LayerQuadType", "_varjo__types__experimental_8h.html#a7fe468a754852c7fd7931eac926161bf", null ]
];