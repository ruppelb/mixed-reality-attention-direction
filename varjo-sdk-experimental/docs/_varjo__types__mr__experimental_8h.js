var _varjo__types__mr__experimental_8h =
[
    [ "varjo_TextureConfig", "_varjo__types__mr__experimental_8h.html#structvarjo___texture_config", [
      [ "format", "_varjo__types__mr__experimental_8h.html#ab9dec721b64da24ebf021364554dbccd", null ],
      [ "height", "_varjo__types__mr__experimental_8h.html#a61ade5291750074eb0bc67e3e66e0f9a", null ],
      [ "width", "_varjo__types__mr__experimental_8h.html#a2138af1b1dce01aaef5ae7c37ceaa339", null ]
    ] ],
    [ "varjo_ShaderParams_VideoPostProcess", "_varjo__types__mr__experimental_8h.html#structvarjo___shader_params___video_post_process", [
      [ "computeBlockSize", "_varjo__types__mr__experimental_8h.html#a52e349e5710b05263c1ae6111a512c5b", null ],
      [ "constantBufferSize", "_varjo__types__mr__experimental_8h.html#a24f71ce7a4f25e86bc1842befe8724db", null ],
      [ "inputFlags", "_varjo__types__mr__experimental_8h.html#aa4a8eb488435cff0c930de7d7ba2684f", null ],
      [ "samplingMargin", "_varjo__types__mr__experimental_8h.html#a422ec2a48f58c7d355cfbb59626b636b", null ],
      [ "textures", "_varjo__types__mr__experimental_8h.html#ae99d1378fc24f4ada242d27578798ead", null ]
    ] ],
    [ "varjo_ShaderParams", "_varjo__types__mr__experimental_8h.html#unionvarjo___shader_params", [
      [ "reserved", "_varjo__types__mr__experimental_8h.html#aa8f9071de2a1966032c94c1c68e1d3c5", null ],
      [ "videoPostProcess", "_varjo__types__mr__experimental_8h.html#a6a6e2eafe58b01a19900b59b1eda33f9", null ]
    ] ],
    [ "varjo_ShaderConfig", "_varjo__types__mr__experimental_8h.html#structvarjo___shader_config", [
      [ "format", "_varjo__types__mr__experimental_8h.html#a771d3cccaf6f4faef399cc80c5716a3b", null ],
      [ "inputLayout", "_varjo__types__mr__experimental_8h.html#ab588529530d4d4a8e200abe44f62365a", null ],
      [ "params", "_varjo__types__mr__experimental_8h.html#ab025741c7ae87773d453f69d37e504db", null ]
    ] ],
    [ "varjo_GLTextureFormat", "_varjo__types__mr__experimental_8h.html#structvarjo___g_l_texture_format", [
      [ "baseFormat", "_varjo__types__mr__experimental_8h.html#a7ac2c03b62e1a3c09b9cb414c8dc313c", null ],
      [ "internalFormat", "_varjo__types__mr__experimental_8h.html#ad38754662f393fd97fae05ba37d6e943", null ]
    ] ],
    [ "varjo_ReconstructionConfig", "_varjo__types__mr__experimental_8h.html#structvarjo___reconstruction_config", [
      [ "framerate", "_varjo__types__mr__experimental_8h.html#a04ed2308aba80772e1cbaa7d31e8dbda", null ],
      [ "reserved", "_varjo__types__mr__experimental_8h.html#aec833d5086f6d5a29385a092a76ab816", null ]
    ] ],
    [ "varjo_PointCloudPoint", "_varjo__types__mr__experimental_8h.html#structvarjo___point_cloud_point", [
      [ "colorBG", "_varjo__types__mr__experimental_8h.html#aeaabdd35f405f0591f070d0a8525b752", null ],
      [ "indexConfidence", "_varjo__types__mr__experimental_8h.html#aa5b8a92dd79b7502ef0547c48ea8dfc1", null ],
      [ "normalXY", "_varjo__types__mr__experimental_8h.html#a08327cbf0e4d9547f7243fe8ae064f50", null ],
      [ "normalZcolorR", "_varjo__types__mr__experimental_8h.html#a76d29c376112b2583a699b0400117a48", null ],
      [ "positionXY", "_varjo__types__mr__experimental_8h.html#a2b35a43ef7f1339b02fcc61b447c61e0", null ],
      [ "positionZradius", "_varjo__types__mr__experimental_8h.html#a9950a7a3024a175c2455caeb6f3deb9a", null ]
    ] ],
    [ "varjo_PointCloudSnapshotContent", "_varjo__types__mr__experimental_8h.html#structvarjo___point_cloud_snapshot_content", [
      [ "pointCount", "_varjo__types__mr__experimental_8h.html#a9138e9342ed3579ae255c79464950cda", null ],
      [ "points", "_varjo__types__mr__experimental_8h.html#a569d63e4733d55e88a9f3c647d27535a", null ],
      [ "timestamp", "_varjo__types__mr__experimental_8h.html#acd80e70f797ab7620f1ba747ec4fccdf", null ]
    ] ],
    [ "varjo_PointCloudDeltaContent", "_varjo__types__mr__experimental_8h.html#structvarjo___point_cloud_delta_content", [
      [ "changedPointCount", "_varjo__types__mr__experimental_8h.html#ae71c853084e25bd9ab94a3db92983c1e", null ],
      [ "changedPoints", "_varjo__types__mr__experimental_8h.html#a24f200d12cb6f6fd54be01d5b3236993", null ],
      [ "maxSurfelIndex", "_varjo__types__mr__experimental_8h.html#a648a52252807d38ea98f580bcbe6e3af", null ],
      [ "removedPointCount", "_varjo__types__mr__experimental_8h.html#a864185e5bd87f2fa675394ca164a05dc", null ],
      [ "removedPointIds", "_varjo__types__mr__experimental_8h.html#a163038006e51597e677d8080c1455868", null ],
      [ "timestamp", "_varjo__types__mr__experimental_8h.html#a5ffa2631ca5fa1a1811f0662af135256", null ]
    ] ],
    [ "varjo_VertexFormat", "_varjo__types__mr__experimental_8h.html#structvarjo___vertex_format", [
      [ "color", "_varjo__types__mr__experimental_8h.html#ac70451efcfaee89670414848f5baca1a", null ],
      [ "normal", "_varjo__types__mr__experimental_8h.html#a9f0d1294f5df9159cbca14cb805b023f", null ],
      [ "position", "_varjo__types__mr__experimental_8h.html#ab1f3cf204168fa18dfc1dcef55dcc5a4", null ],
      [ "reserved", "_varjo__types__mr__experimental_8h.html#ad6f08ed0a4df3ad4af89df8080601d14", null ]
    ] ],
    [ "varjo_MeshReconstructionConfig", "_varjo__types__mr__experimental_8h.html#structvarjo___mesh_reconstruction_config", [
      [ "chunksPerMeter", "_varjo__types__mr__experimental_8h.html#a9ee07802694236718efa086739efbe2e", null ],
      [ "maxChunks", "_varjo__types__mr__experimental_8h.html#a1595e787b7c7d495fc1bd6cb8f8bddc0", null ],
      [ "maxTrianglesPerChunk", "_varjo__types__mr__experimental_8h.html#ae9e17bae76c6ce16d2ebfb6557716308", null ],
      [ "maxVerticesPerChunk", "_varjo__types__mr__experimental_8h.html#a41486f1a316cc147a8c66d9608358000", null ],
      [ "reserved", "_varjo__types__mr__experimental_8h.html#a1e147afdfae578717f101b0ca0158197", null ],
      [ "vertexFormat", "_varjo__types__mr__experimental_8h.html#a626f55f55e2ded515ffad0d475627aae", null ]
    ] ],
    [ "varjo_MeshChunkDescription", "_varjo__types__mr__experimental_8h.html#structvarjo___mesh_chunk_description", [
      [ "position", "_varjo__types__mr__experimental_8h.html#a0704573af3c30e27094fe53b1c2bd401", null ],
      [ "reserved", "_varjo__types__mr__experimental_8h.html#a4a5acbcbb1835bb088fb4fa8e00f4743", null ],
      [ "triangleCount", "_varjo__types__mr__experimental_8h.html#a55c477ed3d6f06e9d0b0ce36faf3a8b6", null ],
      [ "updateTimestamp", "_varjo__types__mr__experimental_8h.html#ac77f6e63c13b59e9acf1a69b33939cfb", null ],
      [ "vertexCount", "_varjo__types__mr__experimental_8h.html#a07224e0a6c0d05be348d053e4683a3ae", null ]
    ] ],
    [ "varjo_MeshVertexPositionArray", "_varjo__types__mr__experimental_8h.html#unionvarjo___mesh_vertex_position_array", [
      [ "positions32f", "_varjo__types__mr__experimental_8h.html#a6a560825c588aaa4fc0737e6797e2b9f", null ]
    ] ],
    [ "varjo_MeshVertexColorArray", "_varjo__types__mr__experimental_8h.html#unionvarjo___mesh_vertex_color_array", [
      [ "colors32f", "_varjo__types__mr__experimental_8h.html#a26f1f769bbc2bb0631e72a18561b4815", null ]
    ] ],
    [ "varjo_MeshVertexNormalArray", "_varjo__types__mr__experimental_8h.html#unionvarjo___mesh_vertex_normal_array", [
      [ "normals32f", "_varjo__types__mr__experimental_8h.html#a0e224b3e9b77709ff32075539fab3169", null ]
    ] ],
    [ "varjo_MeshChunkContent", "_varjo__types__mr__experimental_8h.html#structvarjo___mesh_chunk_content", [
      [ "description", "_varjo__types__mr__experimental_8h.html#a176788abd28b0bb62c9f348209515708", null ],
      [ "reserved", "_varjo__types__mr__experimental_8h.html#acca06e9544c858e21b1e84ef55c79306", null ],
      [ "triangleIndices", "_varjo__types__mr__experimental_8h.html#a6b04eea7a472f4ac77b014410f54c2d4", null ],
      [ "vertexColors", "_varjo__types__mr__experimental_8h.html#a25c9ce18dd77db8f85c40748938ef58f", null ],
      [ "vertexNormals", "_varjo__types__mr__experimental_8h.html#a314a86e0f3f6e71b1c21c2c45a85e0ef", null ],
      [ "vertexPositions", "_varjo__types__mr__experimental_8h.html#a80b668aeb1f16982f7c8dbc366337791", null ]
    ] ],
    [ "varjo_ChunkContentsBufferId", "_varjo__types__mr__experimental_8h.html#a718ed344f591b794315ffb605712ba81", null ],
    [ "varjo_DXGITextureFormat", "_varjo__types__mr__experimental_8h.html#a17bb2b1f30c945a5b2106520b03b97a5", null ],
    [ "varjo_MeshChunkContentsBufferId", "_varjo__types__mr__experimental_8h.html#a858386e931d4bec9e005bf0f55d1c2d6", null ],
    [ "varjo_PointCloudSnapshotId", "_varjo__types__mr__experimental_8h.html#a5485c4cf3bbfe2d4c4f1389a37a3f667", null ],
    [ "varjo_PointCloudSnapshotStatus", "_varjo__types__mr__experimental_8h.html#ab8944080af16b5f0d12257852ad2094e", null ],
    [ "varjo_ShaderFlags_VideoPostProcess", "_varjo__types__mr__experimental_8h.html#a485bde8ccd470fd0d1bb1c3b9a0b86c1", null ],
    [ "varjo_ShaderFormat", "_varjo__types__mr__experimental_8h.html#a103e19337e2abb3fc80a6063a26ac122", null ],
    [ "varjo_ShaderInputLayout", "_varjo__types__mr__experimental_8h.html#a14b9f07ed09840999b0c0de513960fb8", null ],
    [ "varjo_ShaderType", "_varjo__types__mr__experimental_8h.html#a87c4733dcf0e5e55bf3fbb7b3c1dbf30", null ],
    [ "varjo_VertexAttribute", "_varjo__types__mr__experimental_8h.html#ac9200c8109f9b0cfe312d8bee31aefc1", null ],
    [ "varjo_VideoDepthTestBehavior", "_varjo__types__mr__experimental_8h.html#a16b9efcea85d25766afb3b821658028d", null ],
    [ "varjo_VideoDepthTestMode", "_varjo__types__mr__experimental_8h.html#aa9d6ec0e9576f5731d9514042efaa044", null ],
    [ "varjo_ChunkContentsBufferId_Invalid", "_varjo__types__mr__experimental_8h.html#a112f0d9fc8f939f374242b828b2baa34", null ],
    [ "varjo_Error_ChunkContentsBufferAlreadyLocked", "_varjo__types__mr__experimental_8h.html#a98220b6eccb0574b44d98ccf0dae10de", null ],
    [ "varjo_Error_ChunkContentsBufferInvalidId", "_varjo__types__mr__experimental_8h.html#af8fd145182f016d46b02e5c0ffec3499", null ],
    [ "varjo_Error_ChunkContentsBufferNotLocked", "_varjo__types__mr__experimental_8h.html#a1984193ff8e0c0ab696a1b71353bfb42", null ],
    [ "varjo_Error_InvalidComputeBlockSize", "_varjo__types__mr__experimental_8h.html#a76e2510635573f098a1d598f2942f272", null ],
    [ "varjo_Error_InvalidConstantBuffer", "_varjo__types__mr__experimental_8h.html#a7276b2900379049543377f158a7762c5", null ],
    [ "varjo_Error_InvalidConstantBufferSize", "_varjo__types__mr__experimental_8h.html#ae073010dd7ccec2e9953a97207d3f562", null ],
    [ "varjo_Error_InvalidIndexCount", "_varjo__types__mr__experimental_8h.html#a95956ff39e15b1bae653e7a4847b2c48", null ],
    [ "varjo_Error_InvalidInputLayout", "_varjo__types__mr__experimental_8h.html#aceda701085ae7d83be92db4be6bb67db", null ],
    [ "varjo_Error_InvalidSamplingMargin", "_varjo__types__mr__experimental_8h.html#ace8ed339375f3af5c11bdc0503c52bae", null ],
    [ "varjo_Error_InvalidShader", "_varjo__types__mr__experimental_8h.html#af2f9f0f2e4da11fc02ce28b5f8483324", null ],
    [ "varjo_Error_InvalidShaderFlags", "_varjo__types__mr__experimental_8h.html#a1d682adbf6893c5ae696976ce1f79600", null ],
    [ "varjo_Error_InvalidShaderFormat", "_varjo__types__mr__experimental_8h.html#a4b5d15f22f833cc46df7933e07575df8", null ],
    [ "varjo_Error_InvalidShaderSize", "_varjo__types__mr__experimental_8h.html#af3dd20cc7648ca09a95daee30cc6d989", null ],
    [ "varjo_Error_InvalidShaderType", "_varjo__types__mr__experimental_8h.html#aa3f89b27702c8489031d93fde330c2b6", null ],
    [ "varjo_Error_InvalidTextureDimensions", "_varjo__types__mr__experimental_8h.html#a6970eb6efd52e40c52bdde93f0af349f", null ],
    [ "varjo_Error_InvalidTextureFormat", "_varjo__types__mr__experimental_8h.html#aa0153b3230fe5ceb9278c59049093522", null ],
    [ "varjo_Error_InvalidTextureIndex", "_varjo__types__mr__experimental_8h.html#a701feac87cb930e144e1423d0a16ebd7", null ],
    [ "varjo_Error_InvalidVideoDepthTestMode", "_varjo__types__mr__experimental_8h.html#a578f78d23874f76f49ca6f628ca8ba34", null ],
    [ "varjo_Error_InvalidVideoDepthTestRange", "_varjo__types__mr__experimental_8h.html#af0e29343f4089b531a5e319a6f935d84", null ],
    [ "varjo_Error_PointCloudSnapshotInvalidId", "_varjo__types__mr__experimental_8h.html#af4731907d89306fac9e81a88a336d264", null ],
    [ "varjo_Error_RenderAPINotInitialized", "_varjo__types__mr__experimental_8h.html#a83177690f323a9356a06afd50a9105bf", null ],
    [ "varjo_Error_TextureLockFailed", "_varjo__types__mr__experimental_8h.html#a80b9c1f1d4a963bb259540a6cb314b3d", null ],
    [ "varjo_Error_TextureNotAcquired", "_varjo__types__mr__experimental_8h.html#a20f0c612a121417fa4239cc05712f60a", null ],
    [ "varjo_Error_TexturesLocked", "_varjo__types__mr__experimental_8h.html#a737b46d37bf37311514c6a63ac12592f", null ],
    [ "varjo_LockType_VideoDepthTest", "_varjo__types__mr__experimental_8h.html#ab0a83983eb1a33adb346ecf652da9737", null ],
    [ "varjo_LockType_VideoPostProcessShader", "_varjo__types__mr__experimental_8h.html#acb5642b0b3c439fc25ece5ca7994a3a2", null ],
    [ "varjo_MeshChunkContentsBufferId_Invalid", "_varjo__types__mr__experimental_8h.html#ab38ec5cf1b1e2afafd8358730fe12ff6", null ],
    [ "varjo_PointCloudSnapshotId_Invalid", "_varjo__types__mr__experimental_8h.html#abc991a04ebac9a50b8eb6e28d9d9c8dc", null ],
    [ "varjo_PointCloudSnapshotStatus_Invalid", "_varjo__types__mr__experimental_8h.html#aa3894f6c91fb2b28c435a4f4f27acea4", null ],
    [ "varjo_PointCloudSnapshotStatus_Pending", "_varjo__types__mr__experimental_8h.html#a4261f4637cca449f5e9da80019b29b5d", null ],
    [ "varjo_PointCloudSnapshotStatus_Ready", "_varjo__types__mr__experimental_8h.html#a7d27f7de9b8430cca76c560e4aee098b", null ],
    [ "varjo_PropertyKey_ReconstructionAvailable", "_varjo__types__mr__experimental_8h.html#ac16383858baa748fbb7c0e876349f9df", null ],
    [ "varjo_ShaderFlag_VideoPostProcess_None", "_varjo__types__mr__experimental_8h.html#a23e0768618dc0665004702273057e76e", null ],
    [ "varjo_ShaderFormat_DxComputeBlob", "_varjo__types__mr__experimental_8h.html#a950483f71eb5e7ac77e31578aa316bfe", null ],
    [ "varjo_ShaderFormat_None", "_varjo__types__mr__experimental_8h.html#a58656f3b870c6778bcdedce566922864", null ],
    [ "varjo_ShaderInputLayout_VideoPostProcess_V2", "_varjo__types__mr__experimental_8h.html#a754e29c7d5555b88439e62dacd33065e", null ],
    [ "varjo_ShaderType_VideoPostProcess", "_varjo__types__mr__experimental_8h.html#a025ed3c2604c0e3e4def9cc625a7246c", null ],
    [ "varjo_VertexAttribute_float32", "_varjo__types__mr__experimental_8h.html#a6170e647e1c190e4b145afcadafed7e9", null ],
    [ "varjo_VideoDepthTestBehavior_CombineRanges", "_varjo__types__mr__experimental_8h.html#a4b9c2fe0b1cea5e35cb33a00d6af14db", null ],
    [ "varjo_VideoDepthTestBehavior_PreferLayerRange", "_varjo__types__mr__experimental_8h.html#a209b784fdf3dd3f6aec18a4c117071aa", null ],
    [ "varjo_VideoDepthTestBehavior_PreferVideoRange", "_varjo__types__mr__experimental_8h.html#a7b248cd6f2564051e349588d77681bf5", null ],
    [ "varjo_VideoDepthTestMode_ForcedRange", "_varjo__types__mr__experimental_8h.html#a7b55fb9222918cd2c8fd88ea05f7b7d6", null ],
    [ "varjo_VideoDepthTestMode_Full", "_varjo__types__mr__experimental_8h.html#a8110d8527895f8717aa37a1d1322c64c", null ],
    [ "varjo_VideoDepthTestMode_LimitedRange", "_varjo__types__mr__experimental_8h.html#a46d680d39675dc3e53e6bf49db4d1f14", null ]
];