var _varjo__types__experimental_8h_structvarjo___layer_quad =
[
    [ "eyeVisibility", "_varjo__types__experimental_8h.html#a2f292bf38dfec402f54d4cd53b251582", null ],
    [ "header", "_varjo__types__experimental_8h.html#af91ff54b7766bdefaf7890e91ef0afdd", null ],
    [ "height", "_varjo__types__experimental_8h.html#aa802e9b135c1f12aea78f262f0374c6b", null ],
    [ "orientation", "_varjo__types__experimental_8h.html#a5becf44b3bb1c169b7fdcc1c8622ef92", null ],
    [ "position", "_varjo__types__experimental_8h.html#a061f1b843e5673ebc153be06ae5fd48e", null ],
    [ "space", "_varjo__types__experimental_8h.html#a8b731f7a2dee9df3eeba99a012ad7ee7", null ],
    [ "viewport", "_varjo__types__experimental_8h.html#a0a6f0afdb4b454bdd6f4128981da3f81", null ],
    [ "width", "_varjo__types__experimental_8h.html#af93ce1252f81b4621ee095eb114d1654", null ]
];