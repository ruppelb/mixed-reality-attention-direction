var _varjo__types__mr__experimental_8h_structvarjo___point_cloud_delta_content =
[
    [ "changedPointCount", "_varjo__types__mr__experimental_8h.html#ae71c853084e25bd9ab94a3db92983c1e", null ],
    [ "changedPoints", "_varjo__types__mr__experimental_8h.html#a24f200d12cb6f6fd54be01d5b3236993", null ],
    [ "maxSurfelIndex", "_varjo__types__mr__experimental_8h.html#a648a52252807d38ea98f580bcbe6e3af", null ],
    [ "removedPointCount", "_varjo__types__mr__experimental_8h.html#a864185e5bd87f2fa675394ca164a05dc", null ],
    [ "removedPointIds", "_varjo__types__mr__experimental_8h.html#a163038006e51597e677d8080c1455868", null ],
    [ "timestamp", "_varjo__types__mr__experimental_8h.html#a5ffa2631ca5fa1a1811f0662af135256", null ]
];