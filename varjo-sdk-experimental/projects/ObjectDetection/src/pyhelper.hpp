#ifndef PYHELPER_HPP
#define PYHELPER_HPP
#pragma once

#include <Python.h>
#include <stdexcept>
#include <vector>

/**
 * Helper class for using python.h in C++
 * 
 * Automatically deinitializes python when instance gets out of scope.
 * 
 */
class CPyInstance
{
public:
	CPyInstance()
	{
		Py_Initialize();
	}

	~CPyInstance()
	{
		Py_Finalize();
	}
};

/**
 * Converts a uint8_t vector to a python compatible list.
 * 
 * @param data - uint8_t vector
 * 
 * @return A PyObject containing the vector as python compatible list
 */ 
static PyObject* vectorToList(const std::vector<uint8_t>& data) {
	PyObject* listObj = PyList_New(data.size());
	if (!listObj) throw std::logic_error("Unable to allocate memory for Python list");
	for (unsigned int i = 0; i < data.size(); i++) {
		PyObject* num = PyLong_FromLong((double)data[i]);
		if (!num) {
			Py_DECREF(listObj);
			throw std::logic_error("Unable to allocate memory for Python list");
		}
		PyList_SET_ITEM(listObj, i, num);
	}
	return listObj;
}

#endif
