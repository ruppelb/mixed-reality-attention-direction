

#pragma once

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include "pyhelper.hpp"

#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/sync/named_semaphore.hpp>
#include <boost/interprocess/containers/string.hpp>
#include <thread>
#include <mutex>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_transform_2d.hpp>
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "glm/ext.hpp"


#define MRSHAREDMEMORY "MRSharedMemory"
#define MRMUTEXSEMAPHORE "MRMutexSemaphore"
#define MREMPTYSEMAPHORE "MREmptySemaphore"
#define MRSTOREDSEMAPHORE "MRStoredSemaphore"

#define INSTRUMENTSHAREDMEMORY "InstrumentSharedMemory"
#define INSTRUMENTMUTEXSEMAPHORE "InstrumentMutexSemaphore"
#define INSTRUMENTEMPTYSEMAPHORE "InstrumentEmptySemaphore"
#define INSTRUMENTSTOREDSEMAPHORE "InstrumentStoredSemaphore"

//set size to max number of instruments times the data needed per instrument: 36 Instruments * 21 Values per Point * 4 Byte per Value + 36 Instruments * 20 Characters * 1 Byte per char = 3024 + 720 = 3744 < 4500
#define INSTRUMENTSHAREDMEMORYSIZE 4500 //1500
#define MAXNUMBEROFINSTRUMENTS 20

using namespace boost::interprocess;



//! Class for sharing data with other processes through shared memory
class IPCConnectorIntermediary
{
public:

    /**
     * The camera intrinsics as they are stored in shared memory.
     */
    struct Intrinsics {
        double principalPointX;            //!< Camera principal point X.
        double principalPointY;            //!< Camera principal point Y.
        double focalLengthX;               //!< Camera focal length X.
        double focalLengthY;               //!< Camera focal length Y.
        double distortionCoefficients[6];  //!< Intrinsics model coefficients. For omnidir: 2 radial, skew, xi, 2 tangential.
    public:
        Intrinsics(double ppX, double ppY, double focLX, double focLY, double* distCoeff)
            :principalPointX(ppX), principalPointY(ppY), focalLengthX(focLX), focalLengthY(focLY)
        {
            std::copy(distCoeff, distCoeff + 6, distortionCoefficients);
        };
        Intrinsics(const Intrinsics& other)
            :principalPointX(other.principalPointX), principalPointY(other.principalPointY), focalLengthX(other.focalLengthX), focalLengthY(other.focalLengthY)
        {
            std::copy(other.distortionCoefficients, other.distortionCoefficients + 6, distortionCoefficients);
        };
        Intrinsics() { };
    };

    /**
     * The VSTFrameInfo class as it is stored in shared memory
     * Contains important information for processing the data arrays of the frames.
     * The data provided can be separated in left frame and right frame data, marked by _l or _r.
     */
    struct VSTFrameInfo {
        int width_l;
        int height_l;
        int channels_l;
        Intrinsics intrinsics_l;
        double extrinsics_l[16];
        double HMDPose_l[16];
        float minDepth_l;
        float maxDepth_l;
        int width_r;
        int height_r;
        int channels_r;
        Intrinsics intrinsics_r;
        double extrinsics_r[16];
        double HMDPose_r[16];
        float minDepth_r;
        float maxDepth_r;
    public:
        VSTFrameInfo(int width_left, int height_left, int channels_left, Intrinsics intrinsics_left, double* extrinsics_left, double* HMDPose_left, float minDepth_left, float maxDepth_left, int width_right, int height_right, int channels_right, Intrinsics intrinsics_right, double* extrinsics_right, double* HMDPose_right, float minDepth_right, float maxDepth_right)
            :width_l(width_left), height_l(height_left), channels_l(channels_left), intrinsics_l(intrinsics_left), minDepth_l(minDepth_left), maxDepth_l(maxDepth_left), width_r(width_right), height_r(height_right), channels_r(channels_right), intrinsics_r(intrinsics_right), minDepth_r(minDepth_right), maxDepth_r(maxDepth_right)
        {
            //copy left extrinsic matrix
            std::copy(extrinsics_left, extrinsics_left + 16, extrinsics_l);

            //copy right extrinsic matrix
            std::copy(extrinsics_right, extrinsics_right + 16, extrinsics_r);

            //copy left HMD pose matrix
            std::copy(HMDPose_left, HMDPose_left + 16, HMDPose_l);

            //copy right HMD pose matrix
            std::copy(HMDPose_right, HMDPose_right + 16, HMDPose_r);
        };
        VSTFrameInfo(const VSTFrameInfo& other)
            :width_l(other.width_l), height_l(other.height_l), channels_l(other.channels_l), intrinsics_l(other.intrinsics_l), minDepth_l(other.minDepth_l), maxDepth_l(other.maxDepth_l), width_r(other.width_r), height_r(other.height_r), channels_r(other.channels_r), intrinsics_r(other.intrinsics_r), minDepth_r(other.minDepth_r), maxDepth_r(other.maxDepth_r)
        {
            //copy left extrinsic matrix
            std::copy(other.extrinsics_l, other.extrinsics_l + 16, extrinsics_l);

            //copy right extrinsic matrix
            std::copy(other.extrinsics_r, other.extrinsics_r + 16, extrinsics_r);

            //copy left HMD pose matrix
            std::copy(other.HMDPose_l, other.HMDPose_l + 16, HMDPose_l);

            //copy right HMD pose matrix
            std::copy(other.HMDPose_r, other.HMDPose_r + 16, HMDPose_r);
        }
        VSTFrameInfo() {};
    };

    /**
     * VST frame data for both eyes.
     * Contains color data arrays, depth data arrays and normal maps of each left and right frame. Also includes frame info.
     */
    struct VSTFrame {
        VSTFrameInfo info;
        std::vector<uint8_t> data_l;
        std::vector<uint8_t> data_r;
        std::vector<uint32_t> depthData_l;
        std::vector<uint32_t> depthData_r;
        std::vector<uint8_t> normalMap_l;
        std::vector<uint8_t> normalMap_r;
    public:
        VSTFrame(const VSTFrame& other)
            :info(other.info), data_l(other.data_l), data_r(other.data_r), depthData_l(other.depthData_l), depthData_r(other.depthData_r), normalMap_l(other.normalMap_l), normalMap_r(other.normalMap_r)
        {};
        VSTFrame() {};
    };

    /// Defines for storing the instrument class in shared memory.
    typedef allocator<char, managed_shared_memory::segment_manager>                           char_allocator;
    typedef basic_string <char, std::char_traits<char>, char_allocator>                         char_string;
    typedef allocator<void, managed_shared_memory::segment_manager>                           void_allocator;
    
    /**
     * The instrument class as it is stored in shared memory
     * Contains a 3D pixel center, four directions for the corners of an instrument frame, a normal of the instrument surface, and a camera position.
     * An instrument stores the directions and positions used for positioning interactables to analogue flight instruments locations in a 3D.
     */
    struct Instrument {
        char_string name;

        //center point (deprojected to 3D space)
        float centerX;
        float centerY;
        float centerZ;

        //top point (direction vector)
        float topX;
        float topY;
        float topZ;

        //left point (direction vector)
        float leftX;
        float leftY;
        float leftZ;

        //right point (direction vector)
        float rightX;
        float rightY;
        float rightZ;

        //bottom point (direction vector)
        float bottomX;
        float bottomY;
        float bottomZ;

        //normal (direction vector)
        float normalX;
        float normalY;
        float normalZ;

        //camera position (3D space)
        float camPosX;
        float camPosY;
        float camPosZ;

    public:
        Instrument(const char* nameIn, const void_allocator& void_alloc, glm::vec3 center, glm::vec3 top, glm::vec3 left, glm::vec3 right, glm::vec3 bottom, glm::vec3 normal, glm::vec3 camPosIn)
            :name(nameIn,void_alloc),centerX(center.x),centerY(center.y),centerZ(center.z), topX(top.x), topY(top.y), topZ(top.z), leftX(left.x), leftY(left.y), leftZ(left.z), rightX(right.x), rightY(right.y), rightZ(right.z), bottomX(bottom.x), bottomY(bottom.y), bottomZ(bottom.z), normalX(normal.x), normalY(normal.y), normalZ(normal.z), camPosX(camPosIn.x),camPosY(camPosIn.y),camPosZ(camPosIn.z)
        {}
    };

    /// Further defines for storing the instrument in shared memory
    typedef allocator <Instrument, managed_shared_memory::segment_manager> instrument_allocator;
    typedef vector<Instrument, instrument_allocator> InstrumentDataVector;

    /**
     * The instrument class as it is stored locally in this class
     * Contains a 3D pixel center, four directions for the corners of an instrument frame, a normal of the instrument surface, and a camera position.
     */
    struct InstrumentLocal {
        const char* name;

        //center point (deprojected to 3D space)
        glm::vec3 center;

        //top point (direction vector)
        glm::vec3 top;

        //left point (direction vector)
        glm::vec3 left;

        //right point (direction vector)
        glm::vec3 right;

        //bottom point (direction vector)
        glm::vec3 bottom;

        //normal (direction vector)
        glm::vec3 normal;

        //camera position (3D space)
        glm::vec3 camPos;
    };

    /**
     * The instrument class as it is received from object detection
     * Contains a center pixel, a top pixel, a left pixel, a right pixel, and a bottom pixel
     */
    struct Instrument2D {
        const char* name;
        glm::ivec2 centerPixel;
        glm::ivec2 topPixel;
        glm::ivec2 leftPixel;
        glm::ivec2 rightPixel;
        glm::ivec2 bottomPixel;

    public:
        Instrument2D(const char* nameIn, int xC, int yC, int xT, int yT, int xL, int yL, int xR, int yR, int xB, int yB)
        {
            name = nameIn;
            centerPixel = glm::ivec2(xC, yC);
            topPixel = glm::ivec2(xT, yT);
            leftPixel = glm::ivec2(xL, yL);
            rightPixel = glm::ivec2(xR, yR);
            bottomPixel = glm::ivec2(xB, yB);
        };
        Instrument2D(const Instrument2D& other)
            :name(other.name),centerPixel(other.centerPixel),topPixel(other.topPixel),leftPixel(other.leftPixel),rightPixel(other.rightPixel),bottomPixel(other.bottomPixel)
        {};
    };

    /**
     * Stores a frame and the instruments detected on it by object detection.
     */
    struct LatestProcessedFrame {
        VSTFrame frame;
        std::vector<Instrument2D> detectedInstruments;
    public:
        LatestProcessedFrame(const LatestProcessedFrame& other)
            :frame(other.frame), detectedInstruments(other.detectedInstruments)
        {};
        LatestProcessedFrame(VSTFrame frameIn, std::vector<Instrument2D> detectedInstrumentsIn)
            :frame(frameIn), detectedInstruments(detectedInstrumentsIn)
        {};
        LatestProcessedFrame(){};
    };

    /**
     * Construct IPCConnectorIntermediary
     */
    IPCConnectorIntermediary();

    /**
     * Destruct IPCConnectorIntermediary. Cleans up running threads and shared memory.
     */
    ~IPCConnectorIntermediary();

    // Disable copy, move and assign
    IPCConnectorIntermediary(const IPCConnectorIntermediary& other) = delete;
    IPCConnectorIntermediary(const IPCConnectorIntermediary&& other) = delete;
    IPCConnectorIntermediary& operator=(const IPCConnectorIntermediary& other) = delete;
    IPCConnectorIntermediary& operator=(const IPCConnectorIntermediary&& other) = delete;

    /* Methods for IPC*/

   /**
     * Enables or disables import of VST frames based on 'status'
     * Start or ends importVSTThread.
     *
     * @param status - Indicator if disabling or enabling VST frame import.
     */
    void SetVSTFrameImport(bool status);

    /**
     * Enables or disables export of instrument data based on 'status'
     * Start or ends exportInstrumentDataThread.
     *
     * @param status - Indicator if disabling or enabling instrument data export.
     */
    void SetInstrumentDataExport(bool status);

    /**
     * Enables or disables the processing of instrument data retrieved from object detection based on 'status'
     * Start or ends processInstrumentThread.
     *
     * @param status - Indicator if disabling or enabling instrument processing.
     */
    void SetInstrumentProcessing(bool status);


    /* Methods for initialisation and excecution*/

    /**
     * Initializes application.
     * Creates memory segments for instrument export
     */
    bool init();

    /**
     * Starts application.
     * Starts import, export, and processing threads.
     * Creates python callCFromPy module and makes it available for the python script.
     * Starts a python script.
     * Runs indefinitly as long as console is not closed or process is not ended.
     */
    void run();

    /*  Python Methods  */

    /** 
     * callCFromPy python module method for retrieving the latest VST frame which is available in this C++ class from a python script.
     * Gets latest frame and saves it in m_DetectionFrame variable. There it is again accessed when instruments are returned with callCFromPy_setNewInstruments.
     * 
     * @returns A tuple of five objects containing, if the data is valid, a width, a height, a stride, and the data vector as python list.
     */
    static PyObject* callCFromPy_getLatestFrame(PyObject* self, PyObject* args);

    /**
     * callCFromPy python module method for receiving the detected instruments from python in this C++ class.
     * 
     * Receives a tuple that includes a list of tuples. Each of latter contains a confidence value, a x pixel coordinate, a y pixel coordinate, a width, and a height.
     * The python types are converted to C++ data types and for each tuple an Instrument2D is constructed.
     */
    static PyObject* callCFromPy_setNewInstruments(PyObject* self, PyObject* args);

    /**
     * Creates and initializes callCFromPy python module
     */
    static PyObject* PyInit_callCFromPy(void);

private:

    /* Private Methods for IPC*/

    /**
     * Initialized and creates memory segments and semaphores for instrument data export.
     */
    bool initMemorySegment();

    /**
     * Method that imports the VST frames.
     * Executed by importVSTThread as long as importVSTData is true.
     * First tries to open VST memory segments, then opens semaphores and checks if a frame is available in shared memory.
     * If a frame is available, it locks the VSTFrameMutex and writes the data from shared memory to the m_LatestVSTFrame variable.
     * Aborts import process if any boost error is thrown.
     *
     */
    void importVSTFrame();

    /**
     * Method that exports the instruments.
     * Executed by exportInstrumentDataThread as long as exportInstrument is true.
     * First tries to open instrument memory segments, then opens semaphores and writes the latest instrument data vector in shared memory if the shared memory is empty.
     * Aborts export process if any boost error is thrown.
     *
     */
    void exportInstrumentData();
    
    /**
     * Deprojects and prepares instruments for export that are extracted from the latest processed frame
     * 
     * Deprojects pixel coordinates given by Instrument2D structure to 3D space.
     * The center pixel gets properly deprojected to 3D space using the depth extracted from the depth map.
     * The other pixels are only deprojected to directions where they are located in 3D space.
     * This is done, because one of the other pixels could be obstructed by an object in front of an instrument. 
     * Therefore a 3D deprojection using depth would lead to a faulty result when calculating scale or rotation, because the coordinate is not in its real location (in the same depth that the instrument would be in).
     * 
     * Appends an InstrumentLocal to m_LatestInstrumentData for every correctly deprojected instrument from the frame.
     * 
     */
    void updateInstrumentData();

private:

    ///Indicator if instrument data is currently exported.
    bool exportInstrument = false;

    ///Indicator if instrument data is currently processed.
    bool processInstruments = false;

    ///Thread that imports VST frames.
    std::thread importVSTThread;

    ///Thread that exports instrument data.
    std::thread exportInstrumentDataThread;

    ///Thread that processes instruments
    std::thread processInstrumentThread;

    ///Contains the latest instrument data for export to shared memory.
    std::vector<InstrumentLocal> m_LatestInstrumentData;

    //Mutex for avoiding concurrent access to m_LatestInstrumentData from thread and in updateInstrumentData.
    mutable std::mutex InstrumentDataMutex;
};
