


#include "IPCConnectorIntermediary.hpp"

#include <string>
#include <algorithm>
#include <vector>
#include <iostream>
#include <wchar.h>
#include <exception>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>


//variables that are static and defined here, because they are accessed by the python script

///Indicator if VST data is currently imported.
static bool importVSTData;

///Contains the latest VST frame received with shared memory.
static IPCConnectorIntermediary::VSTFrame m_LatestVSTFrame;

///Contains the frame currently being processed by the python script
static IPCConnectorIntermediary::VSTFrame m_DetectionFrame;

///Contains the latest frame which includes instruments detected on it
static IPCConnectorIntermediary::LatestProcessedFrame m_LatestProcessedFrame;

///Mutex for avoiding concurrent access to m_LatestProcessedFrame from thread and in callCFromPy_setNewInstruments.
static std::mutex ProcessedFrameMutex;

///Mutex for avoiding concurrent access to m_LatestVSTFrame from thread and in callCFromPy_getLatestFrame.
static std::mutex VSTFrameMutex;

IPCConnectorIntermediary::IPCConnectorIntermediary()
{
    shared_memory_object::remove(INSTRUMENTSHAREDMEMORY);

    named_semaphore::remove(INSTRUMENTMUTEXSEMAPHORE);
    named_semaphore::remove(INSTRUMENTEMPTYSEMAPHORE);
    named_semaphore::remove(INSTRUMENTSTOREDSEMAPHORE);

    importVSTData = false;
}

IPCConnectorIntermediary::~IPCConnectorIntermediary()
{
    //wait for threads to end
    importVSTData = false;

    //wait for thread to join, before memory segments get destroyed
    if (importVSTThread.joinable()) {
        importVSTThread.join();
    }

    exportInstrument = false;
    //wait for thread to join, before memory segments get destroyed
    if (exportInstrumentDataThread.joinable()) {
        exportInstrumentDataThread.join();
    }

    processInstruments = false;
    //wait for thread to join
    if (processInstrumentThread.joinable()) {
        processInstrumentThread.join();
    }

    shared_memory_object::remove(INSTRUMENTSHAREDMEMORY);

    named_semaphore::remove(INSTRUMENTMUTEXSEMAPHORE);
    named_semaphore::remove(INSTRUMENTEMPTYSEMAPHORE);
    named_semaphore::remove(INSTRUMENTSTOREDSEMAPHORE);
}


bool IPCConnectorIntermediary::initMemorySegment() {

    //init instrumentData shared memory segment
    {
        try {
            managed_shared_memory(create_only, INSTRUMENTSHAREDMEMORY, INSTRUMENTSHAREDMEMORYSIZE);
            std::cout << "Instrument Shared Memory Segment created" << std::endl;
        }
        catch (interprocess_exception e) {
            std::cout << "Instrument shared Memory segment could not be created. Got error: " << e.what() << std::endl;
            return false;
        }


        try {
            //create semaphores for synchronization of processes
            
            //semaphore that handels that only one process at a time can access our instrument data
            named_semaphore MutexSemaphore(create_only_t(), INSTRUMENTMUTEXSEMAPHORE, 1);

            //semaphore that holds the number of items that have been emptied from shared memory
            named_semaphore EmptySemaphore(create_only_t(), INSTRUMENTEMPTYSEMAPHORE, 1);

            //semaphore that holds the number of items currently stored in the shared memory
            named_semaphore StoredSemaphore(create_only_t(), INSTRUMENTSTOREDSEMAPHORE, 0);
        }
        catch (interprocess_exception e) {
            std::cout << "Could not create Instrument semaphores. Got error: " << e.what() << std::endl;
            return false;
        }

        return true;
    }
}

void IPCConnectorIntermediary::importVSTFrame()
{
    try {
        //open semaphores
        //semaphore that handels that only one process at a time can access our shared memory region
        named_semaphore VSTMutexSemaphore(open_only_t(), MRMUTEXSEMAPHORE);
        //semaphore that holds the number of items that have been emptied from shared memory
        named_semaphore VSTEmptySemaphore(open_only_t(), MREMPTYSEMAPHORE);
        //semaphore that holds the number of items currently stored in the shared memory
        named_semaphore VSTStoredSemaphore(open_only_t(), MRSTOREDSEMAPHORE);
        std::cout << "VST Semaphores opened" << std::endl;

        //open VST Memory Segment
        managed_shared_memory VSTMemorySegment(open_only_t(), MRSHAREDMEMORY);
        std::cout << "VST Shared Memory Segment opened" << std::endl;

        while (importVSTData) {

            if (VSTStoredSemaphore.try_wait()) {
                if (VSTMutexSemaphore.try_wait()) {
                    //Find frame info object
                    VSTFrameInfo* info = VSTMemorySegment.find<VSTFrameInfo>("VSTFrameInfo").first;

                    //Find the color array with the left eye frame data
                    std::pair<uint8_t*, std::size_t> VSTArray_l = VSTMemorySegment.find<uint8_t>("VSTDataArray_l");

                    //Find the color array with the right eye frame data 
                    std::pair<uint8_t*, std::size_t> VSTArray_r = VSTMemorySegment.find<uint8_t>("VSTDataArray_r");

                    //Find depth data array of the left eye frame
                    std::pair<uint32_t*, std::size_t> DepthArray_l = VSTMemorySegment.find<uint32_t>("DepthDataArray_l");

                    //Find depth data array of the right eye frame
                    std::pair<uint32_t*, std::size_t> DepthArray_r = VSTMemorySegment.find<uint32_t>("DepthDataArray_r");

                    //Find normal data array of the left eye frame
                    std::pair<uint8_t*, std::size_t> NormalArray_l = VSTMemorySegment.find<uint8_t>("NormalDataArray_l");

                    //Find normal data array of the right eye frame
                    std::pair<uint8_t*, std::size_t> NormalArray_r = VSTMemorySegment.find<uint8_t>("NormalDataArray_r");

                    //check if we found something
                    if (info != 0 && VSTArray_l.first != 0 && VSTArray_r.first != 0){//&& DepthArray_l.first != 0 && DepthArray_r.first != 0) {
                        //Prevent acces on m_LatestVSTFrame from different thread
                        std::lock_guard<std::mutex> streamLock(VSTFrameMutex);

                        //update current frame

                        //copy frame info
                        m_LatestVSTFrame.info = VSTFrameInfo(*info);

                        //copy left color data
                        if (VSTArray_l.second > 0) {
                            //copy vector at this point, to prevent long intervals between frames due to further processing of the received data.
                            m_LatestVSTFrame.data_l = std::vector<uint8_t>(VSTArray_l.first, VSTArray_l.first + VSTArray_l.second);
                        }
                        else {
                            //delete old data
                            m_LatestVSTFrame.data_l = std::vector<uint8_t>();
                        }

                        //copy right color data
                        if (VSTArray_r.second > 0) {
                            //copy vector at this point, to prevent long intervals between frames due to further processing of the received data.
                            m_LatestVSTFrame.data_r = std::vector<uint8_t>(VSTArray_r.first, VSTArray_r.first + VSTArray_r.second);
                        }
                        else {
                            //delete old data
                            m_LatestVSTFrame.data_r = std::vector<uint8_t>();
                        }

                        //copy left depth data
                        if (DepthArray_l.second > 0) {
                            m_LatestVSTFrame.depthData_l = std::vector<uint32_t>(DepthArray_l.first, DepthArray_l.first + DepthArray_l.second);
                        }
                        else {
                            //delete old data
                            m_LatestVSTFrame.depthData_l = std::vector<uint32_t>();
                        }

                        //copy right depth data
                        if (DepthArray_r.second > 0) {
                            m_LatestVSTFrame.depthData_r = std::vector<uint32_t>(DepthArray_r.first, DepthArray_r.first + DepthArray_r.second);
                        }
                        else {
                            //delete old data
                            m_LatestVSTFrame.depthData_r = std::vector<uint32_t>();
                        }

                        //copy left normal data
                        if (NormalArray_l.second > 0) {
                            m_LatestVSTFrame.normalMap_l = std::vector<uint8_t>(NormalArray_l.first, NormalArray_l.first + NormalArray_l.second);
                        }
                        else {
                            //delete old data
                            m_LatestVSTFrame.normalMap_l= std::vector<uint8_t>();
                        }

                        //copy right normal data
                        if (NormalArray_r.second > 0) {
                            m_LatestVSTFrame.normalMap_r = std::vector<uint8_t>(NormalArray_r.first, NormalArray_r.first + NormalArray_r.second);
                        }
                        else {
                            //delete old data
                            m_LatestVSTFrame.normalMap_r = std::vector<uint8_t>();
                        }
                    }

                    //When done destroy the arrays and frame info from the segment
                    VSTMemorySegment.destroy<uint8_t>("VSTDataArray_l");
                    VSTMemorySegment.destroy<uint8_t>("VSTDataArray_r");

                    VSTMemorySegment.destroy<VSTFrameInfo>("VSTFrameInfo");

                    VSTMemorySegment.destroy<uint32_t>("DepthDataArray_l");
                    VSTMemorySegment.destroy<uint32_t>("DepthDataArray_r");

                    VSTMemorySegment.destroy<uint8_t>("NormalDataArray_l");
                    VSTMemorySegment.destroy<uint8_t>("NormalDataArray_r");


                    //notify host that memory segment is empty
                    VSTMutexSemaphore.post();
                    VSTEmptySemaphore.post();
                }
                else {
                    //post Stored Semaphore again, so next loop we can check for both conditions
                    VSTStoredSemaphore.post();
                }
            }
        }
    }
    catch (boost::interprocess::interprocess_exception e) {
        importVSTData = false;
        std::cout << "Error when importing VST Frame. Stopping import. Exception: " << e.what() << std::endl;
    }
}

void IPCConnectorIntermediary::exportInstrumentData() {
    try {
        //open semaphores
        //semaphore that handels that only one process at a time can access our shared memory region
        named_semaphore InstrumentMutexSemaphore(open_only_t(), INSTRUMENTMUTEXSEMAPHORE);
        //semaphore that holds the number of items that have been emptied from shared memory
        named_semaphore InstrumentEmptySemaphore(open_only_t(), INSTRUMENTEMPTYSEMAPHORE);
        //semaphore that holds the number of items currently stored in the shared memory
        named_semaphore InstrumentStoredSemaphore(open_only_t(), INSTRUMENTSTOREDSEMAPHORE);
        std::cout << "Instrument Semaphores opened" << std::endl;

        //open instrument Memory Segment
        managed_shared_memory InstrumentMemorySegment(open_only_t(), INSTRUMENTSHAREDMEMORY);
        std::cout << "Instrument Shared Memory Segment opened" << std::endl;

        while (exportInstrument) {

            if (InstrumentEmptySemaphore.try_wait()) {
                if (InstrumentMutexSemaphore.try_wait()) {

                    //lock mutex for accessing latest instrument data
                    std::lock_guard<std::mutex> streamLock(InstrumentDataMutex);

                    //avoid writing empty vector to shared memory
                    if (!m_LatestInstrumentData.empty()) {

                        //check if number of instruments do not exceed the number allocated for
                        if (m_LatestInstrumentData.size() <= MAXNUMBEROFINSTRUMENTS) {
                            std::cout << "Sent instruments" << std::endl;
                            
                            void_allocator alloc_inst(InstrumentMemorySegment.get_segment_manager());

                            //create instrument vector in shared memory
                            InstrumentDataVector* InstrumentVector = InstrumentMemorySegment.construct<InstrumentDataVector>("InstrumentDataVector")(alloc_inst);

                            //push data in newly created vector
                            for (InstrumentLocal instrument: m_LatestInstrumentData) {
                               InstrumentVector->push_back(Instrument(instrument.name, alloc_inst, instrument.center, instrument.top, instrument.left, instrument.right, instrument.bottom, instrument.normal, instrument.camPos));
                            }

                            //empty m_latestInstrumentData to avoid sending same data again
                            m_LatestInstrumentData.clear();

                            //notify client that memory segment has data stored
                            InstrumentMutexSemaphore.post();
                            InstrumentStoredSemaphore.post();
                        }
                        else {
                            std::cout << "Instrument vector too big for allocated memory. Skipping vector..." << std::endl;
                            InstrumentMutexSemaphore.post();
                            InstrumentEmptySemaphore.post();
                        }
                    }
                    else {
                        //since no data is available, increase StoredSemaphore and try again next loop
                        InstrumentMutexSemaphore.post();
                        InstrumentEmptySemaphore.post();
                    }
                }
                else {
                    //post Stored Semaphore again, so next loop we can check for both conditions
                    InstrumentEmptySemaphore.post();
                }
            }
        }
    }
    catch (boost::interprocess::interprocess_exception e) {
        exportInstrument = false;
        std::cout << "Error when exporting instrument data. Stopping export. Exception: " << e.what() << std::endl;
    }
}

void IPCConnectorIntermediary::SetVSTFrameImport(bool status)
{
    if (status && !importVSTData) {
        //start importing vst data

        //set vst import bool
        importVSTData = status;

        //start vst import thread
        importVSTThread = std::thread(&IPCConnectorIntermediary::importVSTFrame, this);
    }
    else if (!status && importVSTData) {
        //stop importing vst data

        //set vst import bool
        importVSTData = status;

        //join thread if joinable
        if (importVSTThread.joinable()) {
            importVSTThread.join();
        }
    }
}

void IPCConnectorIntermediary::SetInstrumentDataExport(bool status) {
    if (status && !exportInstrument) {
        //start exporting instrument data

        //set instrument export bool
        exportInstrument = status;

        //start instrument export thread
        exportInstrumentDataThread = std::thread(&IPCConnectorIntermediary::exportInstrumentData, this);
    }
    else if (!status && exportInstrument) {
        //stop exporting instrument data

        //set instrument export bool
        exportInstrument = status;

        //join thread if joinable
        if (exportInstrumentDataThread.joinable()) {
            exportInstrumentDataThread.join();
        }
    }
}

void IPCConnectorIntermediary::SetInstrumentProcessing(bool status) {
    if (status && !processInstruments) {
        //start processing instruments

        //set process instruments bool
        processInstruments = status;

        //start process instruments thread
        processInstrumentThread = std::thread(&IPCConnectorIntermediary::updateInstrumentData, this);
    }
    else if (!status && processInstruments) {
        //stop processing instruments

        //set process instruments bool
        processInstruments = status;

        //join thread if joinable
        if (processInstrumentThread.joinable()) {
            processInstrumentThread.join();
        }
    }
}

void IPCConnectorIntermediary::updateInstrumentData() {

    while (processInstruments) {
        LatestProcessedFrame processedFrame;

        {
            //acquire latestProcessedFrame
            std::lock_guard<std::mutex> ProcessedFrameLock(ProcessedFrameMutex);
            processedFrame = m_LatestProcessedFrame;

            //clear m_LatestProcessedFrame to avoid accessing same processedframe multiple times
            m_LatestProcessedFrame = LatestProcessedFrame();
        }

        if (!processedFrame.detectedInstruments.empty()) {

            //access only left frame info for now, because only left frame gets processed
            std::pair<int, int> resolution = std::pair<int, int>(processedFrame.frame.info.width_l, processedFrame.frame.info.height_l);
            double principalPointX = processedFrame.frame.info.intrinsics_l.principalPointX;
            double principalPointY = processedFrame.frame.info.intrinsics_l.principalPointY;
            double focalLengthX = processedFrame.frame.info.intrinsics_l.focalLengthX;
            double focalLengthY = processedFrame.frame.info.intrinsics_l.focalLengthY;
            double* extrinsics = processedFrame.frame.info.extrinsics_l;
            std::vector<uint32_t> depthData = processedFrame.frame.depthData_l;
            float minDepth = processedFrame.frame.info.minDepth_l;
            float maxDepth = processedFrame.frame.info.maxDepth_l;
            std::vector<Instrument2D> instruments = processedFrame.detectedInstruments;
            std::vector<uint8_t> normalMap = processedFrame.frame.normalMap_l;

            if (!depthData.empty() && !normalMap.empty()) {

                //build inverse instrinsic matrix
                const glm::mat3x3 offset = glm::translate(glm::mat3(1.0f), glm::vec2((principalPointX - 0.5) * -2.0, (0.5 - principalPointY) * -2.0)); // glm::vec2(principalPointX, principalPointY)
                const glm::mat3x3 scale = glm::scale(glm::mat3(1.0f), glm::vec2(focalLengthX, focalLengthY));
                const glm::mat3x3 intrinsicsMat = offset * scale;
                const glm::mat3x3 invIntr = glm::inverse(intrinsicsMat);

                //convert extrinsic array to extrinsic matrix (extrinsics include transform from world space to camera relative space)
                glm::mat4x4 extrinsicsMat(extrinsics[0], extrinsics[1], extrinsics[2], extrinsics[3],
                    extrinsics[4], extrinsics[5], extrinsics[6], extrinsics[7],
                    extrinsics[8], extrinsics[9], extrinsics[10], extrinsics[11],
                    extrinsics[12], extrinsics[13], extrinsics[14], extrinsics[15]);

                //invert, because typically extrinsics are used to transform world points to camera coordinates. Here we want to do the opposite, so we inverse the extrinsics
                glm::mat4x4 invExt = glm::inverse(extrinsicsMat);

                const glm::mat3x3 rot = glm::mat3x3(extrinsicsMat);
                const glm::vec3 t = glm::vec3(extrinsicsMat[3]);

                //calculate camera world position for later
                glm::vec4 camPos4 = invExt * glm::vec4(0, 0, 0, 1);
                glm::vec3 camPos = glm::vec3(camPos4.x / camPos4.w, camPos4.y / camPos4.w, camPos4.z / camPos4.w);


                std::vector<InstrumentLocal> newInstruments;
                for (Instrument2D instrument : instruments) {

                    //calculate 3D coordinates for each instrument
                    std::vector<glm::ivec2> pixels;
                    pixels.push_back(instrument.centerPixel); // center is in first entry
                    pixels.push_back(instrument.topPixel);
                    pixels.push_back(instrument.leftPixel);
                    pixels.push_back(instrument.rightPixel);
                    pixels.push_back(instrument.bottomPixel);

                    //get depth and normal only for center pixel
                    float centerPixelDepth;
                    glm::vec3 centerPixelNormal;

                    //calculate center pixel index in one dimensional depth array
                    int pixel1D = (instrument.centerPixel.y * resolution.first) + instrument.centerPixel.x;
                    //only proceed if index is within bounds
                    if (pixel1D < resolution.first * resolution.second) {

                        //first get depth for this pixel from depth image
                        uint32_t depth = depthData[pixel1D];

                        //if there is no valid depth value ( valid means > 0), don't process this pixel. 
                        if (depth > 0) {
                            //calculate depth in meters
                            
                            //convert from uint32_t to float between 0 and 1
                            float convertedDepth;
                            if (depth == 4294967295) {
                                convertedDepth = 1.0;
                            }
                            else {
                                convertedDepth = depth / 4294967296.0;
                            }

                            //convert from 0 to 1 float range to depth in meters
                            centerPixelDepth = (convertedDepth * (maxDepth - minDepth)) + minDepth;
                        
                            
                            //get normal from normalmap
                            int pixelID1D3C = (instrument.centerPixel.y * resolution.first * 3) + (instrument.centerPixel.x * 3);
                            //lookup normal coordinates
                            uint8_t intX = normalMap[pixelID1D3C];
                            uint8_t intY = normalMap[pixelID1D3C + 1];
                            uint8_t intZ = normalMap[pixelID1D3C + 2];
                            //convert to float
                            float normalX = glm::unpackUnorm1x8(intX);
                            float normalY = glm::unpackUnorm1x8(intY);
                            float normalZ = glm::unpackUnorm1x8(intZ);

                            //map back to -1 to 1 range
                            normalX = (normalX * 2.0) - 1.0;
                            normalY = (normalY * 2.0) - 1.0;
                            normalZ = (normalZ * 2.0) - 1.0;

                            //save normal
                            centerPixelNormal = glm::vec3(normalX, normalY, normalZ);
                            centerPixelNormal = glm::normalize(centerPixelNormal);

                        }
                        else {
                            continue;
                        }
                    }
                    else {
                        continue;
                    }

                    //calculate world coordinates for each pixel coordinate

                    std::vector<glm::vec3> points;
                    for (int i = 0; i < pixels.size(); i++) {
                        //get 3D coordinate of pixels

                        //convert pixel coordinates to ndc
                        glm::vec2 ndc = glm::vec2(((glm::vec2(pixels[i]) + 0.5f) / glm::vec2(resolution.first, resolution.second) * glm::vec2(2.0f, -2.0f)) + glm::vec2(-1.0f, 1.0f));

                        //inverse camera intriscic transformation
                        glm::vec3 pi = invIntr * glm::vec3(ndc, -1);

                        //transform point to world coordinates with inverse extrinsic matrix
                        glm::vec4 pw4 = invExt * glm::vec4(pi, 1);//t + rot * pi;
                        glm::vec3 pw = glm::vec3(pw4.x / pw4.w, pw4.y / pw4.w, pw4.z / pw4.w);

                        //get direction vector from camera to world coordinate point
                        glm::vec3 dir = pw - camPos;
                        dir = glm::normalize(dir);
                        
                        if (i == 0) {
                            //if we deproject the center pixel, save exact 3D world position
                            //get world coordinate 3D point of pixel by multiplying direction times depth and adding camera translation
                            glm::vec3 position = camPos + (dir * centerPixelDepth);
                            points.push_back(position);
                        }
                        else {
                            //for all other pixels only save direction
                            points.push_back(dir);
                        }
                    }

                    //save results as new instrument
                    InstrumentLocal localInstrument;
                    localInstrument.name = instrument.name;
                    localInstrument.center = points[0];
                    localInstrument.top = points[1];
                    localInstrument.left = points[2];
                    localInstrument.right = points[3];
                    localInstrument.bottom = points[4];
                    localInstrument.normal = centerPixelNormal;
                    localInstrument.camPos = camPos;

                    newInstruments.push_back(localInstrument);
                }

                if (!newInstruments.empty()) {
                    //insert new instruments into vector for export
                    std::lock_guard<std::mutex> streamLock(InstrumentDataMutex);
                    m_LatestInstrumentData.insert(m_LatestInstrumentData.end(), newInstruments.begin(), newInstruments.end());
                }
            }
            else {
                std::cout << "No Depth data provided" << std::endl;
            }

        }

    }
}


PyObject* IPCConnectorIntermediary::callCFromPy_getLatestFrame(PyObject* self, PyObject* args)
{
    //grab latest VSTFrame
    {
        //Prevent acces on m_LatestVSTFrame from different thread
        std::lock_guard<std::mutex> VSTLock(VSTFrameMutex);

        m_DetectionFrame = m_LatestVSTFrame;
    }

    //only return left image for now

    PyObject* tupleObject = PyTuple_New(5);
    PyObject* width = PyLong_FromLong(m_DetectionFrame.info.width_l);
    PyObject* height = PyLong_FromLong(m_DetectionFrame.info.height_l);
    PyObject* stride = PyLong_FromLong(m_DetectionFrame.info.channels_l * sizeof(uint8_t));

    //if not importing VSTData, then stop frame processing and python application
    PyObject* valid = PyBool_FromLong(importVSTData);

    PyTuple_SetItem(tupleObject, 0, valid);
    PyTuple_SetItem(tupleObject, 1, width);
    PyTuple_SetItem(tupleObject, 2, height);
    PyTuple_SetItem(tupleObject, 3, stride);
    PyTuple_SetItem(tupleObject, 4, vectorToList(m_DetectionFrame.data_l));

    return tupleObject;
}

PyObject* IPCConnectorIntermediary::callCFromPy_setNewInstruments(PyObject* self, PyObject* args)
{
    PyObject* resultList;
    if (PyArg_UnpackTuple(args, "", 1, 1, &resultList))
    {
        std::vector<Instrument2D> resultVector;

        //parse returned list
        if (PyList_Check(resultList)) {
            for (Py_ssize_t i = 0; i < PyList_Size(resultList); i++) {
                PyObject* value = PyList_GetItem(resultList, i);

                //create instrument from each tuple in the list
                if (PyTuple_Check(value)) {
                    PyObject* nameP = PyTuple_GetItem(value, 0);
                    
                    PyObject* confidenceP = PyTuple_GetItem(value, 1);
                    PyObject* xP = PyTuple_GetItem(value, 2);
                    PyObject* yP = PyTuple_GetItem(value, 3);
                    PyObject* wP = PyTuple_GetItem(value, 4);
                    PyObject* hP = PyTuple_GetItem(value, 5);

                    //we receive center pixel value and width and height from python


                    //convert to c datatypes
                    const char* name = PyUnicode_AsUTF8(nameP);
                    double confidence = PyFloat_AsDouble(confidenceP);
                    int xC = PyLong_AsLong(xP);
                    int yC = PyLong_AsLong(yP);
                    int w = PyLong_AsLong(wP);
                    int h = PyLong_AsLong(hP);

                    if (confidence <0.8) {
                        //skip instruments that have a confidence of lower than 80 percent
                        continue;
                    }
                    //calculate corners of quad
                    
                    float halfX = w / 2;
                    float halfY = h / 2;

                    //top pixel
                    int xT = xC;
                    int yT = yC - halfY;

                    //left pixel
                    int xL = xC - halfX;
                    int yL = yC;

                    //right pixel
                    int xR = xC + halfX;
                    int yR = yC;
                    
                    //bottom pixel
                    int xB = xC;
                    int yB = yC + halfY;

                    resultVector.push_back(Instrument2D(name, xC, yC, xT, yT, xL, yL, xR, yR, xB, yB));
                }

            }
        }
        std::cout << "Got: " << resultVector.size() << " instruments!" << std::endl;

        //update latest processed frame
        std::lock_guard<std::mutex> ProcessedFrameLock(ProcessedFrameMutex);

        m_LatestProcessedFrame = LatestProcessedFrame(m_DetectionFrame, resultVector);
    }

    return PyLong_FromLong(0);
}

static struct PyMethodDef methods[] = {
    { "getLatestFrame", IPCConnectorIntermediary::callCFromPy_getLatestFrame, METH_VARARGS, "Transfers latest available frame from C++ class"},
    { "setNewInstruments", IPCConnectorIntermediary::callCFromPy_setNewInstruments, METH_VARARGS, "Transfers detected instruments to C++ class" },
    { NULL, NULL, 0, NULL }
};

static struct PyModuleDef modDef = {
    PyModuleDef_HEAD_INIT, "callCFromPy", NULL, -1, methods,
    NULL, NULL, NULL, NULL
};

PyObject* IPCConnectorIntermediary::PyInit_callCFromPy(void)
{
    return PyModule_Create(&modDef);
}

bool IPCConnectorIntermediary::init()
{
    //create memory segments and semaphores for instrument export
    return initMemorySegment();
}

void IPCConnectorIntermediary::run()
{   
    //enable shared memory import and export processes
    SetVSTFrameImport(true);
    SetInstrumentDataExport(true);

    //start processing instruments thread
    SetInstrumentProcessing(true);

    //create python module
    PyImport_AppendInittab("callCFromPy", &PyInit_callCFromPy);

    CPyInstance hInstance;

    //prepare arguments for python script
    int argc2;
    wchar_t* argv2[7];

    //set python script name
    wchar_t myString0[1024];
    std::wcsncpy(myString0, L"detect_for_BA2.py", 1024);

    //parameter for specifying a weights file
    wchar_t myString1[1024];
    std::wcsncpy(myString1, L"--weights", 1024);

    //weights file path (relative to .exe)
    wchar_t myString2[1024];
    std::wcsncpy(myString2, L"../../projects/ObjectDetection/files/best.pt", 1024);
    //std::wcsncpy(myString2, L"objectDetectionFiles/best.pt", 1024);

    //parameter for specifying the source image (not used, only needed for start up)
    wchar_t myString3[1024];
    std::wcsncpy(myString3, L"--source", 1024);

    //source image file path (relative to .exe)
    wchar_t myString4[1024];
    std::wcsncpy(myString4, L"../../projects/ObjectDetection/files/Test_2.jpg", 1024);
    //std::wcsncpy(myString4, L"objectDetectionFiles/Test_2.jpg", 1024);

    //parameter for specifying the confidence threshold
    wchar_t myString5[1024];
    std::wcsncpy(myString5, L"--conf-thres", 1024);

    //the confidence threshold
    wchar_t myString6[1024];
    std::wcsncpy(myString6, L"0.45", 1024);

    //add arguments to one array
    argc2 = 7;
    argv2[0] = myString0;
    argv2[1] = myString1;
    argv2[2] = myString2;
    argv2[3] = myString3;
    argv2[4] = myString4;
    argv2[5] = myString5;
    argv2[6] = myString6;

    //set arguments to python
    PySys_SetArgv(argc2, argv2);

    //YOLO script file path (relative to .exe)
    const char pFile[] = "../../projects/ObjectDetection/files/detect_for_BA2.py";//"objectDetectionFiles/detect_for_BA2.py";

    //open file
    FILE* fp = fopen(pFile, "r");

    if (fp) {
        std::cout << "Found script file." << std::endl;

        //run python script
        PyRun_SimpleFile(fp, pFile);

        //close file
        fclose(fp);
    }
    else {
        std::cout << "Script file not found." << std::endl;
    }

    //infinite loop, so application runs as long as console is not closed, or process is manually terminated
    while (true) {

    }
}