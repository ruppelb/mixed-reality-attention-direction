// Copyright 2019-2021 Varjo Technologies Oy. All rights reserved.

/* Mixed Reality Example Application
 *
 * - Showcases Varjo MR API features: Camera, data streams, rendering, and more!
 * - Run example and press F1 for help
 */

// Internal includes
//#include "Globals.hpp"
#include "IPCConnectorIntermediary.hpp"
#include <iostream>
#include <cstdio>
#include <cstdint>
#include <system_error>
#include <string>
#include <stdexcept>
#include <functional>
#include <wrl.h>

std::unique_ptr<IPCConnectorIntermediary, std::default_delete<IPCConnectorIntermediary>> IPCConnector;

// Common main function called from the entry point
void commonMain()
{
    // Instantiate application logic and view
    IPCConnector = std::make_unique<IPCConnectorIntermediary>();

    try {
        // Init application
        std::cout << "Initializing application.." << std::endl;
        if (IPCConnector->init()) {
            // Enter the main loop
            std::cout << "Running application.." << std::endl;
            IPCConnector->run();
        } else {
            std::cout << "Initializing application failed." << std::endl;
        }
    } catch (const std::runtime_error& e) {
        std::cout << "Critical error caught: %s" << e.what() << std::endl;
    }

    // Deinitialize client app
    std::cout << "Deinitializing application.." << std::endl;
    IPCConnector.reset();

    // Exit successfully
    std::cout << "Done!" << std::endl;
}

BOOL WINAPI ConsoleHandler(DWORD CEvent)
{
    switch (CEvent)
    {
    case CTRL_C_EVENT:
        IPCConnector.reset();
        break;
    case CTRL_BREAK_EVENT:
        IPCConnector.reset();
        break;
    case CTRL_CLOSE_EVENT:
        IPCConnector.reset();
        break;
    case CTRL_LOGOFF_EVENT:
        IPCConnector.reset();
        break;
    case CTRL_SHUTDOWN_EVENT:
        IPCConnector.reset();
        break;

    }
    return TRUE;
}

// Console application entry point
int main(int argc, char** argv)
{
    if (SetConsoleCtrlHandler(
        (PHANDLER_ROUTINE)ConsoleHandler, TRUE) == FALSE)
    {
        // unable to install handler... 
        // display message to the user
        printf("Unable to install handler!\n");
        return -1;
    }

    // Call common main function
    commonMain();

    // Application finished
    return EXIT_SUCCESS;
}


// Windows application entry point
int __stdcall wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow)
{
    int argc = 0;
    LPWSTR* args = CommandLineToArgvW(pCmdLine, &argc);

    bool console = false;
    for (int i = 0; i < argc; i++) {
        if (std::wstring(args[i]) == (L"--console")) {
            console = true;
        }
    }

    if (console) {
        if (AttachConsole(ATTACH_PARENT_PROCESS) || AllocConsole()) {
            errno_t err;
            FILE* stream = nullptr;
            err = freopen_s(&stream, "CONOUT$", "w", stdout);
            err = freopen_s(&stream, "CONOUT$", "w", stderr);
        }
    }

    if (SetConsoleCtrlHandler(
        (PHANDLER_ROUTINE)ConsoleHandler, TRUE) == FALSE)
    {
        // unable to install handler... 
        // display message to the user
        printf("Unable to install handler!\n");
        return -1;
    }

    // Call common main function
    commonMain();

    // Application finished
    return EXIT_SUCCESS;
}