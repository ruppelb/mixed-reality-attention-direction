// Copyright 2019-2021 Varjo Technologies Oy. All rights reserved.

#pragma once

#include <glm/glm.hpp>

#include "Globals.hpp"
#include "MarkerTracker.hpp"

//! Application state struct
struct AppState {
    //! General params structure
    struct General {
        double frameTime{0.0f};   //!< Current frame time
        int64_t frameCount{0};    //!< Current frame count
        bool mrAvailable{false};  //!< Mixed reality available flag
    };

    //! Options structure
    struct Options {
        bool videoRenderingEnabled{true};                 //!< Render VST image flag
        bool reactToConnectionEvents{true};               //!< Flag for reacting to MR connect/disconnect events
        bool dataStreamColorEnabled{true};               //!< Color data stream enabled flag
        bool delayedBufferHandlingEnabled{false};         //!< Delayed data stream buffer handling
        glm::vec3 ambientLightGainRGB{1.0f, 1.0f, 1.0f};  //!< Ambient light RGB color gains
    };

    General general{};  //!< General params
    Options options{};  //!< Current state
};
