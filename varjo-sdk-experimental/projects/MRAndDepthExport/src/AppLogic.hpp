// Copyright 2019-2021 Varjo Technologies Oy. All rights reserved.

#pragma once

#include <memory>
#include <chrono>
#include <glm/glm.hpp>
#include <atomic>

#include "Globals.hpp"
#include "D3D11Renderer.hpp"
#include "MultiLayerView.hpp"
#include "HeadlessView.hpp"
#include "Scene.hpp"
#include "DataStreamer.hpp"
#include "PointCloud.hpp"
#include <Varjo_mr_experimental.h>
#include "IPCConnectorHost.hpp"

#include "AppState.hpp"
#include "GfxContext.hpp"


//! Application logic class
class AppLogic
{
public:
    //! Constructor
    AppLogic() = default;

    //! Destruictor
    ~AppLogic();

    // Disable copy and assign
    AppLogic(const AppLogic& other) = delete;
    AppLogic(const AppLogic&& other) = delete;
    AppLogic& operator=(const AppLogic& other) = delete;
    AppLogic& operator=(const AppLogic&& other) = delete;

    //! Initialize application
    bool init(VarjoExamples::GfxContext& context);

    //! Check for Varjo API events
    void checkEvents();

    //! Update application state
    void setState(const AppState& appState, bool force);

    //! Returns application state
    const AppState& getState() const { return m_appState; }

    //! Update application
    void update();

    //! Return data streamer instance
    VarjoExamples::DataStreamer& getStreamer() const { return *m_streamer; }

private:
    //! Toggle VST rendering
    void setVideoRendering(bool enabled);

    //! Handle mixed reality availablity
    void onMixedRealityAvailable(bool available, bool forceSetState);

    //! Handles new color stream frames from the data streamer (run in DataStreamer's worker thread)
    void onFrameReceived(const VarjoExamples::DataStreamer::Frame& frame);

    //! Handles new pointcloud snapshot data from the pointcloud (run in pointcloud's worker thread)
    void onPointCloudSnapshotReceived(const varjo_PointCloudSnapshotContent& content);

    //! Undistorts latest color stream frames, creates a depth and normal map, and exports the data with shared memory.
    void processFrame();

private:
    varjo_Session* m_session{nullptr};                           //!< Varjo session
    std::unique_ptr<VarjoExamples::HeadlessView> m_varjoView;  //!< Varjo layer ext view instance
    AppState m_appState{};                                       //!< Application state

    std::unique_ptr<VarjoExamples::DataStreamer> m_streamer;  //!< Data streamer instance

    struct FrameData {
        std::optional<varjo_DistortedColorFrameMetadata> metadata;                     //!< Color stream metadata
        std::array<std::optional<VarjoExamples::DataStreamer::Frame>, 2> colorFrames;  //!< Color stream stereo frames
        std::optional<VarjoExamples::DataStreamer::Frame> cubemapFrame;                //!< HDR cubemap frame
    };
    FrameData m_frameData;        //!< Latest frame data
    std::mutex m_frameDataMutex;  //!< Mutex for locking frame data

    varjo_PointCloudSnapshotContent m_pointCloudSnapshotContent; //!< Latest pointcloud snapshot
    std::mutex m_pointCloudSnapshotContentMutex; //!< Mutex for locking pointcloud snapshot data

    std::unique_ptr<PointCloud> m_pointCloud;  //!< Point Cloud instance

    std::unique_ptr<IPCConnectorHost> m_IPCConnector; //!< IPCConnector instance

    varjo_TextureFormat m_colorStreamFormat{varjo_TextureFormat_INVALID};  //!< Texture format for color stream

    std::thread processFrameThread; //!< Thread for processing the latest frame for shared memory export

    std::atomic<bool> processingFrame = false; //!< Bool for indicating if a frame is currently processed by the processFrameThread

    std::atomic<bool> validSnapshot = false; //!<Bool for indicating if a new pointcloud snapshot is available
};
