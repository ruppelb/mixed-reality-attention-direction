// Copyright 2019-2021 Varjo Technologies Oy. All rights reserved.

#include "AppLogic.hpp"

#include <cstdio>
#include <vector>
#include <string>
#include <fstream>
#include <set>
#include <thread>

#include <Varjo.h>
#include <Varjo_events.h>
#include <Varjo_mr.h>
#include <Varjo_d3d11.h>
#include <glm/gtx/matrix_decompose.hpp>
#include "glm/ext.hpp"

#include <opencv2/opencv.hpp>
#include <opencv2/cudawarping.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/ccalib/omnidir.hpp>

#include "D3D11MultiLayerView.hpp"

// VarjoExamples namespace contains simple example wrappers for using Varjo API features.
// These are only meant to be used in SDK example applications. In your own application,
// use your own production quality integration layer.
using namespace VarjoExamples;

namespace
{
    // Varjo application priority to force this as background behind other apps.
    // The default value is 0, so we go way less than that.
    constexpr int32_t c_appOrderBg = -1000;

}  // namespace
//---------------------------------------------------------------------------

AppLogic::~AppLogic()
{
    //let all threads join
    if (processFrameThread.joinable()) {
        processFrameThread.join();
    }

    //free IPCConnector resources
    m_IPCConnector.reset();

    //free point cloud resources
    m_pointCloud.reset();

    // Free data stremer resources
    m_streamer.reset();

    // Free view resources
    m_varjoView.reset();

    // Shutdown the varjo session. Can't check errors anymore after this.
    LOG_DEBUG("Shutting down Varjo session..");
    varjo_SessionShutDown(m_session);
    m_session = nullptr;
}

bool AppLogic::init(GfxContext& context)
{
    // Initialize the varjo session.
    LOG_DEBUG("Initializing Varjo session..");
    m_session = varjo_SessionInit();
    if (CHECK_VARJO_ERR(m_session) != varjo_NoError) {
        LOG_ERROR("Creating Varjo session failed.");
        return false;
    }

    {
        // Get graphics adapter used by Varjo session
        auto dxgiAdapter = D3D11MultiLayerView::getAdapter(m_session);

        // Init graphics
        context.init(dxgiAdapter.Get());

        // Set application Z order so that this application is on background behind other apps. Only
        // used if application runs in headless mode not intended to render anything by itself.
        varjo_SessionSetPriority(m_session, c_appOrderBg);
        CHECK_VARJO_ERR(m_session);

        // Create varjo multi layer view
        m_varjoView = std::make_unique<HeadlessView>(m_session);
    }

    // Create data streamer instance
    m_streamer = std::make_unique<DataStreamer>(m_session,std::bind(&AppLogic::onFrameReceived, this, std::placeholders::_1));

    //Create point cloud instance
    m_pointCloud = std::make_unique<PointCloud>(m_session, std::bind(&AppLogic::onPointCloudSnapshotReceived, this, std::placeholders::_1));

    //Create IPCConnector instance
    m_IPCConnector = std::make_unique<IPCConnectorHost>();

    // Check if Mixed Reality features are available.
    varjo_Bool mixedRealityAvailable = varjo_False;
    varjo_SyncProperties(m_session);
    CHECK_VARJO_ERR(m_session);
    if (varjo_HasProperty(m_session, varjo_PropertyKey_MRAvailable)) {
        mixedRealityAvailable = varjo_GetPropertyBool(m_session, varjo_PropertyKey_MRAvailable);
    }

    // Handle mixed reality availability
    onMixedRealityAvailable(mixedRealityAvailable == varjo_True, false);

    return true;
}

void AppLogic::setVideoRendering(bool enabled)
{
    varjo_MRSetVideoRender(m_session, enabled ? varjo_True : varjo_False);
    if (CHECK_VARJO_ERR(m_session) == varjo_NoError) {
        LOG_INFO("Video rendering: %s", enabled ? "ON" : "OFF");
    }
    m_appState.options.videoRenderingEnabled = enabled;
}

void AppLogic::setState(const AppState& appState, bool force)
{
    // Store previous state and set new one
    const auto prevState = m_appState;
    m_appState = appState;

    // Check for mixed reality availability
    if (!m_appState.general.mrAvailable) {
        // Toggle video rendering off
        if (m_appState.options.videoRenderingEnabled) {
            setVideoRendering(false);
        }
        return;
    }

    // React MR events
    if (force || appState.options.reactToConnectionEvents != prevState.options.reactToConnectionEvents) {
        LOG_INFO("Handling connection events: %s", appState.options.reactToConnectionEvents ? "ON" : "OFF");
    }

    // Data stream buffer handling
    if (force || appState.options.delayedBufferHandlingEnabled != prevState.options.delayedBufferHandlingEnabled) {
        m_streamer->setDelayedBufferHandlingEnabled(appState.options.delayedBufferHandlingEnabled);
        LOG_INFO("Buffer handling: %s", appState.options.delayedBufferHandlingEnabled ? "DELAYED" : "IMMEDIATE");
    }

    // Data stream: YUV
    if (force || appState.options.dataStreamColorEnabled != prevState.options.dataStreamColorEnabled) {
        const auto streamType = varjo_StreamType_DistortedColor;
        const auto streamFormat = m_colorStreamFormat;
        const auto streamChannels = varjo_ChannelFlag_Left | varjo_ChannelFlag_Right;
        varjo_ChannelFlag currentChannels = varjo_ChannelFlag_None;

        if (appState.options.dataStreamColorEnabled) {
            if (m_streamer->isStreaming(streamType, streamFormat, currentChannels)) {
                if (currentChannels == streamChannels) {
                    // If running stream channels match, match we want to stop it
                    //m_streamer->stopDataStream(streamType, streamFormat);
                    //do nothing
                } else {
                    // When color correction is enabled, stop stream and start new stream with data channels
                    //should never be the case in this implementation
                    LOG_INFO("Switching to color stream with data channels..");
                    m_streamer->stopDataStream(streamType, streamFormat);
                    m_streamer->startDataStream(streamType, streamFormat, streamChannels);
                }
            } else {
                // No streams running, just start our color data stream
                m_streamer->startDataStream(streamType, streamFormat, streamChannels);
            }

            //start frame export
            m_IPCConnector.get()->SetVSTFrameExport(true);

        } else {
            if (m_streamer->isStreaming(streamType, streamFormat, currentChannels)) {
                if (currentChannels == streamChannels) {
                    // If running stream channels match, match we want to stop it
                    m_streamer->stopDataStream(streamType, streamFormat);
                } else {
                    m_streamer->stopDataStream(streamType, streamFormat);
                }
            } else {
                // No streams running, just ignore
            }

            // Reset textures if disabled
            {
                std::lock_guard<std::mutex> streamLock(m_frameDataMutex);
                m_frameData.colorFrames = {};
            }
            //stop frame export
            m_IPCConnector.get()->SetVSTFrameExport(false);
        }

        // Write stream status back to state
        m_appState.options.dataStreamColorEnabled = m_streamer->isStreaming(streamType, streamFormat);
    }
}

void AppLogic::onFrameReceived(const DataStreamer::Frame& frame)
{
    const auto& streamFrame = frame.metadata.streamFrame;

    std::lock_guard<std::mutex> streamLock(m_frameDataMutex);
    switch (streamFrame.type) {
    case varjo_StreamType_DistortedColor: {
        // We update color stream metadata from first channel only
        if (frame.metadata.channelIndex == varjo_ChannelIndex_First) {
            m_frameData.metadata = streamFrame.metadata.distortedColor;
        }
        m_frameData.colorFrames[static_cast<size_t>(frame.metadata.channelIndex)] = frame;
    } break;
    case varjo_StreamType_EnvironmentCubemap: {
        // Do nothing since we do not need the cubemap frame
    } break;
    default: {
        LOG_ERROR("Unsupported stream type: %d", streamFrame.type);
        assert(false);
    } break;
    }
}

void AppLogic::onPointCloudSnapshotReceived(const varjo_PointCloudSnapshotContent& content)
{
    std::lock_guard<std::mutex> streamLock(m_pointCloudSnapshotContentMutex);

    if (content.pointCount > 0) {
        m_pointCloudSnapshotContent = content;
        validSnapshot = true;
    }
}

void AppLogic::processFrame()
{
    // Get latest frame data
    FrameData frameData{};
    {
        std::lock_guard<std::mutex> streamLock(m_frameDataMutex);

        frameData.metadata = m_frameData.metadata;

        // Move frame datas
        for (size_t ch = 0; ch < m_frameData.colorFrames.size(); ch++) {
            frameData.colorFrames[ch] = std::move(m_frameData.colorFrames[ch]);
            m_frameData.colorFrames[ch] = std::nullopt;
        }

        frameData.cubemapFrame = std::move(m_frameData.cubemapFrame);
        m_frameData.cubemapFrame = std::nullopt;
    }
    bool showFrameProcessingPictures = false;
    bool saveFrameProcessingPictures = false;

    // Get latest color frame
    for (size_t ch = 0; ch < frameData.colorFrames.size(); ch++) {
        if (frameData.colorFrames[ch].has_value()) {

            const auto& colorFrame = frameData.colorFrames[ch].value();

            // Skip conversions if the stream has only metadata.
            if (colorFrame.metadata.bufferMetadata.byteSize == 0) {
                continue;
            }

            const auto w = colorFrame.metadata.bufferMetadata.width;
            const auto h = colorFrame.metadata.bufferMetadata.height;
            const auto rowStride = w * 4;

            // Convert to RGBA
            std::vector<uint8_t> bufferRGBA(rowStride * h);

            //since conversion from NV12 to RGBA is not available in OpenCV + CUDA use already implemented conversion with CPU
            DataStreamer::convertToR8G8B8A(colorFrame.metadata.bufferMetadata, colorFrame.data.data(), bufferRGBA.data());
   
            static cv::cuda::GpuMat gpu_xmap, gpu_ymap;

            //convert buffer to OpenCV mat
            cv::Mat img = cv::Mat(h, w, CV_8UC4, bufferRGBA.data());
                
            if (showFrameProcessingPictures || saveFrameProcessingPictures) {
                cv::Mat converted;
                cv::cvtColor(img, converted, cv::COLOR_RGBA2BGR);

                //cv::Vec3b color = cv::Vec3b(0, 255, 0);
                //converted.at<cv::Vec3b>(cv::Point(floor(w / 2), floor(h / 2))) = color;

                if(showFrameProcessingPictures) {
                    cv::imshow("Distorted", converted);
                    cv::waitKey();
                }
                if (saveFrameProcessingPictures) {
                    if (converted.empty())
                    {
                        std::cerr << "Could not access distorted frame." << std::endl;
                    }
                    else {
                        // Save the frame into a file
                        imwrite("testDis.png", converted);
                    }
                }
            }

            //undistort image
            //calculate undistortion maps on cpu
            cv::Mat xmap(h, w, CV_32FC1);
            cv::Mat ymap(h, w, CV_32FC1);

            //principal and focal length values are relative to the image size. Therefore we must multiply them with the width/height.
            double fx = colorFrame.metadata.intrinsics.focalLengthX * w;
            double px = colorFrame.metadata.intrinsics.principalPointX * w;
            double fy = colorFrame.metadata.intrinsics.focalLengthY * h;
            double py = colorFrame.metadata.intrinsics.principalPointY * h;
            double skew = colorFrame.metadata.intrinsics.distortionCoefficients[2];
                
            //build intrinsic matrix from camera intrinsics
            cv::Mat K = (cv::Mat_<double>(3, 3) << fx, skew, px, 0.0, fy, py, 0.0, 0.0, 1.0);

            //build distortion vector
            double k1 = colorFrame.metadata.intrinsics.distortionCoefficients[0]; //radial 1
            double k2 = colorFrame.metadata.intrinsics.distortionCoefficients[1]; //radial 2
            double p1 = colorFrame.metadata.intrinsics.distortionCoefficients[4]; //tangential 1
            double p2 = colorFrame.metadata.intrinsics.distortionCoefficients[5]; //tangential 2
            double xi = colorFrame.metadata.intrinsics.distortionCoefficients[3]; //xi

            cv::Mat D = (cv::Mat_<double>(1, 4) << k1, k2, p1, p2);

            //build extrinsic rotation matrix
            cv::Mat extRot(3, 3, cv::DataType<double>::type);
            extRot.at<double>(0, 0) = colorFrame.metadata.extrinsics.value[0];
            extRot.at<double>(0, 1) = colorFrame.metadata.extrinsics.value[4];
            extRot.at<double>(0, 2) = colorFrame.metadata.extrinsics.value[8];

            extRot.at<double>(1, 0) = colorFrame.metadata.extrinsics.value[1];
            extRot.at<double>(1, 1) = colorFrame.metadata.extrinsics.value[5];
            extRot.at<double>(1, 2) = colorFrame.metadata.extrinsics.value[9];

            extRot.at<double>(2, 0) = colorFrame.metadata.extrinsics.value[2];
            extRot.at<double>(2, 1) = colorFrame.metadata.extrinsics.value[6];
            extRot.at<double>(2, 2) = colorFrame.metadata.extrinsics.value[10];
            extRot = extRot.inv();

            //New matrix for similar result as received by Varjo Undistorter class
            cv::Mat Knew = (cv::Mat_<double>(3, 3) << w * 0.595, 0.0, px, 0.0, h * 0.595, py, 0.0, 0.0, 1.0);

            cv::omnidir::initUndistortRectifyMap(K, D, xi, extRot, Knew, cv::Size(w, h), CV_32FC1, xmap, ymap, cv::omnidir::RECTIFY_PERSPECTIVE);
                
            gpu_xmap.upload(xmap);
            gpu_ymap.upload(ymap);

            //copy frame to gpu and remap pixels
            cv::cuda::GpuMat d_Mat_RGBA_dest;
            d_Mat_RGBA_dest.upload(img);
            cv::cuda::GpuMat d_Mat_RGBA_Src;
            d_Mat_RGBA_dest.copyTo(d_Mat_RGBA_Src); // cannot avoid one copy
            cv::cuda::remap(d_Mat_RGBA_Src, d_Mat_RGBA_dest, gpu_xmap, gpu_ymap, cv::INTER_CUBIC, cv::BORDER_CONSTANT, cv::Scalar(0.f, 0.f, 0.f, 0.f));
            cv::Mat result;
            d_Mat_RGBA_dest.download(result);

            if (showFrameProcessingPictures || saveFrameProcessingPictures) {
                cv::Mat converted;
                cv::cvtColor(result, converted, cv::COLOR_RGBA2BGR);

                //cv::Vec3b color = cv::Vec3b(0, 255, 0);
                //converted.at<cv::Vec3b>(cv::Point(floor(w / 2), floor(h / 2))) = color;

                if (showFrameProcessingPictures) {
                    cv::imshow("Undistorted", converted);
                    cv::waitKey();
                }
                if (saveFrameProcessingPictures) {
                    if (converted.empty())
                    {
                        std::cerr << "Could not access undistorted frame." << std::endl;
                    }
                    else {
                        // Save the frame into a file
                        imwrite("testUndis.png", converted);
                    }
                }
            }
                
            //save undistorted image
            bufferRGBA.assign(result.data, result.data + result.total() * result.channels());

            //update intrinsics and remove principal point and distortion parameters since they are not needed
            double newDist[6] = { 0.0,0.0,0.0,0.0,0.0,0.0 };
            varjo_CameraIntrinsics newInt{ 1,px / w,py / h,(Knew.at<double>(0,0)/w) + xi,(Knew.at<double>(1,1) / h) + xi,*newDist};

            //alter extrinsics to incorporate transform from HMD origin
            glm::mat4x4 ext = VarjoExamples::fromVarjoMatrix(colorFrame.metadata.extrinsics);
    
            glm::mat4x4 inverseExtrinsicsRotation = glm::diagonal4x4(glm::vec4{ 1.0, 1.0, 1.0, 1.0 });
            inverseExtrinsicsRotation[0][0] = ext[0][0];
            inverseExtrinsicsRotation[0][1] = ext[0][1];
            inverseExtrinsicsRotation[0][2] = ext[0][2];
            inverseExtrinsicsRotation[1][0] = ext[1][0];
            inverseExtrinsicsRotation[1][1] = ext[1][1];
            inverseExtrinsicsRotation[1][2] = ext[1][2];
            inverseExtrinsicsRotation[2][0] = ext[2][0];
            inverseExtrinsicsRotation[2][1] = ext[2][1];
            inverseExtrinsicsRotation[2][2] = ext[2][2];
            inverseExtrinsicsRotation = glm::inverse(inverseExtrinsicsRotation);

            glm::mat4x4 streamTransform = VarjoExamples::fromVarjoMatrix(colorFrame.metadata.streamTransform);
            glm::mat4x4 flipYZGLM = glm::diagonal4x4(glm::vec4{ 1.0, -1.0, -1.0, 1.0 });

            glm::mat4x4 pose = VarjoExamples::fromVarjoMatrix(colorFrame.metadata.streamFrame.hmdPose);
            pose = glm::inverse(pose);

            //* inverseExtrinsicsRotation *
            glm::mat4x4 newExtGLM = flipYZGLM * inverseExtrinsicsRotation * ext * flipYZGLM * streamTransform * pose;
            varjo_Matrix newExt = VarjoExamples::toVarjoMatrix(newExtGLM);

            /*
            std::vector<glm::vec3> points;
            points.push_back(glm::vec3(0.5 + colorFrame.metadata.streamTransform.value[12], 0.5 + colorFrame.metadata.streamTransform.value[13], -1));
            points.push_back(glm::vec3(0, 0, -0.1));
            points.push_back(glm::vec3(1.7005, 1.19175, -1.11016));
            
            std::vector<uint32_t> bufferDepth;
            float minDepth;
            float maxDepth;
            std::vector<uint8_t> normalMap;
            m_pointCloud->createDepthAndNormalMapFromPoints(glm::ivec2(w, h), newInt, newExt, points.data(), points.size(), bufferDepth, minDepth, maxDepth, normalMap);

            m_IPCConnector->updateColorFrameWithDepthAndNormals(static_cast<int>(ch), glm::ivec2(w, h), varjo_TextureFormat_R8G8B8A8_UNORM, 4, bufferRGBA, newInt, newExt, colorFrame.metadata.streamFrame.hmdPose, bufferDepth, minDepth, maxDepth, normalMap);
            if (bufferDepth.size() > 0) {
                cv::Mat depthImage = cv::Mat(h, w, CV_8UC1, bufferDepth.data());
                cv::imshow("Depth", depthImage);
                cv::waitKey();
            }
            else {
                std::cout << "Depth is invalid" << std::endl;
            }

            if (normalMap.size() > 0) {
                cv::Mat normalImage = cv::Mat(h, w, CV_8UC3, normalMap.data());
                cv::Mat convte;
                cv::cvtColor(normalImage, convte, cv::COLOR_RGB2BGR);
                cv::imshow("Normals", convte);
                cv::waitKey();
            }
            else {
                std::cout << "Normals invalid" << std::endl;
            }*/

            std::vector<uint32_t> bufferDepth;
            float minDepth;
            float maxDepth;
            std::vector<uint8_t> normalMap;

            if (!showFrameProcessingPictures && !saveFrameProcessingPictures) {
                //create depth and normal map
                m_pointCloud->createDepthAndNormalMap(glm::ivec2(w, h), newInt, newExt, bufferDepth, minDepth, maxDepth, normalMap);
                //m_pointCloud->createDepthAndNormalMapFromPoints(glm::ivec2(w, h), newInt, newExt, points.data(), points.size(), bufferDepth, minDepth, maxDepth, normalMap);
                //update frame data with depth and normal information
                m_IPCConnector->updateColorFrameWithDepthAndNormals(static_cast<int>(ch), glm::ivec2(w, h), varjo_TextureFormat_R8G8B8A8_UNORM, 4, bufferRGBA, newInt, newExt, colorFrame.metadata.streamFrame.hmdPose, bufferDepth, minDepth, maxDepth, normalMap);
            }
            else {
                //show image after each step of our processing
                std::vector<uint8_t> bufferDepth8;

                m_pointCloud->createDepthAndNormalMapForDisplay(glm::ivec2(w, h), newInt, newExt, bufferDepth8, minDepth, maxDepth, normalMap);

                if (bufferDepth8.size() > 0) {
                    cv::Mat depthImage = cv::Mat(h, w, CV_8UC1, bufferDepth8.data());

                    if (showFrameProcessingPictures) {
                        cv::imshow("Depth Image", depthImage);
                        cv::waitKey();
                    }

                    if (saveFrameProcessingPictures) {
                        if (depthImage.empty())
                        {
                            std::cerr << "Could not access depth frame." << std::endl;
                        }
                        else {
                            // Save the frame into a file
                            imwrite("testDepth.png", depthImage);
                        }
                    }
                }
                else {
                    std::cout << "Received depth map is invalid" << std::endl;
                }

                if (normalMap.size() > 0) {
                    cv::Mat normalImage = cv::Mat(h, w, CV_8UC3, normalMap.data());

                    cv::Mat converted;
                    cv::cvtColor(normalImage, converted, cv::COLOR_RGB2BGR);

                    if (showFrameProcessingPictures) {
                        cv::imshow("Normal Map", converted);
                        cv::waitKey();
                    }
                    if (saveFrameProcessingPictures) {
                        if (converted.empty())
                        {
                            std::cerr << "Could not access normal map frame." << std::endl;
                        }
                        else {
                            // Save the frame into a file
                            imwrite("testNormal.png", converted);
                        }
                    }
                }
                else {
                    std::cout << "Received normal map invalid" << std::endl;
                }

            }
            
        }
    }
    processingFrame = false;
}

void AppLogic::update()
{
    // Check for new mixed reality events
    checkEvents();

    // Handle delayed data stream buffers
    m_streamer->handleDelayedBuffers();

    // Sync frame
    m_varjoView->syncFrame();

    // Update frame time
    m_appState.general.frameTime += m_varjoView->getDeltaTime();
    m_appState.general.frameCount = m_varjoView->getFrameNumber();

    //only process new frame, if previous processing is done
    if (!processingFrame) {
        if (processFrameThread.joinable()) {
            processFrameThread.join();
        }
        processingFrame = true;
        processFrameThread = std::thread(&AppLogic::processFrame, this);
    }

    
    //update pointcloud
    std::lock_guard<std::mutex> streamLock(m_pointCloudSnapshotContentMutex);
    if (validSnapshot) {
        m_IPCConnector.get()->updatePointCloudSnapshotContent(m_pointCloudSnapshotContent);
        validSnapshot = false;
    }
}

void AppLogic::onMixedRealityAvailable(bool available, bool forceSetState)
{
    m_appState.general.mrAvailable = available;

    if (available) {
        // Update stuff here if needed

        // Get format for color stream
        m_colorStreamFormat = m_streamer->getFormat(varjo_StreamType_DistortedColor);

        if (m_appState.options.reactToConnectionEvents && !m_appState.options.videoRenderingEnabled) {
            LOG_INFO("Enabling video rendering on MR available event..");
            setVideoRendering(true);
        }

    } else {
        LOG_ERROR("Mixed Reality features not available.");

        // Update stuff here if needed
        if (m_appState.options.reactToConnectionEvents && m_appState.options.videoRenderingEnabled) {
            LOG_INFO("Disabling video rendering on MR unavailable event..");
            setVideoRendering(false);
        }      

    }

    //update point cloud status
    m_pointCloud.get()->onMixedRealityAvailable(available);
    //m_IPCConnector.get()->SetPointCloudDataExport(available);

    // Force set state when MR becomes active
    if (forceSetState) {
        setState(m_appState, true);
    }
}

void AppLogic::checkEvents()
{
    varjo_Bool ret = varjo_False;

    do {
        varjo_Event evt{};
        ret = varjo_PollEvent(m_session, &evt);
        CHECK_VARJO_ERR(m_session);

        if (ret == varjo_True) {
            switch (evt.header.type) {
                case varjo_EventType_MRDeviceStatus: {
                    switch (evt.data.mrDeviceStatus.status) {
                        // Occurs when Mixed Reality features are enabled
                        case varjo_MRDeviceStatus_Connected: {
                            LOG_INFO("EVENT: Mixed reality device status: %s", "Connected");
                            constexpr bool forceSetState = true;
                            onMixedRealityAvailable(true, forceSetState);
                        } break;
                        // Occurs when Mixed Reality features are disabled
                        case varjo_MRDeviceStatus_Disconnected: {
                            LOG_INFO("EVENT: Mixed reality device status: %s", "Disconnected");
                            constexpr bool forceSetState = false;
                            onMixedRealityAvailable(false, forceSetState);
                        } break;
                        default: {
                            // Ignore unknown status.
                        } break;
                    }
                } break;

                case varjo_EventType_DataStreamStart: {
                    LOG_INFO("EVENT: Data stream started: id=%d", static_cast<int>(evt.data.dataStreamStart.streamId));
                } break;

                case varjo_EventType_DataStreamStop: {
                    LOG_INFO("EVENT: Data stream stopped: id=%d", static_cast<int>(evt.data.dataStreamStop.streamId));
                } break;

                default: {
                    // Ignore unknown event.
                } break;
            }
        }
    } while (ret);
}
