// Copyright 2019-2021 Varjo Technologies Oy. All rights reserved.

#include "PointCloud.hpp"
#include <cstdio>
#include <vector>
#include <string>
#include <limits>
#include <map>
#include <iostream>
#include <glm/gtx/matrix_transform_2d.hpp>
#include "glm/ext.hpp"

// VarjoExamples namespace contains simple example wrappers for using Varjo API features.
// These are only meant to be used in SDK example applications. In your own application,
// use your own production quality integration layer.
using namespace VarjoExamples;

PointCloud::PointCloud(varjo_Session* session, const std::function<void(const varjo_PointCloudSnapshotContent&)>& onPointCloudCallback)
    :m_session(session), m_onPointCloudCallback(onPointCloudCallback)
{}

PointCloud::~PointCloud()
{
    //stop thread
    updateSnapshot = false;

    //wait for thread to join
    if (PointCloudThread.joinable()) {
        PointCloudThread.join();
    }

    //end capturing snapshot
    if (varjo_MRGetPointCloudSnapshotStatus(m_session, m_PointCloudSnapshotId) != 0) {
        varjo_MREndPointCloudSnapshot(m_session, m_PointCloudSnapshotId);
    }

    m_PointCloudSnapshotId = varjo_PointCloudSnapshotId_Invalid;

    // Disable 3D reconstruction
    varjo_MRSetReconstruction(m_session, varjo_False);
    CHECK_VARJO_ERR(m_session);
}

void PointCloud::update()
{

    //update point cloud snapshot
    while (updateSnapshot) {

        //only update snapshot if mixed reality is available
        LOG_DEBUG("Trying to update Snapshot...");
        varjo_PointCloudSnapshotStatus snapshotStatus = varjo_MRGetPointCloudSnapshotStatus(m_session, m_PointCloudSnapshotId);
        if(snapshotStatus == 0) {
            //need to begin new snapshot
            LOG_DEBUG("Beginning new snapshot...");
            m_PointCloudSnapshotId = varjo_MRBeginPointCloudSnapshot(m_session);
        }
        else if (snapshotStatus == 1) {
            //ignore since snapshot is not ready
            LOG_DEBUG("Snapshot not ready...");
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }   
        else if(snapshotStatus == 2) {
            //save snapshot content, release and end snapshot
            LOG_DEBUG("Snapshot ready, saving content");
            std::lock_guard<std::mutex> streamLock(pointCloudMutex);
            m_latestSnapshot = varjo_PointCloudSnapshotContent();

            varjo_MRGetPointCloudSnapshotContent(m_session, m_PointCloudSnapshotId, &m_latestSnapshot);

            if (m_onPointCloudCallback && m_latestSnapshot.pointCount > 0) {
                //call callback
                m_onPointCloudCallback(m_latestSnapshot);
            }

            LOG_DEBUG("Releasing snapshot..");
            varjo_MRReleasePointCloudSnapshot(m_session, m_PointCloudSnapshotId);
            LOG_DEBUG("Snapshot ended.");
            varjo_MREndPointCloudSnapshot(m_session, m_PointCloudSnapshotId);

            //begin new snapshot
            LOG_DEBUG("Beginning new snapshot...");
            m_PointCloudSnapshotId = varjo_MRBeginPointCloudSnapshot(m_session);
        }
    }
}

void PointCloud::onMixedRealityAvailable(bool available)
{
    mrAvailable = available;

    if (available && !updateSnapshot) {

        // When MR becomes available, Enable 3D reconstruction

        // Enable 3D reconstruction
        varjo_MRSetReconstruction(m_session, varjo_True);
        CHECK_VARJO_ERR(m_session);

        //Begin capturing Snapshot
        m_PointCloudSnapshotId = varjo_MRBeginPointCloudSnapshot(m_session);

        updateSnapshot = available;

        //start PointCloudThread
        PointCloudThread = std::thread(&PointCloud::update, this);

        LOG_INFO("Started capturing pointcloud snapshots");

    }
    else if(!available && updateSnapshot) {
        LOG_ERROR("Mixed Reality features not available.");

        // When MR becomes unavailable, disable 3D reconstruction

        //stop thread
        updateSnapshot = available;

        //wait for thread to join
        if (PointCloudThread.joinable()) {
            PointCloudThread.join();
        }

        //end capturing snapshot
        if (varjo_MRGetPointCloudSnapshotStatus(m_session, m_PointCloudSnapshotId) != 0) {
            varjo_MREndPointCloudSnapshot(m_session, m_PointCloudSnapshotId);
        }

        m_PointCloudSnapshotId = varjo_PointCloudSnapshotId_Invalid;

        // Disable 3D reconstruction
        varjo_MRSetReconstruction(m_session, varjo_False);
        CHECK_VARJO_ERR(m_session);

        LOG_INFO("Stopped capturing pointcloud snapshots");

    }
}

/**
 * A 3D point with an id and a surface normal 
 */
struct IdentifiedPoint3D {
    uint32_t id;
    glm::vec3 coord;
    glm::vec3 normal;
public:
    IdentifiedPoint3D(uint32_t idIn, glm::vec3 coordIn)
        :id(idIn), coord(coordIn)
    {};
    IdentifiedPoint3D(uint32_t idIn, glm::vec3 coordIn, glm::vec3 normalIn)
        :id(idIn), coord(coordIn), normal(normalIn)
    {};
};

/**
 * An id of a pointcloud point with a distance from the camera and a surface normal
 */
struct IdAndDepth {
    uint32_t id;
    float dist;
    glm::vec3 normal;
public:
    IdAndDepth(uint32_t idIn, float distIn, glm::vec3 normalIn)
        :id(idIn), dist(distIn), normal(normalIn)
    {}
    IdAndDepth(uint32_t idIn, float distIn)
        :id(idIn), dist(distIn)
    {}
    IdAndDepth()
        :id(std::numeric_limits<uint32_t>::max()), dist(0.0), normal(glm::vec3(0,0,0))
    {
    //setting id default to maximal uint32 value, because the maximal used id from varjo struct is uint32 >> 8 and so this value can't be reached when the id is valid
    }
};

void PointCloud::createDepthAndNormalMap(const glm::ivec2 resolution, varjo_CameraIntrinsics intrinsics, varjo_Matrix extrinsics, std::vector<uint32_t>& depthMap, float& minDist, float& maxDist, std::vector<uint8_t>& normalMap)
{
    //only generate depth and normal maps when snapshot is currently generated.
    if (updateSnapshot) {
        //Acquire latest pointcloud snapshot
        varjo_PointCloudSnapshotContent latestContent = varjo_PointCloudSnapshotContent();
        {
            std::lock_guard<std::mutex> streamLock(pointCloudMutex);
            latestContent = m_latestSnapshot;
        }

        //only generate depth and normal maps when we have points
        if (latestContent.pointCount > 0) {
            maxDist = std::numeric_limits<float>::min();
            minDist = std::numeric_limits<float>::max();

            //build intrinsic matrix
            const glm::mat3x3 offset = glm::translate(glm::mat3(1.0f), glm::vec2((intrinsics.principalPointX - 0.5) * -2.0, (0.5 - intrinsics.principalPointY) * -2.0));
            const glm::mat3x3 scale = glm::scale(glm::mat3(1.0f), glm::vec2(intrinsics.focalLengthX, intrinsics.focalLengthY));
            glm::mat3x3 intrinsicsMat = offset * scale;

            //get camera center point direction (in varjo camera space negative z goes forward, so in this case it would just be (0,0,-1))
            glm::vec3 center(0, 0, -1.0);
            glm::vec3 centerNormed = glm::normalize(center); //not necessary, but included for completion

            //get extrinsic matrix
            glm::mat4x4 extrinsicsGLM = VarjoExamples::fromVarjoMatrix(extrinsics);
            std::vector<IdentifiedPoint3D> pointCameraCoordinates;

            for (int i = 0; i < latestContent.pointCount; i++) {

                //unpack pointcloud point coordinates and normals
                varjo_PointCloudPoint p = latestContent.points[i];
                uint32_t id = (p.indexConfidence >> 8);
                uint32_t confidence = (p.indexConfidence & 0xFF);
                glm::vec2 positionXY = glm::unpackHalf2x16(p.positionXY);
                glm::vec2 positionZradius = glm::unpackHalf2x16(p.positionZradius);
                glm::vec2 normalXY = glm::unpackHalf2x16(p.normalXY);
                glm::vec2 normalZcolorR = glm::unpackHalf2x16(p.normalZcolorR);

                //convert to camera coordinates
                glm::vec4 vec4P(glm::vec3(positionXY.x, positionXY.y, positionZradius.x), 1);
                glm::vec4 cameraP = extrinsicsGLM * vec4P;
                glm::vec3 vec3P = glm::vec3(cameraP.x / cameraP.w, cameraP.y / cameraP.w, cameraP.z / cameraP.w);

                //filter points based on confidence here
                if (confidence < 1) {
                    continue;
                }

                //compute dot product with camera vector to sort out points behind the camera/that can't be in view
                float d = glm::dot(centerNormed, vec3P);

                if (d >= 0) {
                    //keep point
                    pointCameraCoordinates.push_back(IdentifiedPoint3D(id, vec3P, glm::vec3(normalXY.x, normalXY.y, normalZcolorR.x))); //vec3n
                }
            }


            //project the rest of the points onto the camera plane and check if resulting u v values are within the width and height specified by the image resolution
            //set value of resulting pixel to distance of the point from camera (clamped between minimal and maximal distance)
            //if multiple values result in the same pixel, then keep value of closest point (~like a z buffer)

            //structure to save per pixel depth values
            std::vector<std::vector<IdAndDepth>> pixelToDepth(resolution.y, std::vector <IdAndDepth>(resolution.x));

            for (IdentifiedPoint3D p : pointCameraCoordinates) {

                //project 3D point coordinates on the camera plane
                glm::vec3 projP = intrinsicsMat * p.coord;

                //use -z coordinate because coordinate system has negative z direction as forward axis
                glm::vec2 projP2D = glm::vec2(projP.x / -projP.z, projP.y / -projP.z);

                //calculate corresponding pixel coordinate
                glm::ivec2 pixel;
                pixel.x = glm::floor(((projP2D.x + 1.0) * 0.5 * resolution.x) - 0.5);
                pixel.y = glm::floor(((1.0 - projP2D.y) * 0.5 * resolution.y) - 0.5);

                if (pixel.x >= 0 && pixel.x < resolution.x && pixel.y >= 0 && pixel.y < resolution.y) {
                    //if point is within our defined width and height, keep value;
                    
                    //compute distance to camera (in camera coordinate system the camera is in the origin)
                    float dist = glm::distance(glm::vec3(0, 0, 0), p.coord);

                    //save distance value in array at pixel location
                    if (pixelToDepth[pixel.y][pixel.x].id != std::numeric_limits<uint32_t>::max()) {
                        //there is already a point projected to this pixel, select closest of both
                        float currentDist = pixelToDepth[pixel.y][pixel.x].dist;

                        if (dist < currentDist) {
                            //save new depth value
                            pixelToDepth[pixel.y][pixel.x] = IdAndDepth(p.id, dist);
                        }
                        else {
                            //keep old depth and id
                        }
                    }
                    else {
                        //save id to pixel
                        pixelToDepth[pixel.y][pixel.x] = IdAndDepth(p.id, dist,p.normal);
                    }
                }
            }

            //calculate max and min distances
            for (std::vector<IdAndDepth> vec : pixelToDepth) {
                for (IdAndDepth pixel : vec) {

                    if (pixel.id != std::numeric_limits<uint32_t>::max()) {
                        //save overall min and max distances for clamping the range between 0.0 and 1.0 later
                        if (pixel.dist > maxDist) {
                            maxDist = pixel.dist;
                        }

                        if (pixel.dist < minDist) {
                            minDist = pixel.dist;
                        }

                    }
                }
            }

            //initialize vectors
            depthMap = std::vector<uint32_t>();

            normalMap = std::vector<uint8_t>();

            //decrease minDist, so no pixel with a value that was set above has 0 as value and we know which pixels are invalid
            if (minDist - 0.01 >= 0.0) {
                minDist = minDist - 0.01f;
            }

            //transform distance values to depth values between 0.0 and 1.0 and save depth image data
            for (std::vector<IdAndDepth> vec : pixelToDepth) {
                for (IdAndDepth pixel : vec) {

                    if (pixel.id != std::numeric_limits<uint32_t>::max()) {
                        //pixel value was set in the procedure above. At least one pointcloud point could be projected to the pixel
                        
                        //calculate depth value between 0.0 an 1.0
                        float color = (pixel.dist - minDist) / (maxDist - minDist);

                        if (color <= 1.0 && color >= 0.0) {
                            
                            //convert to int32_t
                            uint32_t convertedColor;
                            if (color >= 1.0) {
                                convertedColor = std::numeric_limits<uint32_t>::max();
                            }
                            else {
                                convertedColor = floor(color * 4294967296);
                            }

                            //save depth to depht map
                            depthMap.push_back(convertedColor);

                            //save normal to normal map
                            float normalX = pixel.normal.x;
                            float normalY = pixel.normal.y;
                            float normalZ = pixel.normal.z;

                            //shift to 0-1 range
                            normalX = (normalX * 0.5) + 0.5;
                            normalY = (normalY * 0.5) + 0.5;
                            normalZ = (normalZ * 0.5) + 0.5;

                            //convert to uint_8
                            uint8_t intX = glm::packUnorm1x8(normalX);
                            uint8_t intY = glm::packUnorm1x8(normalY);
                            uint8_t intZ = glm::packUnorm1x8(normalZ);
                            //r = x
                            normalMap.push_back(intX);
                            //g = y
                            normalMap.push_back(intY);
                            //b = z
                            normalMap.push_back(intZ);


                        }
                        else {
                            //invalid value. set pixel to black
                            depthMap.push_back(0);
                            //r = x
                            normalMap.push_back(0);
                            //g = y
                            normalMap.push_back(0);
                            //b = z
                            normalMap.push_back(0);
                        }
                    }
                    else {
                        //no pixel value was set above. No pointcloud point could be projected to the pixel. Set color to black/0
                        depthMap.push_back(0);
                        //r = x
                        normalMap.push_back(0);
                        //g = y
                        normalMap.push_back(0);
                        //b = z
                        normalMap.push_back(0);
                    }

                }
            }
        }
    }
}

void PointCloud::createDepthAndNormalMapForDisplay(const glm::ivec2 resolution, varjo_CameraIntrinsics intrinsics, varjo_Matrix extrinsics, std::vector<uint8_t>& depthMap, float& minDist, float& maxDist, std::vector<uint8_t>& normalMap)
{
    //only generate depth and normal maps when snapshot is currently generated.
    if (updateSnapshot) {
        //Acquire latest pointcloud snapshot
        varjo_PointCloudSnapshotContent latestContent = varjo_PointCloudSnapshotContent();
        {
            std::lock_guard<std::mutex> streamLock(pointCloudMutex);
            latestContent = m_latestSnapshot;
        }

        //only generate depth and normal maps when we have points
        if (latestContent.pointCount > 0) {
            maxDist = std::numeric_limits<float>::min();
            minDist = std::numeric_limits<float>::max();

            //build intrinsic matrix
            const glm::mat3x3 offset = glm::translate(glm::mat3(1.0f), glm::vec2((intrinsics.principalPointX - 0.5) * -2.0, (0.5 - intrinsics.principalPointY) * -2.0));
            const glm::mat3x3 scale = glm::scale(glm::mat3(1.0f), glm::vec2(intrinsics.focalLengthX, intrinsics.focalLengthY));
            glm::mat3x3 intrinsicsMat = offset * scale;

            //get camera center point direction (in varjo camera space negative z goes forward, so in this case it would just be (0,0,-1))
            glm::vec3 center(0, 0, -1.0);
            glm::vec3 centerNormed = glm::normalize(center); //not necessary, but included for completion

            //get extrinsic matrix
            glm::mat4x4 extrinsicsGLM = VarjoExamples::fromVarjoMatrix(extrinsics);
            std::vector<IdentifiedPoint3D> pointCameraCoordinates;

            for (int i = 0; i < latestContent.pointCount; i++) {

                //unpack pointcloud point coordinates and normals
                varjo_PointCloudPoint p = latestContent.points[i];
                uint32_t id = (p.indexConfidence >> 8);
                uint32_t confidence = (p.indexConfidence & 0xFF);
                glm::vec2 positionXY = glm::unpackHalf2x16(p.positionXY);
                glm::vec2 positionZradius = glm::unpackHalf2x16(p.positionZradius);
                glm::vec2 normalXY = glm::unpackHalf2x16(p.normalXY);
                glm::vec2 normalZcolorR = glm::unpackHalf2x16(p.normalZcolorR);

                //convert to camera coordinates
                glm::vec4 vec4P(glm::vec3(positionXY.x, positionXY.y, positionZradius.x), 1);
                glm::vec4 cameraP = extrinsicsGLM * vec4P;
                glm::vec3 vec3P = glm::vec3(cameraP.x / cameraP.w, cameraP.y / cameraP.w, cameraP.z / cameraP.w);

                //filter points based on confidence here
                if (confidence < 1) {
                    continue;
                }


                //compute dot product with camera vector to sort out points behind the camera/that can't be in view
                float d = glm::dot(centerNormed, vec3P);

                if (d >= 0) {
                    //keep point
                    pointCameraCoordinates.push_back(IdentifiedPoint3D(id, vec3P, glm::vec3(normalXY.x, normalXY.y, normalZcolorR.x))); //vec3n
                }
            }


            //project the rest of the points onto the camera plane and check if resulting u v values are within the width and height specified by the image resolution
            //set value of resulting pixel to distance of the point from camera (clamped between minimal and maximal distance)
            //if multiple values result in the same pixel, then keep value of closest point (~like a z buffer)

            //structure to save per pixel depth values
            std::vector<std::vector<IdAndDepth>> pixelToDepth(resolution.y, std::vector <IdAndDepth>(resolution.x));

            for (IdentifiedPoint3D p : pointCameraCoordinates) {

                //project 3D point coordinates on the camera plane
                glm::vec3 projP = intrinsicsMat * p.coord;

                //use -z coordinate because coordinate system has negative z direction as forward axis
                glm::vec2 projP2D = glm::vec2(projP.x / -projP.z, projP.y / -projP.z);

                //calculate corresponding pixel coordinate
                glm::ivec2 pixel;
                pixel.x = glm::floor(((projP2D.x + 1.0) * 0.5 * resolution.x) - 0.5);
                pixel.y = glm::floor(((1.0 - projP2D.y) * 0.5 * resolution.y) - 0.5);

                if (pixel.x >= 0 && pixel.x < resolution.x && pixel.y >= 0 && pixel.y < resolution.y) {
                    //if point is within our defined width and height, keep value;

                    //compute distance to camera (in camera coordinate system the camera is in the origin)
                    float dist = glm::distance(glm::vec3(0, 0, 0), p.coord);

                    //save distance value in array at pixel location
                    if (pixelToDepth[pixel.y][pixel.x].id != std::numeric_limits<uint32_t>::max()) {
                        //there is already a point projected to this pixel, select closest of both
                        float currentDist = pixelToDepth[pixel.y][pixel.x].dist;

                        if (dist < currentDist) {
                            //save new depth value
                            pixelToDepth[pixel.y][pixel.x] = IdAndDepth(p.id, dist);
                        }
                        else {
                            //keep old depth and id
                        }
                    }
                    else {
                        //save id to pixel
                        pixelToDepth[pixel.y][pixel.x] = IdAndDepth(p.id, dist, p.normal);
                    }
                }
            }

            //calculate max and min distances
            for (std::vector<IdAndDepth> vec : pixelToDepth) {
                for (IdAndDepth pixel : vec) {

                    if (pixel.id != std::numeric_limits<uint32_t>::max()) {
                        //save overall min and max distances for clamping the range between 0.0 and 1.0 later
                        if (pixel.dist > maxDist) {
                            maxDist = pixel.dist;
                        }

                        if (pixel.dist < minDist) {
                            minDist = pixel.dist;
                        }

                    }
                }
            }

            //initialize vectors
            depthMap = std::vector<uint8_t>();

            normalMap = std::vector<uint8_t>();

            //decrease minDist, so no pixel with a value that was set above has 0 as value and we know which pixels are invalid
            if (minDist - 0.01 >= 0.0) {
                minDist = minDist - 0.01f;
            }

            //transform distance values to depth values between 0.0 and 1.0 and save depth image data
            for (std::vector<IdAndDepth> vec : pixelToDepth) {
                for (IdAndDepth pixel : vec) {

                    if (pixel.id != std::numeric_limits<uint32_t>::max()) {
                        //pixel value was set in the procedure above. At least one pointcloud point could be projected to the pixel

                        //calculate depth value between 0.0 an 1.0
                        float color = (pixel.dist - minDist) / (maxDist - minDist);

                        if (color <= 1.0 && color >= 0.0) {

                            //convert to uint38_t
                            uint32_t convertedColor;
                            if (color >= 1.0) {
                                convertedColor = std::numeric_limits<uint8_t>::max();
                            }
                            else {
                                convertedColor = floor(color * 256);
                            }

                            //save depth to depht map
                            depthMap.push_back(convertedColor);

                            //save normal to normal map
                            float normalX = pixel.normal.x;
                            float normalY = pixel.normal.y;
                            float normalZ = pixel.normal.z;

                            //shift to 0-1 range
                            normalX = (normalX * 0.5) + 0.5;
                            normalY = (normalY * 0.5) + 0.5;
                            normalZ = (normalZ * 0.5) + 0.5;

                            //convert to uint_8
                            uint8_t intX = glm::packUnorm1x8(normalX);
                            uint8_t intY = glm::packUnorm1x8(normalY);
                            uint8_t intZ = glm::packUnorm1x8(normalZ);
                            //r = x
                            normalMap.push_back(intX);
                            //g = y
                            normalMap.push_back(intY);
                            //b = z
                            normalMap.push_back(intZ);


                        }
                        else {
                            //invalid value. set pixel to black
                            depthMap.push_back(0);
                            //r = x
                            normalMap.push_back(0);
                            //g = y
                            normalMap.push_back(0);
                            //b = z
                            normalMap.push_back(0);
                        }
                    }
                    else {
                        //no pixel value was set above. No pointcloud point could be projected to the pixel. Set color to black/0
                        depthMap.push_back(0);
                        //r = x
                        normalMap.push_back(0);
                        //g = y
                        normalMap.push_back(0);
                        //b = z
                        normalMap.push_back(0);
                    }

                }
            }
        }
    }
}

void PointCloud::createDepthAndNormalMapFromPoints(const glm::ivec2 resolution, varjo_CameraIntrinsics intrinsics, varjo_Matrix extrinsics, glm::vec3* points, int pointcount, std::vector<uint32_t>& depthMap, float& minDist, float& maxDist, std::vector<uint8_t>& normalMap)
{
    if (pointcount > 0) {

        maxDist = std::numeric_limits<float>::min();
        minDist = std::numeric_limits<float>::max();

        //build intrinsic matrix
        const glm::mat3x3 offset = glm::translate(glm::mat3(1.0f), glm::vec2((intrinsics.principalPointX - 0.5) * -2.0, (0.5 - intrinsics.principalPointY) * -2.0));
        const glm::mat3x3 scale = glm::scale(glm::mat3(1.0f), glm::vec2(intrinsics.focalLengthX, intrinsics.focalLengthY));
        glm::mat3x3 intrinsicsMat = offset * scale;


        //get camera center point direction (in varjo camera space negative z goes forward, so in this case it would just be (0,0,-1))
        glm::vec3 center(0, 0, -1.0);
        glm::vec3 centerNormed = glm::normalize(center); //not necessary, but included for completion

        //get extrinsic matrix
        glm::mat4x4 extrinsicsGLM = VarjoExamples::fromVarjoMatrix(extrinsics);
        std::vector<IdentifiedPoint3D> pointCameraCoordinates;


        for (int i = 0; i < pointcount; i++) {

            glm::vec3 point = points[i];

            //convert to camera coordinates
            glm::vec4 vec4P(point, 1);
            glm::vec4 cameraP = extrinsicsGLM * vec4P;
            glm::vec3 vec3P = glm::vec3(cameraP.x / cameraP.w, cameraP.y / cameraP.w, cameraP.z / cameraP.w);

            //compute dot product with camera vector to sort out points behind the camera/that can't be in view
            float d = glm::dot(centerNormed, vec3P);
            if (d >= 0) {
                //keep point
                pointCameraCoordinates.push_back(IdentifiedPoint3D(i, vec3P));
            }
        }


        //project the rest of the points onto the camera plane and check if resulting u v values are within the width and height specified by the image resolution
        //set value of resulting pixel to distance of the point from camera (clamped between minimal and maximal distance)
        //if multiple values result in the same pixel, then keep value of closest point (~like a z buffer)

        //structure to save per pixel depth values
        std::vector<std::vector<IdAndDepth>> pixelToDepth(resolution.y, std::vector <IdAndDepth>(resolution.x));

        for (IdentifiedPoint3D p : pointCameraCoordinates) {

            //project 3D point coordinates on the camera plane
            glm::vec3 projP = intrinsicsMat * p.coord;

            //use -z coordinate because coordinate system has negative z direction as forward axis
            glm::vec2 projP2D = glm::vec2(projP.x / -projP.z, projP.y / -projP.z);

            //calculate corresponding pixel coordinate
            int x;
            int y;
            x = glm::floor(((projP2D.x + 1.0) * 0.5 * resolution.x) - 0.5);
            y = glm::floor(((1.0 - projP2D.y) * 0.5 * resolution.y) - 0.5);

            if (x >= 0 && x < resolution.x && y >= 0 && y < resolution.y) {
                //if point is within our defined width and height, keep value;
                
                //compute distance to camera (in camera coordinate system the camera is in the origin)
                float dist = glm::distance(glm::vec3(0, 0, 0), p.coord);

                //save overall min and max distances for clamping the range between 0.0 and 1.0 later
                if (dist > maxDist) {
                    maxDist = dist;
                }

                if (dist < minDist) {
                    minDist = dist;
                }

                //save distance value in array at pixel location
                if (pixelToDepth[y][x].id != std::numeric_limits<uint32_t>::max()) {
                    //there is already a point projected to this pixel, select closest of both
                    float currentDist = pixelToDepth[y][x].dist;

                    if (dist < currentDist) {
                        //save new depth value
                        pixelToDepth[y][x] = IdAndDepth(p.id, dist);
                    }
                    else {
                        //keep old depth and id
                    }
                }
                else {
                    //save id to pixel
                    pixelToDepth[y][x] = IdAndDepth(p.id, dist);
                }
            }
        }

        //initialize vectors
        depthMap = std::vector<uint32_t>();

        normalMap = std::vector<uint8_t>();

        //decrease minDist, so no pixel with a value that was set above has 0 as value and we know which pixels are invalid
        if (minDist - 0.01 >= 0.0) {
            minDist = minDist - 0.01f;
        }

        //transform distance values to depth values between 0.0 and 1.0 and save depth image data
        for (std::vector<IdAndDepth> vec : pixelToDepth) {
            for (IdAndDepth pixel : vec) {

                if (pixel.id != std::numeric_limits<uint32_t>::max()) {
                    //pixel value was set in the procedure above. At least one pointcloud point could be projected to the pixel
                    
                    //calculate depth value between 0.0 an 1.0
                    float color = (pixel.dist - minDist) / (maxDist - minDist);

                    if (color <= 1.0 && color >= 0.0) {
                        
                        //convert to int32_t
                        uint32_t convertedColor;
                        if (color >= 1.0) {
                            convertedColor = std::numeric_limits<uint32_t>::max();
                        }
                        else {
                            convertedColor = floor(color * 4294967296);
                        }

                        //save depth to depht map
                        depthMap.push_back(convertedColor);

                        //save normal to normal map
                        float normalX = pixel.normal.x;
                        float normalY = pixel.normal.y;
                        float normalZ = pixel.normal.z;

                        //shift to 0-1 range
                        normalX = (normalX * 0.5) + 0.5;
                        normalY = (normalY * 0.5) + 0.5;
                        normalZ = (normalZ * 0.5) + 0.5;

                        //convert to uint_8
                        uint8_t intX = glm::packUnorm1x8(normalX); //convertToUint8(normalX);
                        uint8_t intY = glm::packUnorm1x8(normalY);
                        uint8_t intZ = glm::packUnorm1x8(normalZ);
                        //r = x
                        normalMap.push_back(intX);
                        //g = y
                        normalMap.push_back(intY);
                        //b = z
                        normalMap.push_back(intZ);


                    }
                    else {
                        //invalid value. set pixel to black
                        depthMap.push_back(0);
                        //r = x
                        normalMap.push_back(128);
                        //g = y
                        normalMap.push_back(128);
                        //b = z
                        normalMap.push_back(255);
                    }
                }
                else {
                    //no pixel value was set above. No pointcloud point could be projected to the pixel. Set color to black/0
                    depthMap.push_back(2147483648);
                    //r = x
                    normalMap.push_back(128);
                    //g = y
                    normalMap.push_back(128);
                    //b = z
                    normalMap.push_back(255);
                }

            }
        }
        //maybe improve sparse depth map using these approaches:
        //https://github.com/kujason/ip_basic or https://github.com/BerensRWU/DenseMap

        //calculate normals in new depth values with:
        //https://stackoverflow.com/questions/34644101/calculate-surface-normals-from-depth-image-using-neighboring-pixels-cross-produc
    }
}
