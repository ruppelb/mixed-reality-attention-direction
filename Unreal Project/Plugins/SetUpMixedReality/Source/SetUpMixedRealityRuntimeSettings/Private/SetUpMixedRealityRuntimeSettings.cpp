#include "SetUpMixedRealityRuntimeSettings.h"

#include "Modules/ModuleInterface.h"
#include "Modules/ModuleManager.h"

DEFINE_LOG_CATEGORY(LogSetUpMixedRealitySettings);

IMPLEMENT_MODULE(FDefaultModuleImpl, SetUpMixedRealityRuntimeSettings);