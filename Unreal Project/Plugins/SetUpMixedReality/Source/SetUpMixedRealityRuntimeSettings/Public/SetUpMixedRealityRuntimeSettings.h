#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "UObject/Object.h"
#include "Engine/EngineTypes.h"

DECLARE_LOG_CATEGORY_EXTERN(LogSetUpMixedRealitySettings, Log, All);

#include "SetUpMixedRealityRuntimeSettings.generated.h"

UENUM()
enum class EnvironmentBlendMode
{
    Opaque = 0,
    AlphaBlend = 1
};

UCLASS(config = Engine, defaultconfig)
class SETUPMIXEDREALITYRUNTIMESETTINGS_API USetUpMixedRealityRuntimeSettings : public UObject
{
public:
    GENERATED_BODY()

    UPROPERTY(GlobalConfig, EditAnywhere, Category = "Environment Blend Mode", Meta = (ConfigRestartRequired = true, DisplayName = "Preferred Environment Blend Mode (Requires Restart)", Tooltip = "Select 'Opaque' for VR and 'Alpha Blend' for MR. Editor restart required."))
        EnvironmentBlendMode PreferredEnvironmentBlendMode = EnvironmentBlendMode::Opaque;

    UPROPERTY(GlobalConfig, EditAnywhere, Category = "Foveated Rendering", Meta = (DisplayName = "Enable Foveated Rendering", Tooltip = "Enable Foveated Rendering. Please note that this option is usually required for high performance, high quality rendering."))
        bool FoveatedRendering = true;
};