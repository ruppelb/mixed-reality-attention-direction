using UnrealBuildTool;

public class SetUpMixedRealityRuntimeSettings : ModuleRules
{
    public SetUpMixedRealityRuntimeSettings(ReadOnlyTargetRules Target) : base(Target)
    {
        PrivateDependencyModuleNames.AddRange(
            new string[]
            {
                "Core",
                "CoreUObject",
                "Engine"
            }
        );

        if (Target.Type == TargetRules.TargetType.Editor || Target.Type == TargetRules.TargetType.Program)
        {
            PrivateDependencyModuleNames.AddRange(
                new string[]
                {
                    "TargetPlatform"
                }
            );
        }
    }
}
