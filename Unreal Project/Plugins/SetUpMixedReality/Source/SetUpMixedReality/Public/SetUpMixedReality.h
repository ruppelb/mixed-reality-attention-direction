// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "SetUpMixedReality.generated.h"

/* 
*	Function library class.
*	Each function in it is expected to be static and represents blueprint node that can be called in any blueprint.
*
*	When declaring function you can define metadata for the node. Key function specifiers will be BlueprintPure and BlueprintCallable.
*	BlueprintPure - means the function does not affect the owning object in any way and thus creates a node without Exec pins.
*	BlueprintCallable - makes a function which can be executed in Blueprints - Thus it has Exec pins.
*	DisplayName - full name of the node, shown when you mouse over the node and in the blueprint drop down menu.
*				Its lets you name the node using characters not allowed in C++ function names.
*	CompactNodeTitle - the word(s) that appear on the node.
*	Keywords -	the list of keywords that helps you to find node when you search for it using Blueprint drop-down menu. 
*				Good example is "Print String" node which you can find also by using keyword "log".
*	Category -	the category your node will be under in the Blueprint drop-down menu.
*
*	For more info on custom blueprint nodes visit documentation:
*	https://wiki.unrealengine.com/Custom_Blueprint_Node_Creation
*/

DECLARE_LOG_CATEGORY_EXTERN(LogSetUpMixedReality, Log, All);

UCLASS(ClassGroup = OpenXR)
class USetUpMixedRealityFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
    /**
     * Enables/disables depth testing.
     *
     * @param    Enabled
     */
    UFUNCTION(BlueprintCallable, meta = (DisplayName = "Set Depth Test Enabled", Keywords = "SetUpMixedReality depth test"), Category = "SetUpMixedReality|Depth")
        static void SetDepthTestEnabled(bool Enabled);

    /**
     * Check if depth testing is supported by active OpenXR runtime.
     */
    UFUNCTION(BlueprintPure, meta = (DisplayName = "Is Depth Test Supported", Keywords = "SetUpMixedReality depth test"), Category = "SetUpMixedReality|Depth")
        static bool IsDepthTestSupported();

    /**
     * Check if depth testing is enabled.
     */
    UFUNCTION(BlueprintPure, meta = (DisplayName = "Is Depth Test Enabled", Keywords = "SetUpMixedReality depth test"), Category = "SetUpMixedReality|Depth")
        static bool IsDepthTestEnabled();

    /**
     * Sets the depth test range.
     *
     * @param    Enabled
     * @param    NearZ is a non-negative distance in meters that specifies the lower bound of the range where depth testing should be performed. Must be less than depthTestRangeFarZ.
     * @param    FarZ is a positive distance in meters that specifies the upper bound of the range where depth testing should be performed. Must be greater than depthTestRangeNearZ.
     */
    UFUNCTION(BlueprintCallable, meta = (DisplayName = "Set Depth Test Range", Keywords = "SetUpMixedReality depth test range"), Category = "SetUpMixedReality|Depth")
        static void SetDepthTestRange(bool Enabled, float NearZ = 0.0f, float FarZ = 1.0f);

    /**
     * Get depth test range.
     */
    UFUNCTION(BlueprintPure, meta = (DisplayName = "Get Depth Test Range", Keywords = "SetUpMixedReality depth test range"), Category = "SetUpMixedReality|Depth")
        static void GetDepthTestRange(bool& bIsEnabled, float& NearZ, float& FarZ);

    /**
     * Enables/disables environment depth estimation.
     *
     * @param    Enabled
     */
    UFUNCTION(BlueprintCallable, meta = (DisplayName = "Set Environment Depth Estimation Enabled", Keywords = "SetUpMixedReality environment depth estimation"), Category = "SetUpMixedReality|Depth")
        static void SetEnvironmentDepthEstimationEnabled(bool Enabled);

    /**
     * Check if environment depth estimation is supported by active OpenXR runtime.
     */
    UFUNCTION(BlueprintPure, meta = (DisplayName = "Is Environment Depth Estimation Supported", Keywords = "SetUpMixedReality environment depth estimation"), Category = "SetUpMixedReality|Depth")
        static bool IsEnvironmentDepthEstimationSupported();

    /**
     * Check if environment depth estimation is enabled.
     */
    UFUNCTION(BlueprintPure, meta = (DisplayName = "Is Environment Depth Estimation Enabled", Keywords = "SetUpMixedReality environment depth estimation"), Category = "SetUpMixedReality|Depth")
        static bool IsEnvironmentDepthEstimationEnabled();

    /**
     * Check if foveated rendering is supported by active OpenXR runtime.
     */
    UFUNCTION(BlueprintPure, meta = (DisplayName = "Is Foveated Rendering Supported", Keywords = "SetUpMixedReality foveated rendering"), Category = "SetUpMixedReality|Foveated Rendering")
        static bool IsFoveatedRenderingSupported();

    /**
     * Check if foveated rendering is enabled.
     */
    UFUNCTION(BlueprintPure, meta = (DisplayName = "Is Foveated Rendering Enabled", Keywords = "SetUpMixedReality foveated rendering"), Category = "SetUpMixedReality|Foveated Rendering")
        static bool IsFoveatedRenderingEnabled();
};