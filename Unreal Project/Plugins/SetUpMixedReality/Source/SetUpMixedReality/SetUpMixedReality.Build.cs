// Some copyright should be here...

using UnrealBuildTool;

public class SetUpMixedReality : ModuleRules
{
	public SetUpMixedReality(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		PrivatePCHHeaderFile = @"Private\OpenXRCommon.h";

		PublicIncludePaths.AddRange(
			new string[] {
				// ... add public include paths required here ...
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				"SetUpMixedReality/Private/External"
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				// ... add other public dependencies that you statically link with here ...
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				"HeadMountedDisplay",
				"SetUpMixedRealityRuntimeSettings",
			}
			);

		PublicDependencyModuleNames.Add("OpenXRHMD");
		// Required to (delayload) link with OpenXR loader.
		AddEngineThirdPartyPrivateStaticDependencies(Target, "OpenXR");

		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);
	}
}
