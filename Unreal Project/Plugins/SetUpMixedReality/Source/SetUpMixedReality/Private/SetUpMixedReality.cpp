// Copyright Epic Games, Inc. All Rights Reserved.

#include "SetUpMixedReality.h"

#include "AlphaBlendModePlugin.h"
#include "DepthPlugin.h"
#include "FoveatedRenderingPlugin.h"

DEFINE_LOG_CATEGORY(LogSetUpMixedReality);

#define LOCTEXT_NAMESPACE "FSetUpMixedRealityModule"

namespace SetUpMixedReality
{
	static class FSetUpMixedRealityModule* g_SetUpMixedRealityModule;
    class FSetUpMixedRealityModule : public IModuleInterface
    {
    public:
        FDepthPlugin DepthPlugin;
        FAlphaBlendModePlugin AlphaBlendModePlugin;
        FFoveatedRenderingPlugin FoveatedRenderingPlugin;

        void StartupModule() override
        {
            AlphaBlendModePlugin.Register();
            DepthPlugin.Register();
            FoveatedRenderingPlugin.Register();

            g_SetUpMixedRealityModule = this;
        }

        void ShutdownModule() override
        {
            g_SetUpMixedRealityModule = nullptr;

            FoveatedRenderingPlugin.Unregister();
            DepthPlugin.Unregister();
            AlphaBlendModePlugin.Unregister();
        }
    };
}

void USetUpMixedRealityFunctionLibrary::SetDepthTestEnabled(bool Enabled)
{
    SetUpMixedReality::g_SetUpMixedRealityModule->DepthPlugin.SetDepthTestEnabled(Enabled);
}

bool USetUpMixedRealityFunctionLibrary::IsDepthTestSupported()
{
    return SetUpMixedReality::g_SetUpMixedRealityModule->DepthPlugin.IsDepthTestSupported();
}

bool USetUpMixedRealityFunctionLibrary::IsDepthTestEnabled()
{
    return SetUpMixedReality::g_SetUpMixedRealityModule->DepthPlugin.IsDepthTestEnabled();
}

void USetUpMixedRealityFunctionLibrary::SetDepthTestRange(bool Enabled, float NearZ, float FarZ)
{
    SetUpMixedReality::g_SetUpMixedRealityModule->DepthPlugin.SetDepthTestRange(Enabled, NearZ, FarZ);
}

void USetUpMixedRealityFunctionLibrary::GetDepthTestRange(bool& bIsEnabled, float& NearZ, float& FarZ)
{
    SetUpMixedReality::g_SetUpMixedRealityModule->DepthPlugin.GetDepthTestRange(bIsEnabled, NearZ, FarZ);
}

void USetUpMixedRealityFunctionLibrary::SetEnvironmentDepthEstimationEnabled(bool Enabled)
{
    SetUpMixedReality::g_SetUpMixedRealityModule->DepthPlugin.SetEnvironmentDepthEstimationEnabled(Enabled);
}

bool USetUpMixedRealityFunctionLibrary::IsEnvironmentDepthEstimationSupported()
{
    return SetUpMixedReality::g_SetUpMixedRealityModule->DepthPlugin.IsEnvironmentDepthEstimationSupported();
}

bool USetUpMixedRealityFunctionLibrary::IsEnvironmentDepthEstimationEnabled()
{
    return SetUpMixedReality::g_SetUpMixedRealityModule->DepthPlugin.IsEnvironmentDepthEstimationEnabled();
}

bool USetUpMixedRealityFunctionLibrary::IsFoveatedRenderingSupported()
{
    return SetUpMixedReality::g_SetUpMixedRealityModule->FoveatedRenderingPlugin.IsFoveatedRenderingSupported();
}

bool USetUpMixedRealityFunctionLibrary::IsFoveatedRenderingEnabled()
{
    return SetUpMixedReality::g_SetUpMixedRealityModule->FoveatedRenderingPlugin.IsFoveatedRenderingEnabled();
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(SetUpMixedReality::FSetUpMixedRealityModule, SetUpMixedReality)