#include "SetUpMixedRealityEditor.h"
#include "SetUpMixedRealityRuntimeSettings.h"
#include "Modules/ModuleInterface.h"
#include "ISettingsModule.h"
#include "Modules/ModuleManager.h"
#include "PropertyEditorModule.h"

#define LOCTEXT_NAMESPACE "FSetUpMixedRealityEditorModule"
void FSetUpMixedRealityEditorModule::StartupModule()
{
    ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings");

    if (SettingsModule != nullptr)
    {
        SettingsModule->RegisterSettings("Project", "Plugins", "SetUpMixedReality",
            LOCTEXT("RuntimeSettingsName", "SetUpMixedReality"),
            LOCTEXT("RuntimeSettingsDescription", "Project settings for SetUpMixedReality Extension plugin"),
            GetMutableDefault<USetUpMixedRealityRuntimeSettings>()
        );
    }
}

void FSetUpMixedRealityEditorModule::ShutdownModule()
{
    ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings");

    if (SettingsModule != nullptr)
    {
        SettingsModule->UnregisterSettings("Project", "Plugins", "SetUpMixedReality");
    }
}

IMPLEMENT_MODULE(FSetUpMixedRealityEditorModule, SetUpMixedRealityEditor);

#undef LOCTEXT_NAMESPACE