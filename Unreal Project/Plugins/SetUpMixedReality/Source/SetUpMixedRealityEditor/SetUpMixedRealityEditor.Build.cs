using UnrealBuildTool;

public class SetUpMixedRealityEditor : ModuleRules
{
    public SetUpMixedRealityEditor(ReadOnlyTargetRules Target) : base(Target)
    {
        PrivateDependencyModuleNames.AddRange(
            new string[] {
                "Core",
                "CoreUObject",
                "InputCore",
                "Engine",
                "Slate",
                "SlateCore",
                "EditorStyle",
                "EditorWidgets",
                "DesktopWidgets",
                "PropertyEditor",
                "UnrealEd",
                "SharedSettingsWidgets",
                "TargetPlatform",
                "RenderCore",
                "SetUpMixedRealityRuntimeSettings"
            }
        );

        PrivateIncludePathModuleNames.AddRange(
            new string[] {
                "Settings"
            }
        );
    }
}