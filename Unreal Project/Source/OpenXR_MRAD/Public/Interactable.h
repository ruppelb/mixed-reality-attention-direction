// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ADComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ADHISMeshComponent.h"
#include "UI/AD_Indicator_Widget.h"
#include "Interactable.generated.h"

class AInteractableManager;

/**
 * A virtual object representing a real-world area of interest, on which we want to measure the attention and direct the attention to. 
 * 
 * This class is thought to utilized in mixed reality (MR) environements to measure the visual attention of a user wearing a HMD in certain real-world points. 
 * It provides a mesh that can be transformed to fit the real-world area and placed in a virtual 3D world at the corresponding real-world position.
 * On this mesh we can measure interaction from the user and start a direction of attention to it based on events or after a time without attention or stop it when the attention lies on the object.
 * Additionally a time threshold can be set to define the minimal interaction time before attention is detected, to avoid registering false attention
 * Two measures for attention direction are implemented in this class. One for directing attention when an object of this class is within the view of the user (ADHISMeshComponent) and one if it is out of view (ADIndicator).
 * This class allows to extend or override the pre-implemented attention direction mechanisms with self defined approaches in ADComponents.
 * Different priority levels of attention direction can be set when excecuting the guidance of attention, to display the severity of attention based on time since the direction is started or to highlight most important objects over less important.
 * The current attention state can be visualized with a material on the mesh, with green meaning no attention required and red the opposite.
 * Multiple interactables are managed by an interactable manager.
 * 
 */

UCLASS()
class OPENXR_MRAD_API AInteractable : public AActor
{
	GENERATED_BODY()
	
public:	
	/**
	 * Sets default values for this actor's properties.
	 * Creates mesh component.
	 * Sets collision objects.
	 * Sets render custom depth option.
	 * Loads materials used in visualization.
	 */ 
	AInteractable();

protected:
	/**
	 * Called when the game starts or when spawned.
	 * Initializes private variables.
	 * Sets up dynamic material instance and starting material.
	 * Sets up ADHISMeshComponent.
	 */
	virtual void BeginPlay() override;

	/**
	 * Called after this actor is finished loading from disk.
	 * Sets up components after actor is loaded.
	 */
	virtual void PostLoad() override;

	/**
	 * Called when this actor is spawned after its creation.
	 * Attaches interactable to manager.
	 * Sets up components after actor is spawned.
	 */
	virtual void PostActorCreated() override;

	/**
	 * Called when this actor is explicitly being destroyed during gameplay or in the editor.
	 * Unregisters interactable at interactable manager.
	 */ 
	virtual void Destroyed() override;

	/**
	 * Called when an instance of this class is placed (in editor) or spawned. In sequence after the PostActorCreated method.
	 * Updates ADHISMeshcomponent to fit actor after interactable is transformed in editor.
	 */
	virtual void OnConstruction(const FTransform& Transform) override;

	/**
	 * Called after duplication & serialization and before PostLoad.
	 * Properly set up interactable after duplication.
	 */
	virtual void PostDuplicate(EDuplicateMode::Type DuplicateMode) override;

#if WITH_EDITOR
	/**
	 * Called when a property on this object has been modified externally (e.g. in editor through UProperties)
	 * Updates visualization and components after changes where made in the properties while in editor.
	 */ 
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

public:	
	/**
	 * Called every frame.
	 * Conducts attention direction and updates visualization color.
	 */ 
	virtual void Tick(float DeltaTime) override;

	/**
	 * Start the attention direction to the interactable.
	 * Sets up inital parameters for directing attention and then calls directAttention.
	 * Either called from within this class when the specified time without attention is up or callable from another class based on certain events.
	 * 
	 */
	UFUNCTION(BlueprintCallable)
	virtual void startAttentionDirection();

	/** 
	 * Called when trying to stop attention with a specified time paramter, to indicate the time spent trying to stop the direction.
	 * The time is summed up till it exceeds the time threshold for minimum interaction time, and then the attention direction is stopped. 
	 * This function can be called from BluePrints. For example:
	 * 
	 *		After an eye gaze interaction is detected with an interactable, the XRPawn BluePrint class calls the tryStoppingAttention method on it to stop a possible attention direction.
	 * 
	 * @param DeltaTime - Time spent trying to stop the attention direction.
	 */
	UFUNCTION(BlueprintCallable)
	virtual void tryStoppingAttentionDirection(float DeltaTime);

	/**
	 *  Called to stop the attention direction on an interactable. This method resets the attention state before another direction begins.
	 *  Relays the call to all attached and registered 'ADComponents'.
	 */
	UFUNCTION(BlueprintCallable)
	virtual void stopAttentionDirection();

	/**
	 * Attaches an 'ADComponent' of the specified class to the interactable and registers it.
	 * 
	 * @param ADComponent - The class of ADComponent which is to be attached to the interactable.
	 * @return a Boolean representing if the operation was sucessful
	 */
	virtual bool setADComponent(TSubclassOf<UADComponent> ADComponent);
	
	/**
	 * Sets the 'ADHISMeshComponent', which used for directing the attention direction in view, to an component of the specified class.
	 * 
	 * @param ADHISMeshComponent - The class of ADHISMeshComponent which is to be attached to the interactable
	 * @return a Boolean representing if the operation was sucessful
	 */
	virtual bool setADHISMeshComponent(TSubclassOf<UADHISMeshComponent> ADHISMeshComponent);

	/**
	 * Called to enable and disable the visualization of the attention on the interactable.
	 * If visualizing, the material of the mesh is changed to a dynamic material, which changes color in a gradient from green to red based on the attention state (has attention - green, needs attention - red).
	 * Otherwise, a material is applied which masks the object in MR, making the object appear invisible to the user, since the real world is visible through it.
	 * 
	 * @param state - The state to set the visualization to.
	 */
	virtual void visualizeAttention(bool state);

	/**
	 * Sets the 'InteractableManager' of this interactable to the one specified.
	 * The interactable then unregisters with its previous manager and registers with and attaches to the interactable manager actor provided.
	 * 
	 * @param newInteractableManager - A reference to the new InteractableManager actor.
	 */
	virtual void setInteractableManager(AInteractableManager* newInteractableManager);

	/**
	 * Sets the mesh of the interactable to the mesh reference specified.
	 * Currently only a plane and a cube mesh are supported. Both have a basic size of 100 CM when scale is 1.
	 *
	 * @param mesh - A reference to the new mesh.
	 */
	virtual void setMesh(UStaticMesh* mesh);

private:

	/**
	 * Updates all attention direction mechanisms for a guidance of attention within view and updates severity level.
	 * Called every frame when attention is directed and if the interactable is in the user's FOV.
	 * Relays the call to all attached and registered ADComponents.
	 * 
	 * @param level - A integer describing the level of the attention direction.
	 */
	virtual void directAttention(int32 levelIn);

	/**
	 * Updates all attention direction mechanisms for a guidance of attention out of view and updates severity level.
	 * Called every frame when attention is directed and if the interactable is not in the user's FOV.
	 * Relays the call to all attached and registered ADComponents.
	 * 
	 * @param level - A integer describing the level of the attention direction.
	 */
	virtual void directAttentionNotInView(int32 levelIn);

	/**
	 * Updates the 'currentADComponent' to the class which is selected in the UProperty 'ADComponent'.
	 * Unregisters and destroyes previous component, that is saved in the variable 'currentADComponent', if it is set.
	 * Registers and attaches a new 'ADComponent' of the class specified in 'ADComponent' and saves this reference in 'currentADComponent'
	 */
	virtual void setUpADComponent();

	/**
	 * Updates the 'currentADHISMeshComponent' to the class which is selected in the UProperty 'ADHISMeshComponent'.
	 * Unregisters and destroyes previous component, that is saved in the variable 'currentADHISMeshComponent', if it is set.
	 * Registers and attaches a new 'ADHISMeshComponent' of the class specified in 'ADComponent' and saves this reference in 'currentADHISMeshComponent'
	 */
	virtual void setUpADHISMeshComponent();

	/**
	 * Attaches the interactable to an 'InteractableManager'.
	 * 
	 * Searches for InteractableManager class objects in the world.
	 * If a reference is found and the Boolean indicating it is valid is set to true, the interactable registers at this interactable manager.
	 * Otherwise, a new  interactable manager is created, which automatically attaches all interactables in the world.
	 */
	virtual void attachToInteractableManager();

	/**
	 * Updates the frame of the 'currentADHISMeshComponent' with the the 'frameThickness' and 'dynamicFrameThickness' values selected on the interactable.
	 * Called every frame during play and in 'OnConstruct' to update frame when in editor.
	 */
	virtual void updateADHISMeshComponent();

	/**
	 * Finds and sets any attached 'ADHISMeshComponent' on the interactable actor.
	 * If an 'ADHISMeshComponent' is found, the reference is saved in 'currentADHISMeshComponent' and the class reference is saved in the 'ADHISMeshComponent' variable.
	 * 
	 * @return if any ADHISMeshComponent could be found
	 */
	virtual bool findAndSetADHISMeshComponent();

	/**
	 * Finds and sets any attached 'ADComponent' on the interactable actor.
	 * If an 'ADComponent' is found, the reference is saved in 'currentADComponent' and the class reference is saved in the 'ADComponent' variable.
	 * 
	 * @return if any ADComponent could be found
	 */
	virtual bool findAndSetADComponent();

	/**
	 * Sets up components on the interactable.
	 * Intended for setting up ADComponents and ADHISMeshComponents after loading or creating the actor.
	 */
	virtual void setUpComponents();

public:
	
	/** 
	 * Mesh component that represents area of interest.
	 * Used to visualize attention and to detect interaction.
	 * Supported meshes include a plane and a cube.
	 */
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	UStaticMeshComponent* visualizationMesh;

	/// Boolean for indicating, if the attention should be directed after a time span defined in 'directAfterSeconds'
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool automaticDirectionAfterTimeSpan = true;

	/// The time in seconds after a guidance of attention to this interactable should be performed
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ClampMin = "1.0", UIMin = "1.0", EditCondition = "automaticDirectionAfterTimeSpan"))
	float directAfterSeconds = 10.0;

	/// The time threshold in seconds after which an interaction with the mesh counts as attention
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ClampMin = "0.0", UIMin = "0.0"))
	float stopDirectionAfterSeconds = 1.0;

	/// Boolean representing the intention to visualize attention
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool visualize = true;

	/// Indicator if the attention direction can be stopped through hand interactions
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool isHandInteractable;
	
	/// Indicator if the attention direction can be stopped through eye gaze interactions
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool isEyeGazeInteractable;

	/// The interactable manager to which this interactable is currently attached to
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	AInteractableManager* interactableManager;

	// The class of the current ADComponent registered at this interactable
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (UseComponentPicker))
	TSubclassOf<UADComponent> ADComponent;

	// The reference of the current ADComponent registered at this interactable
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	UADComponent* currentADComponent;

	// The class of the current ADHISMeshComponent registered at this interactable
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (UseComponentPicker))
	TSubclassOf<UADHISMeshComponent> ADHISMeshComponent;

	// The reference of the current ADHISMeshComponent registered at this interactable
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	UADHISMeshComponent* currentADHISMeshComponent;

	/// Indicator if the thickness of the ADHISMeshComponent frame should scale dynamically
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool dynamicFrameThickness = true;

	/// The thickness of the frame of the current ADHISMeshComponent
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ClampMin = "0.0", UIMin = "0.0", EditCondition = "!dynamicFrameThickness"))
	float frameThickness = 1.0f;

	/// The class of the current ADIndicator widget
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (UseComponentPicker))
	TSubclassOf<UAD_Indicator_Widget> indicatorWidget;

	/// The level of severity for the attention direction
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ClampMin = "0.0", UIMin = "0.0", ClampMax = "100.0", UIMax = "100.0"))
	int level = 0;

	/// Indicator if debug information should be printed
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool printDebug = false;

private:
	/// The seconds since the last interaction that counted as attention
	float secondsSinceAttention;

	/// The seconds since the last attention direction has started
	float secondsSinceAttentionDirectionTrigger;

	/// Indicator, if the interactable is currently directing attention
	bool directingAttention;

	/// The sum of seconds that was interacted with this interactable since the start of the last attention direction
	float secondsWithAttention;

	/// The reference of the material used for visualization
	UMaterialInstanceDynamic* visualizationMaterial;

	/// The reference of the visualization material loaded at construction
	UMaterialInterface* loadedVisualizationMaterial;

	/// The start color of the visualization material
	FLinearColor visualizationStartColor;

	/// The reference to the material which is used for Masking
	UMaterialInterface* loadedMaskingMaterial;

	/// The current color of the visualization mesh
	FLinearColor currentColor;

	/// The reference of the 'ADIndicator' widget which is currently displayed on the 'ADHUD'
	UAD_Indicator_Widget* currentlyDisplayedWidget;
};
