// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Kismet/KismetMathLibrary.h"
#include "RotationTest.generated.h"

UCLASS()
class OPENXR_MRAD_API ARotationTest : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARotationTest();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void OnConstruction(const FTransform& Transform) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, CallInEditor)
    virtual void RotateToOrigin();

	UFUNCTION(BlueprintCallable, CallInEditor)
	virtual void RotateToOriginNew();

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
    UStaticMeshComponent* Mesh;

};
