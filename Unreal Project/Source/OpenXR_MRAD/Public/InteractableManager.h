// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Interactable.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UI/AD_HUD.h"
#include "Components/WidgetComponent.h"
#include "InteractableManager.generated.h"

/**
 * Provides functions to collectivly manage multiple interactables at the same time.
 * 
 * This class is used to simplify the management of each interactable when multiple interactables are in the world.
 * Only one interactable manager may exist simultaneously and therefore measures are implemented to check the validity of an interactable manager if multiple are present.
 * It supplies functions to register or unregister an interactable and saves the HUD reference so that it is accessible for all interactables.
 * It allows to toggle the visualization of attention at all registered interactables or center all interactables at the current player position.
 * 
 */

UCLASS()
class OPENXR_MRAD_API AInteractableManager : public AActor
{
	GENERATED_BODY()
	
public:	
	/**
	 * Sets default values for this actor's properties.
	 * Sets up rootcomponent.
	 * Initializes variables.
	 */
	AInteractableManager();

protected:
	/**
	 * Called when the game starts or when spawned.
	 * Starts timer to retrieve HUD handle with initPlayerHUD.
	 */
	virtual void BeginPlay() override;

	/**
	 * Called after this actor is finished loading from disk.
	 * Checks validity of this interactable manager.
	 */
	virtual void PostLoad() override;

	/**
	 * Called when this actor is spawned after its creation.
	 * Checks validity of this interactable manager.
	 */
	virtual void PostActorCreated() override;

	/**
	 * Called when an instance of this class is placed (in editor) or spawned. In sequence after the PostActorCreated method.
	 * Destroys interactable manager if it is not valid.
	 * Locks position to world origin (0,0,0).
	 */
	virtual void OnConstruction(const FTransform& Transform) override;

	/**
	 * Called when this actor is explicitly being destroyed during gameplay or in the editor.
	 * Resets the interactable manager for all interactables attached.
	 */
	virtual void Destroyed() override;

public:	
	/**
	 * Called every frame.
	 */
	virtual void Tick(float DeltaTime) override;

	/**
	 * Called after actor is loaded or created.
	 * Checks for all interactable manager actors in the world, if one of them is valid.
	 * If none is valid, this interactable manager is set valid and all interactables are attached.
	 * If a valid one is found, this interactable manager is set invalid.
	 */
	virtual void checkValidity();

	/**
	 * Can be called during play.
	 * Positions the interactable manager at the current pawn position and rotation. Therefore all attached interactables are reset to the player.
	 */
	UFUNCTION(BlueprintCallable)
	virtual void resetInteractablesToPlayer();

	/**
	 * Called by an interactable or internally to register an interactable.
	 * Adds an interactable reference to the interactable list.
	 * 
	 * @param interactable - The interactable reference to add to the list.
	 */
	UFUNCTION(BlueprintCallable)
	virtual void registerInteractable(AInteractable* interactable);

	/**
	 * Called by an interactable or internally to register an interactable.
	 * Removes an interactable reference from the interactable list.
	 * 
	 * @param interactable - The interactable reference to remove from the list.
	 */
	UFUNCTION(BlueprintCallable)
	virtual void unregisterInteractable(AInteractable* interactable);

	/**
	 * Called when an interactable manager is spawned or loaded or manually.
	 * Attaches and registers all interactables in the world to this interactable manager.
	 */
	UFUNCTION(BlueprintCallable, CallInEditor)
	virtual void attachAllInteractables();

	/**
	 * Toggles the visualization on all registered interactables.
	 */
	UFUNCTION(BlueprintCallable, CallInEditor)
	virtual void visualizeAttention();

private:

	/**
	 * Called after begin play with a time delay, since widget containers need some time to be initialized.
	 * Saves the player hud reference of the pawn object and makes this available to all registered interactables.
	 */
	virtual void initPlayerHUD();

public:
	///The reference to the HUD Widget of the pawn.
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere);
	UAD_HUD* playerHUD;

	///The list of all registered interactables.
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere);
	TArray<AInteractable*> interactableList;

	/// Indicator if debug information should be printed
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool printDebug = false;

	///Indicator if this is a valid interactable manager.
	bool isValidInteractableManager;

private:

	///Indicator which visualization state is currently toggled on all interactables.
	bool visualize;
};
