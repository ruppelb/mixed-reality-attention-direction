// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Overlay.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/VerticalBox.h"
#include "AD_Indicator_Widget.h"
#include "Interactable.h"
#include "AD_HUD.generated.h"

/**
 * Widget container used for dislaying ADIndicators, MRSettings, or other information directly in the view of a XRPawn
 * 
 * Includes functions to register and unregister ADIndicators or debug widgets.
 * Provides an Overlay where multiple ADIndicators are overlayed and displayed.
 * Supplies a vertical box where the debug information can be displayed.
 * 
 */
UCLASS()
class OPENXR_MRAD_API UAD_HUD : public UUserWidget
{
	GENERATED_BODY()
public:
	/**
	 * Called when an instance of this class is spawned or added to the viewport.
	 * Initializes widget components.
	 */
	virtual void NativeConstruct() override;

	/**
	 * Called by an interactable if it wants to display a ADindicator widget for out of view attention direction. 
	 * Spawns an ADIndicator widget, sets up its variables and adds it to the overlay.
	 * 
	 * @param interactable - The interactable to which the ADIndicator should indicate the direction.
	 */
	virtual UAD_Indicator_Widget* RegisterInteractableWidget(AInteractable* interactable);

	/**
	 * Called by an interactable if it wants stop displaying a ADindicator widget for out of view attention direction. 
	 * Removes the specified ADIndicator widget from the overlay and destroys it.
	 * 
	 * @param widget - The widget to be removed from the overlay.
	 */
	virtual void UnregisterInteractableWidget(UAD_Indicator_Widget* widget);

	/**
	 * Called to add a widget to the debug box.
	 * Adds the specified widget to the vertical debug box.
	 * 
	 * @param widget - The widget that is added to the debug box.
	 */
	UFUNCTION(BlueprintCallable)
	virtual void RegisterWidgetOnDebugBox(UWidget* widget);

	/**
	 * Called to remove a widget from the debug box.
	 * Removes the specified widget from the vertical debug box.
	 * 
	 * @param widget - The widget that is removed from the debug box.
	 */
	UFUNCTION(BlueprintCallable)
	virtual void UnregisterWidgetOnDebugBox(UWidget* widget);

	/**
	 * Overlay container in which the ADIndicators are displayed.
	 * This widget must be present with the same name in the dedicated blueprint class.
	 */
	UPROPERTY(meta = (BindWidget))
	UOverlay* IndicatorOverlay;

	/**
	 * Vertical box container in which the debug widgets are displayed.
	 * This widget must be present with the same name in the dedicated blueprint class.
	 */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UVerticalBox* DebugBox;
};
