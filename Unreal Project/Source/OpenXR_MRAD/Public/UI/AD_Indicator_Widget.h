// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Border.h"
#include "Components/CanvasPanelSlot.h"
#include "Kismet/KismetMathLibrary.h"
#include "AD_Indicator_Widget.generated.h"

/**
 * Displays a 2D directional indicator showing the shortest way to an interactable in 3D space
 * 
 * Spawned by an interactable when directing attention out of the users view.
 * The indicator is registered with the ADHUD widget component on the pawn when it is spawned and gets unregistered before it is destroyed.
 * During play, every tick the shortest path from the camera to the interactable to which it directs the attention is computed.
 * This path is projected on the camera plane and indicates the shortest direction that the camera must be rotatet to face the interactable.
 * The indicator is altered by changing parameters of a material, which is displayed as circular overlay in the center on the border.
 * It that takes an angle parameter for adjusting the direction on screen and an aplha parameter to define the width of the indicator.
 */

UCLASS()
class OPENXR_MRAD_API UAD_Indicator_Widget : public UUserWidget
{
	GENERATED_BODY()
public:
	/**
	 * Called when an instance of this class is spawned or added to the viewport.
	 * Initializes widget components.
	 */
	virtual void NativeConstruct() override;

	/**
	 * Called every frame.
	 * Updates the angle and alpha parameters of the ADIndicator to fit the shortest direction and the distance to the indicator.
	 */
	virtual void NativeTick(const FGeometry& MyGeometry, float DeltaTime) override;
	

	/**
	 * Border in which the indication is displayed. Holds the material used for showing the indicator.
	 * This widget must be present with the same name in the dedicated blueprint class.
	 */
	UPROPERTY(meta = (BindWidget))
	UBorder* border;

	/// The material instance utilized to display the indicator.
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UMaterialInstanceDynamic* material;

	/// The camera location from which the indicator shows the shortest path.
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	APlayerCameraManager* playerCamera;
	
	/// The interactable to which the indicator shows the shortest path.
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	AActor* interactable;

	/// The maximal width of the ADIndicator displayed.
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float maxWidth = 0.3f;
};
