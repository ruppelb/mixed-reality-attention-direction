// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "Components/Spacer.h"
#include "Components/HorizontalBox.h"
#include "Components/HorizontalBoxSlot.h"
#include "Kismet/KismetInputLibrary.h"
#include "MRSettings_Widget.generated.h"


/**
 * The states for each MRSetting
 * 
 * These values are used to display if an MRSetting is enabled, disabled, or unsupported.
 */
UENUM()
enum EnabledState
{
	Enabled = 0  UMETA(DisplayName = "Enabled"),
	Disabled = 1 UMETA(DisplayName = "Disabled"),
	Unsupported = 2 UMETA(DisplayName = "Unsupported")
};

/**
 * A base class for displaying the state of MR settings that are used throughout this project with widgets.
 * 
 * Based on the settings supplied with the Varjo Unreal Engine template project. 
 * Needs to be extended with blueprints for full functionalities and provides base funcionalities needed for all MRSettings.
 * This class initializes the widget, and updates the state of the setting during play based on a function implemented in a blueprint subclass.
 * On construction, the labels of the setting are set and the toggle key is requested from the blueprint.
 */
UCLASS()
class OPENXR_MRAD_API UMRSettings_Widget : public UUserWidget
{
	GENERATED_BODY()
public:
	/**
	 * Called when an instance of this class is spawned or added to the viewport.
	 * Initializes widget components and updates the labels and toggle key.
	 */
	virtual void NativeConstruct() override;

	/**
	 * Called every frame.
	 * Updates the current enabled state
	 */
	virtual void NativeTick(const FGeometry& MyGeometry, float DeltaTime) override;

	/**
	 * Called to update the color and text of the value textblock based on the EnabledState returned by getEnabledState.
	 */
	UFUNCTION(BlueprintCallable)
	virtual void updateValue();

	/**
	 * Called to set the current EnabledState based on the Booleans returned by getState.
	 * 
	 * @return the enabled state of the MRSetting.
	 */
	UFUNCTION(BlueprintCallable)
	virtual EnabledState getEnabledState();

	/**
	 * Called to request from the blueprint class if the MRSetting is supported and enabled.
	 * Must be implemented in a blueprint subclass of this.
	 * 
	 * @param isSupported - An indicator if the setting is supported.
	 * @param isEnabled - An indicator if the setting is enabled.
	 */
	UFUNCTION(BlueprintImplementableEvent)
	void getState(bool& isSupported, bool& isEnabled);

	/**
	 * Called after construction to update the labels of the setting based on values specified in variables and getToggleKey.
	 */
	UFUNCTION(BlueprintCallable)
	virtual void updateLabel();

	/**
	 * Called to get the toggle key assigned to this setting.
	 * Must be implemented in a blueprint subclass of this.
	 * 
	 * @return the key assigned to the setting.
	 */
	UFUNCTION(BlueprintImplementableEvent)
	FKey getToggleKey();

	/**
	 * Container in which the label, spacer, and value are displayed.
	 * This widget must be present with the same name in the dedicated blueprint class.
	 */
	UPROPERTY(meta = (BindWidget))
	UHorizontalBox* HorizontalBox;

	/**
	 * Textblock containing Label of the Setting and its toggle key.
	 * This widget must be present with the same name in the dedicated blueprint class.
	 */
	UPROPERTY(meta = (BindWidget))
	UTextBlock* Label;

	/**
	 * Textblock containing the enabled state of the setting.
	 * This widget must be present with the same name in the dedicated blueprint class.
	 */
	UPROPERTY(meta = (BindWidget))
	UTextBlock* Value;

	/**
	 * Spacer between label and value.
	 * This widget must be present with the same name in the dedicated blueprint class.
	 */
	UPROPERTY(meta = (BindWidget))
	USpacer* Spacer;

	/**
	 * The name that is used to display next to the toggle key in the Label text block.
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FString StateName;

private:

	/// Text for EnabledState enabled.
	FString EnabledText = "Enabled";

	/// Text for EnabledState disabled.
	FString DisabledText = "Disabled";

	/// Text for EnabledState unsupported.
	FString UnsupportedText = "Unsupported";

	/// Color of value text when EnabledState is enabled.
	FLinearColor EnabledColor = FLinearColor::Green;

	/// Color of value text when EnabledState is disabled.
	FLinearColor DisabledColor = FLinearColor::Red;

	/// Color of value text when EnabledState is unsupported.
	FLinearColor UnsupportedColor = FLinearColor::Gray;
};
