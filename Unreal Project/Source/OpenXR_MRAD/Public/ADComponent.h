// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ADComponent.generated.h"


/**
 * A component that can be used to implement own attention direction mechanisms.
 * Provides functions that set parameters for directing the attention direction every tick.
 * Can access parent/Owner properties and change them.
 * This is a template class and should not directly be used. Instead, create a C++ subclass of this.
 * Theoretically also supports creation of  Blueprint subclasses, but this functionality was not tested.
 */

UCLASS(meta = (BlueprintSpawnableComponent, IsBlueprintBase = "true"))
class OPENXR_MRAD_API UADComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	/**
	 * Sets default values for this actor's properties.
	 */
	UADComponent();

protected:
	/**
	 * Called when the game starts or when spawned.
	 */
	virtual void BeginPlay() override;

public:	
	/**
	 * Called every frame.
	 * Conducts attention direction. (Add attention direction functionality here).
	 */
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/**
	 * Updates the variables that attention direction is currently performed inside the user's FOV and updates severty level.
	 * Called every frame by interactable owner when attention is directed and if it is in the user's FOV.
	 *
	 * @param level - A integer describing the level of the attention direction.
	 */
	virtual void directAttention(int32 level);

	/**
	 * Updates the variables that attention direction is currently performed outside the user's FOV and updates severity level.
	 * Called every frame by interactable owner when attention is directed and if it is not in the user's FOV.
	 *
	 * @param level - A integer describing the level of the attention direction.
	 */
	virtual void directAttentionNotInView(int32 level);

	/**
	 *  Updates the variables that the attention direction is stopped. Resets the severity level.
	 */
	virtual void stopDirectingAttention();

	///Indicator if attention direction should be performed within view
	UPROPERTY(BlueprintReadWrite)
	bool directInView = false;

	///Indicator if attention direction should be performed out of view
	UPROPERTY(BlueprintReadWrite)
	bool directOutOfView = false;

	/// The level of severity for the attention direction
	UPROPERTY(BlueprintReadWrite)
	int32 ADLevel = 0;
};
