// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Misc/DateTime.h"

//in order to include boost libraries, we must undefine some macros and tell the compiler that the following includes are third party.
THIRD_PARTY_INCLUDES_START
#pragma push_macro("check")
#undef check
#pragma push_macro("verify")
#undef verify

#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/sync/named_semaphore.hpp>
#include <boost/interprocess/containers/string.hpp>

// Redefine macros
#pragma pop_macro("verify")
#pragma pop_macro("check")

THIRD_PARTY_INCLUDES_END

#include <thread>
#include <mutex>
#include <tuple>

#define MRSHAREDMEMORY "MRSharedMemory"
#define MRMUTEXSEMAPHORE "MRMutexSemaphore"
#define MREMPTYSEMAPHORE "MREmptySemaphore"
#define MRSTOREDSEMAPHORE "MRStoredSemaphore"

#define DEPTHSHAREDMEMORY "DepthSharedMemory"
#define DEPTHMUTEXSEMAPHORE "DepthMutexSemaphore"
#define DEPTHEMPTYSEMAPHORE "DepthEmptySemaphore"
#define DEPTHSTOREDSEMAPHORE "DepthStoredSemaphore"

#define INSTRUMENTSHAREDMEMORY "InstrumentSharedMemory"
#define INSTRUMENTMUTEXSEMAPHORE "InstrumentMutexSemaphore"
#define INSTRUMENTEMPTYSEMAPHORE "InstrumentEmptySemaphore"
#define INSTRUMENTSTOREDSEMAPHORE "InstrumentStoredSemaphore"

using namespace boost::interprocess;

/**
 * Receives VST Frames, point cloud data and instruments through shared memory.
 * 
 * This class utilized the boost interprocess header to achieve this.
 * Three shared memory regions are opend for receiving each data.
 * Each memory region has semaphores to control access to the region and prevent multiple processes accessing the same region.
 * The mutex semaphore assures that only one process accesses the memory region at any given time.
 * The stored semaphore signals the client, that an object is stored in the shared memory.
 * The empty semaphore signals to the host, that the shared memory region is empty.
 * Each data type we receive has its own thread that manages the data import, concurrent to the game thread.
 * The data can be extracted with the defined methods. To prevent simultaneous access to the variable storing the latest data, we employ one mutex per thread.
 * 
 */

class OPENXR_MRAD_API MRAD_IPCConnectorClient
{
public:

    /// Defines for storing the instrument class in shared memory.
    typedef allocator<char, managed_shared_memory::segment_manager>                           char_allocator;
    typedef basic_string<char, std::char_traits<char>, char_allocator>   char_string;
    typedef allocator<void, managed_shared_memory::segment_manager>                           void_allocator;
    
    /**
     * The instrument class as it is stored in shared memory
     * Contains a 3D pixel center, four directions for the corners of an instrument frame, a normal of the instrument surface, and a camera position. 
     * An instrument stores the directions and positions used for positioning interactables to analogue flight instruments locations in a 3D.
     */
    struct Instrument {
        char_string name;

        //center point (deprojected to 3D space)
        float centerX;
        float centerY;
        float centerZ;

        //top point (direction vector)
        float topX;
        float topY;
        float topZ;

        //left point (direction vector)
        float leftX;
        float leftY;
        float leftZ;

        //right point (direction vector)
        float rightX;
        float rightY;
        float rightZ;

        //bottom point (direction vector)
        float bottomX;
        float bottomY;
        float bottomZ;

        //normal (direction vector)
        float normalX;
        float normalY;
        float normalZ;

        //camera position (3D space)
        float camPosX;
        float camPosY;
        float camPosZ;
    };

    /// Further defines for storing the instrument in shared memory
    typedef allocator <Instrument, managed_shared_memory::segment_manager> instrument_allocator;
    typedef vector<Instrument, instrument_allocator> InstrumentDataVector;

    /**
     * The instrument class as it is stored locally in this class
     * Contains a 3D pixel center, four directions for the corners of an instrument frame, a normal of the instrument surface, and a camera position.
     */
    struct InstrumentLocal {
        const char* name;

        //center point (deprojected to 3D space)
        FVector center;

        //top point (direction vector)
        FVector top;

        //left point (direction vector)
        FVector left;

        //right point (direction vector)
        FVector right;

        //bottom point (direction vector)
        FVector bottom;

        //normal (direction vector)
        FVector normal;

        //camera position (3D space)
        FVector camPos;

    public:
        InstrumentLocal(const char* nameIn, float centerXIn, float centerYIn, float centerZIn, float topXIn, float topYIn, float topZIn, float leftXIn, float leftYIn, float leftZIn, float rightXIn, float rightYIn, float rightZIn, float bottomXIn, float bottomYIn, float bottomZIn, float normalXIn, float normalYIn, float normalZIn, float camPosXIn, float camPosYIn, float camPosZIn)
            :name(nameIn), center(FVector(centerXIn, centerYIn, centerZIn)), top(FVector(topXIn, topYIn, topZIn)),left(FVector(leftXIn, leftYIn, leftZIn)), right(FVector(rightXIn, rightYIn, rightZIn)), bottom(FVector(bottomXIn, bottomYIn, bottomZIn)), normal(FVector(normalXIn, normalYIn, normalZIn)),camPos(FVector(camPosXIn,camPosYIn,camPosZIn))
        {}
    };


    /**
     * The camera intrinsics as they are stored in shared memory.
     * Important for projecting things in the camera plane or undistort a camera image.
     */
    struct Intrinsics {
        double principalPointX;            //!< Camera principal point X.
        double principalPointY;            //!< Camera principal point Y.
        double focalLengthX;               //!< Camera focal length X.
        double focalLengthY;               //!< Camera focal length Y.
        double distortionCoefficients[6];  //!< Intrinsics model coefficients. For omnidir: 2 radial, skew, xi, 2 tangential.
    public:
        Intrinsics(double ppX, double ppY, double focLX, double focLY, double* distCoeff)
            :principalPointX(ppX), principalPointY(ppY), focalLengthX(focLX), focalLengthY(focLY)
        {
            std::copy(distCoeff, distCoeff + 6, distortionCoefficients);
        };
        Intrinsics(const Intrinsics& other)
            :principalPointX(other.principalPointX), principalPointY(other.principalPointY), focalLengthX(other.focalLengthX), focalLengthY(other.focalLengthY)
        {
            std::copy(other.distortionCoefficients, other.distortionCoefficients + 6, distortionCoefficients);
        };
        Intrinsics() { };
    };

    /**
     * The VSTFrameInfo class as it is stored in shared memory
     * Contains important information for processing the data arrays of the frames.
     * The data provided can be separated in left frame and right frame data, marked by _l or _r.
     */
    struct VSTFrameInfo {
        int width_l;
        int height_l;
        int channels_l;
        Intrinsics intrinsics_l;
        double extrinsics_l[16];
        double HMDPose_l[16];
        float minDepth_l;
        float maxDepth_l;
        int width_r;
        int height_r;
        int channels_r;
        Intrinsics intrinsics_r;
        double extrinsics_r[16];
        double HMDPose_r[16];
        float minDepth_r;
        float maxDepth_r;
    public:
        VSTFrameInfo(const VSTFrameInfo& other)
            :width_l(other.width_l), height_l(other.height_l), channels_l(other.channels_l), intrinsics_l(other.intrinsics_l), minDepth_l(other.minDepth_l), maxDepth_l(other.maxDepth_l), width_r(other.width_r), height_r(other.height_r), channels_r(other.channels_r), intrinsics_r(other.intrinsics_r), minDepth_r(other.minDepth_r), maxDepth_r(other.maxDepth_r)
        {
            //copy left extrinsic matrix
            std::copy(other.extrinsics_l, other.extrinsics_l + 16, extrinsics_l);

            //copy right extrinsic matrix
            std::copy(other.extrinsics_r, other.extrinsics_r + 16, extrinsics_r);

            //copy left HMD pose matrix
            std::copy(other.HMDPose_l, other.HMDPose_l + 16, HMDPose_l);

            //copy right HMD pose matrix
            std::copy(other.HMDPose_r, other.HMDPose_r + 16, HMDPose_r);
        };
        VSTFrameInfo() {};
    };

    /**
    * Data point which belongs to the point cloud. Contains the following fields in a packed format:
    *
    * Index: globally unique identifier for the point.
    * Confidence: Integer confidence value for the point. Points with confidence of 0 are to be considered removed.
    * Normal: Unit normal in world coordinates.
    * Color: RGB color of the point.
    * Position: position relative to the HMD tracking origin in meters.
    * Radius: Radius of the point in meters.
    */
    struct PointCloudPoint {
        uint32_t indexConfidence;  //!< (index << 8) | (confidence & 0xFF);
        uint32_t normalXY;         //!< float16 / float16
        uint32_t normalZcolorR;    //!< float16 / float16
        uint32_t colorBG;          //!< float16 / float16
        uint32_t positionXY;       //!< float16 / float16
        uint32_t positionZradius;  //!< float16 / float16
    public:
        PointCloudPoint(const PointCloudPoint& other)
            :indexConfidence(other.indexConfidence), normalXY(other.normalXY), normalZcolorR(other.normalZcolorR), colorBG(other.colorBG), positionXY(other.positionXY), positionZradius(other.positionZradius)
        {};
        PointCloudPoint() {};
    };


    /**
     * VST frame data for both eyes.
     * Contains color data arrays, depth data arrays and normal maps of each left and right frame. Also includes frame info.
     */
    struct VSTFrame {
        VSTFrameInfo info;
        std::vector<uint8_t> data_l;
        std::vector<uint8_t> data_r;
        std::vector<uint32_t> depthData_l;
        std::vector<uint32_t> depthData_r;
        std::vector<uint8_t> normalMap_l;
        std::vector<uint8_t> normalMap_r;
    public:
        VSTFrame() {};
    };

    /**
     * Structure for storing point cloud content.
     * Contains an array of pointcloud points and the size of the array.
     */
    struct PointCloudSnapshotContent {
        std::vector<PointCloudPoint> points;
        int32_t pointCount;
    public:
        PointCloudSnapshotContent(PointCloudSnapshotContent& other)
            :pointCount(other.pointCount)
        {
            points = std::move(other.points);
        };
        PointCloudSnapshotContent() {};
    };

    /**
     * Construct IPCConnectorClient
     */ 
    MRAD_IPCConnectorClient();

    /**
     * Destruct IPCConnector. Cleans up running threads and shared memory.
     */
    ~MRAD_IPCConnectorClient();

    
    /// Disable copy, move and assign
    MRAD_IPCConnectorClient(const MRAD_IPCConnectorClient& other) = delete;
    MRAD_IPCConnectorClient(const MRAD_IPCConnectorClient&& other) = delete;
    MRAD_IPCConnectorClient& operator=(const MRAD_IPCConnectorClient& other) = delete;
    MRAD_IPCConnectorClient& operator=(const MRAD_IPCConnectorClient&& other) = delete;
    
    /**
     * Enables or disables import of VST frames based on 'status'
     * Start or ends importVSTThread.
     * 
     * @param status - Indicator if disabling or enabling VST frame import.
     */
    void SetVSTFrameImport(bool status);

    /**
     * Enables or disables import of pointcloud data based on 'status'
     * Start or ends importPointCloudThread.
     * 
     * @param status - Indicator if disabling or enabling pointcloud data import.
     */
    void SetPointCloudDataImport(bool status);

    /**
     * Enables or disables import of instruments based on 'status'
     * Start or ends importInstrumentThread.
     * 
     * @param status - Indicator if disabling or enabling instrument import.
     */
    void SetInstrumentDataImport(bool status);

    /**
     * Returns the data of the latest received VST camera image for a given channel.
     * 
     * @param[in] ch - The channel of the image requested (0 for left VST image, 1 for right VST image).
     * @param[out] resolution - The resolution of the image as pair (first value width, second height).
     * @param[out] channels - Color channels of the received image.
     * @param[out] data - Data vector containing image in bytes.
     * @param[out] principalPointX - Principal point in X dimension.
     * @param[out] principalPointY - Principal point in Y dimension.
     * @param[out] focalLengthX - Focal length in X dimension.
     * @param[out] focalLengthY - Focal length in Y dimension.
     * @param[out] extrinsics - Extrinsics of the camera that created the image.
     * @param[out] HMDPose - HMD Pose when the image was created.
     * 
     * @return Indicator if the data could be successfully retrieved.
     */
    bool getColorFrame(int ch, std::pair<int,int>& resolution, int& channels,std::vector<uint8_t>& data, double& principalPointX, double& principalPointY, double& focalLengthX, double& focalLengthY, double* extrinsics, double* HMDPose);

    /**
     * Returns the data of the latest received pointcloud.
     * 
     * @param[out] normals - An array of unpacked normals for each pointcloud point.
     * @param[out] colors - An array of unpacked colors for each pointcloud point.
     * @param[out] positions - An array of unpacked positions for each pointcloud point.
     * @param[out] radii - An array of unpacked radii for each pointcloud point.
     * @param[out] ids - An array of unpacked ids for each pointcloud point.
     * @param[out] confidences - An array of unpacked confidences for each pointcloud point.
     * 
     * @return Indicator if the data could be successfully retrieved.
     */
    bool getPointCloudPoints(TArray<FVector3f>& normals, TArray<FColor>& colors, TArray<FVector3f>& positions, TArray<float>& radii, TArray<uint32_t>& ids, TArray<uint32_t>& confidences);

    /**
     * Returns the data of the latest received instrument vector.
     *
     * @param[out] names - An array of the names for each instrument.
     * @param[out] centers - An array of the center points for each instrument.
     * @param[out] tops - An array of the top pixel direction vectors for each instrument.
     * @param[out] lefts - An array of the left pixel direction vectors for each instrument.
     * @param[out] rights - An array of the right pixel direction vectors for each instrument.
     * @param[out] bottoms - An array of the bottom pixel direction vectors for each instrument.
     * @param[out] normals - An array of the normals for each instrument.
     * @param[out] camPositions - An array of the camera positions for each instrument. The camera position when the source image was taken.
     * 
     * @return Indicator if the data could be successfully retrieved.
     */
    bool getInstrumentData(TArray<FString>& names, TArray<FVector>& centers, TArray<FVector>& tops, TArray<FVector>& lefts, TArray<FVector>& rights, TArray<FVector>& bottoms, TArray<FVector>& normals, TArray<FVector>& camPositions);
    
private:
    
    /**
     * Method that imports the VST frames.
     * Executed by importVSTThread as long as importVSTData is true.
     * First tries to open VST memory segments, then opens semaphores and checks if a frame is available in shared memory.
     * If a frame is available, it locks the VSTFrameMutex and writes the data from shared memory to the m_LatestVSTFrame variable.
     * Aborts import process if any boost error is thrown.
     * 
     */
    void importVSTFrame();

    /**
     * Method that imports the pointcloud data.
     * Executed by importPointCloudThread as long as importPointCloud is true.
     * First tries to open Pointcloud memory segments, then opens semaphores and checks if an array of pointcloud points is available in shared memory.
     * If an array of pointcloud points is available, it locks the pointCloudMutex and writes the data from shared memory to the m_LatestPointCloudSnapshotContent variable.
     * Aborts import process if any boost error is thrown.
     */
    void importPointCloudData();

    /**
     * Method that imports the instrument data.
     * Executed by importInstrumentThread as long as importInstrument is true.
     * First tries to open instrument memory segments, then opens semaphores and checks if an instrument vector is available in shared memory.
     * If an instrument vector is available, it locks the instrumentMutex and writes the data from shared memory to the m_LatestInstrumentData variable.
     * Aborts import process if any boost error is thrown.
     */
    void importInstrumentData();
    
private:

    ///Indicator if VST data is currently imported.
    bool importVSTData = false;

    ///Indicator if pointcloud data is currently imported.
    bool importPointCloud = false;

    ///Indicator if instrument data is currently imported.
    bool importInstrument = false;

    ///Thread that imports VST frames.
    std::thread importVSTThread;

    ///Thread that imports pointcloud arrays.
    std::thread importPointCloudThread;

    ///Thread that imports instrument vectors.
    std::thread importInstrumentThread;
    
    ///Contains the latest VST frame received with shared memory.
    VSTFrame m_LatestVSTFrame;

    ///Contains the latest pointcloud snapshot received with shared memory.
    PointCloudSnapshotContent m_LatestPointCloudSnapshotContent;

    ///Contains the instrument vector received with shared memory.
    std::vector<InstrumentLocal> m_LatestInstrumentData;

    ///Mutex for avoiding concurrent access to m_LatestVSTFrame from thread and in getColorFrame.
    mutable std::mutex VSTFrameMutex;

    ///Mutex for avoiding concurrent access to m_LatestPointCloudSnapshotContent from thread and in getPointCloudPoints.
    mutable std::mutex pointCloudMutex;

    ///Mutex for avoiding concurrent access to m_LatestInstrumentData from thread and in getInstrumentData.
    mutable std::mutex instrumentMutex;

/////////////////////////////////////////////////////////////////////
//The functions and unions below are extracted from the glm library to unpack unit32 to two floats (func_packing.hpp, type_half.hpp)
/////////////////////////////////////////////////////////////////////
private:
    union uif32
    {
        float f;
        uint32 i;

        uif32() :
            i(0)
        {};

        uif32(float f_) :
            f(f_)
        {};

        uif32(uint32 i_) :
            i(i_)
        {};
    };

    /**
     * Unpacks a int32 to two float32 values. 
     * 
     * @param v - an unsigned 32 bit integer
     */
    FVector2f unpackHalf2x16(unsigned int v);

    /**
     * Converts a short to a float32.
     * 
     * @param value - a short value
     */
    float toFloat32(short value);

};
