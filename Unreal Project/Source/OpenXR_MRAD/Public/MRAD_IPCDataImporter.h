// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TextureRenderTarget2D.h"
#include "MRAD_IPCConnectorClient.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Interactable.h"
#include "InteractableManager.h"
#include <tuple>
#include <limits>
#include <thread>
#include <mutex>
#include "Misc/MonitoredProcess.h"
#include "MRAD_IPCDataImporter.generated.h"

/**
 * Interfaces between Unreal Engine classes and ipc data connection. Additionally starts other processes used with this project.
 * 
 * This class executes the Varjo SDK Project and Object Detection Project processes when the Unreal Engine application is started.
 * The processes are terminated when the UE application ends.
 * To correctly set up the IPC, each process is started after a specific time span.
 * We provide methods to read a color frame from IPC and provide its data to the FrameVisualizer class.
 * Additionally a depth image can be created using imported point cloud points and VST frames.
 * The instruments received by the IPCConnectorClient are utilized to place interactables at instrument locations.
 */

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class OPENXR_MRAD_API UMRAD_IPCDataImporter : public UActorComponent
{
	GENERATED_BODY()

public:	
	/**
	 * Sets default values for this actor's properties.
	 * Loads preset blueprint class derived from interacatable which is used for spawning interactables at instrument locations.
	 */
	UMRAD_IPCDataImporter();


protected:
	/**
	 * Called when the game starts or when spawned.
	 * Initializes matrix for converting from Varjo's to UE's coordinate system.
	 * Starts first timer before starting Varjo SDK project, so that the UE OpenXR plugin can start correctly.
	 */
	virtual void BeginPlay() override;

	/**
	 * Called when the game ends.
	 * Ends processes and resets process handles when application is ended.
	 * Also resets IPCConnectorClient
	 */
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	/**
	 * Called every frame.
	 * Calls manageNewInstruments to process new instrument data received by the IPCConnector.
	 */
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/**
	 * Called for receiving the latest received color frame.
	 * The image received uses 8 bit per color channel and usually RGBA format.
	 * 
	 * @param channel - Channel of image requested (0 for left VST image, 1 for right).
	 * 
	 * @return Tupel containing if the frame is valid, the width, the height, the number of color channels, and a vector of bytes with the image data. 
	 */
	virtual std::tuple<bool,int, int,int,std::vector<uint8_t>> getColorFrame(int channel);

	/**
	 * Called for receiving a depth frame calculated from the latest color frame and the pointcloud data.
	 * The image received uses 8 bit per color channel and RGBA format.
	 * 
	 * We project the point cloud points to the same camera projection plane where the color image was taken using the extrinsic and intrinsic camera paramters provided with the VST image.
	 * 
	 * @param channel - Channel of image requested (0 for left VST image, 1 for right).
	 * 
	 * @return Tupel containing if the frame is valid, the width, the height, the number of color channels, and a vector of bytes with the image data.
	 */
	virtual std::tuple<bool, int, int, int, std::vector<uint8_t>> getDepthFrame(int channel);

	/**
	 * Called for receiving the latest pointcloud snapshot points.
	 * 
	 * @param[out] valid - Indicator if the pointcloud points could be acquired and are valid.
	 * @param[out] normals - An array of normals for each pointcloud point.
	 * @param[out] colors - An array of colors for each pointcloud point.
	 * @param[out] positions - An array of positions for each pointcloud point.
	 * @param[out] radii - An array of radii for each pointcloud point.
	 */
	UFUNCTION(BlueprintCallable)
	virtual void getPointCloudPoints(bool& valid, TArray<FVector3f>& normals,TArray<FColor>& colors, TArray<FVector3f>& positions, TArray<float>& radii);

	/**
	 * Indicator if IPCConnector has been initialized.
	 */
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	bool IPCConnectorClientInitialized = false;

private:

	/**
	 * Sets up input events for toggling automatic placement of interactables.
	 */
	virtual void setupInput();


	/**
	 * Manages input and toggles automatic placement of interactables based on it.
	 */
	virtual void manageInput();

	/**
	 * Creates IPCConnector instance and starts import processes.
	 */
	virtual void initIPCConnectorClient();

	/**
	 * Starts the Varjo SDK Project process which exports VST and Depth data through IPC.
	 * Creates timer after which the Object detection is started, since the object detection needs memory segments created in the Varjo SDK process.
	 */
	virtual void startExportProcess();

	/**
	 * Starts the Object Detection Project process which imports VST data, processes the images and exports deprojected instrument data.
	 * Creates timer after which the IPCConnection is initialized, since this needs the instrument memory segments of the Object detection process.
	 */
	virtual void startObjectDetection();

	/**
	 * Processes the output from the object detection process, which is received through pipes.
	 */
	virtual void processPipeOutput();

	/**
	 * Retrieves newly received instruments from IPCConnector and spawns or updates interactables according to it.
	 * 
	 * First retrieves current instrument list from IPCConnector.
	 * The for each instrument it:
	 *	-Transforms the vectors to UE coordinate system.
	 *  -Checks if a interactable with the instrument name exists:
	 *		-If so: retrieves the pointer to it and updates the position provided with the center value.
	 *		-Else: Spawns a new instrument at the location provided with the center value and adds it to the list of mapped instruments.
	 *  -Calculates the new rotation by using the provided normal.
	 *	-Calculates the new width and height by intersecting the direction vectors from top,right,left,bottom with the plane described by the normal and location.
	 *  -Adjusts the rotation so that the local y-axis of the instrument points in the height axis direction of the instrument.
	 *  -Applys the height scale to the y-axis of the interactable and the width scale to the x-axis.
	 * 
	 */
	virtual void manageNewInstruments();

	/**
	 * Helper functions ot compute an intersection of the top,left,right,bottom direction vectors with the plane defined by the center coordinate and normal.
	 * 
	 * @param planeCenter - the center of the plane with which we compute the intersection.
	 * @param planeNormal - the normal of the plane with which we compute the intersection.
	 * @param start - the start point of the projection line we want to intersect with the plane.
	 * @param direction - the direction of the projection line we want to intersect with the plane.
	 * 
	 * @return A vector at the intersection point of the line with the plane.
	 */
	virtual FVector computeIntersectionOnPlane(FVector planeCenter, FVector planeNormal, FVector start, FVector direction);

	/// The handle to the IPCConnectorClient
	std::unique_ptr<MRAD_IPCConnectorClient> m_IPCConnectorClient;

	/// The process handle for the Varjo SDK project process.
	FProcHandle MRAndDepthExportHandle;

	/// The process handle for the ObjectDetection project process.
	FProcHandle ObjectDetectionHandle;

	/// The list of already created interactables mapped after the name provided with the instrument they represent.
	TMap <FString, AInteractable*> mappedInstruments{};

	/// A matrix for transforming Varjo coordinate system coordinates to UE coordinate system coordinates. (Varjo = Right handed: X: RIGHT Y: UP Z: BACKWARDS; UE = Left handed: X: FORWARD Y: RIGHT Z: UP)
	FMatrix TransformVarjoToUnreal;

	/// The class of interactable spawned at instrument positions.
	TSubclassOf<AInteractable> InstrumentInteractable;

	//Indicator if currently interactables are placed based on object detection.
	bool automaticallyPlaceInteractables = false;

	//Indicator if this can currently receive inputs.
	bool canReceiveInput = true;

	//Pipes for reading output from the object detection
	void* ReadPipe;
	void* WritePipe;

	//Indicator if the output from the object detection should be processed.
	bool processPipes = false;

	///Thread that processes the output of the object detection process.
	std::thread processPipesThread;
};
