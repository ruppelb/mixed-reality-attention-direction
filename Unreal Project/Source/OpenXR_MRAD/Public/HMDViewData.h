// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "IXRTrackingSystem.h"
#include "IHeadMountedDisplay.h"
#include "HMDViewData.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class OPENXR_MRAD_API UHMDViewData : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHMDViewData();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	UFUNCTION(BlueprintCallable)
	virtual void getHMDViewData();

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	float monitorSizeX;
	
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	float monitorSizeY;
		
};
