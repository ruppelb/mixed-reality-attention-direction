// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"
#include "MRAD_IPCDataImporter.h"
#include "Materials/MaterialExpressionTextureSampleParameter2D.h"
#include "Materials/MaterialExpressionTextureCoordinate.h"
#include <thread>
#include <mutex>
#include "MRAD_FrameVisualizer.generated.h"

UCLASS()
class OPENXR_MRAD_API AMRAD_FrameVisualizer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMRAD_FrameVisualizer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Called when the game ends
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UMaterialInterface* frameMaterial;
	UPROPERTY(VisibleAnywhere)
		UMaterialInstanceDynamic* frameMaterialDynamic;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		UStaticMeshComponent* frameMesh;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (EditCondition = "!displayDepthFrame"))
		bool displayColorFrame = true;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (EditCondition = "!displayColorFrame"))
		bool displayDepthFrame = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool displayRightChannel = false;

private:

	virtual void drawColorFrame();

	virtual void drawDepthFrame();

	virtual void acquireLatestDepthFrame();

	UMRAD_IPCDataImporter* dataImporter;

	UTexture2D* colorFrameTexture;

	UTexture2D* depthFrameTexture;

	std::thread depthFrameThread;

	mutable std::mutex depthFrameMutex;

	bool concurrentlyGetDepthFrame = false;

	std::tuple<bool, int, int, int, std::vector<uint8_t>> latestDepthFrame;
};
