// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "IHeadMountedDisplay.h"
#include "IXRTrackingSystem.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "ADHISMeshComponent.generated.h"

/**
 * Displays a frame around the an interactable or the actor it is attached to including the from the pawn visible object bounds.
 * 
 * The frame consists of 4 mesh instances of the same mesh that are positioned to form a frame (e.g. one at the bottom, one right, one left, one at the top).
 * This frame supports dynamic scaling in the local x and y dimensions, to incorporate object bounds even when the pawn position changes.
 * The scaling technique prevents the distortion of other dimensions when scaling one axis (e.g. usually when an actor is scaled in the x dimension, parts of the object in the y dimension are warped).
 * Because of this, only specific meshs are supported. We provide a reticle and a frame mesh.
 * The frame thickness is dynamically scaled and can be also set to a fixed value.
 * The frame automatically scales and positions itself around the parent, so that it includes the from the pawn visible object bounds of its parent component.
 * The parents bounds are estimated with the bounding box coordinates of its mesh component.
 * This class is derived from the UHierarchicalInstancedStaticMeshComponent class.
 */

UCLASS()
class OPENXR_MRAD_API UADHISMeshComponent : public UHierarchicalInstancedStaticMeshComponent
{
	GENERATED_BODY()

public:
	/**
	 * Sets default values for this actor's properties.
	 */
	UADHISMeshComponent();

protected:
	/**
	 * Called when the game starts or when spawned.
	 * Resets important values.
	 * Checks if a HMD is connected and saves the device id.
	 */
	virtual void BeginPlay() override;

public:

	/**
	 * Called every frame.
	 * Manages dynamic rescale and positioning during play.
	 * Adapts the frame to new view perspectives and visible object bounds when the pawn or the parent is moved.
	 * Only excecutes functions when the frame is visible.
	 */
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType,FActorComponentTickFunction* ThisTickFunction) override;
	
	/**
	 * Called in the interactable onConstruction method.
	 * Manages the adaption to the current parent scale, when it is changed within the editor.
	 * Gets actual scale and bounding box from parent and then calls rescale.
	 */
	UFUNCTION(BlueprintCallable)
	virtual void rescaleInEditor();

	/**
	 * Called from within this class to update the transform of the instances that form the mesh according to the arguments specified.
	 * Here, the relative location, rotation and scale of each instance is adapted.
	 * The scaling process works by only scaling the instances that are parallel to the scaling axis.
	 * The other instances are positioned at the positive and negative actor bounds within their axis.
	 * This results in a frame where the dimensions that are not scaled remain undistorted.
	 * 
	 * @param MaxX - The maximal x value of the owner's bounding box (used for upper X instance)
	 * @param MinX - The minimal x value of the owner's bounding box (used for lower X instance)
	 * @param MaxY - The maximal y value of the owner's bounding box (used for upper Y instance)
	 * @param MinY - The minimal y value of the owner's bounding box (used for lower Y instance)
	 * @param scale - The owner's scale
	 */
	UFUNCTION(BlueprintCallable)
	virtual void rescale(float MaxX, float MinX, float MaxY, float MinY, FVector scale);

	/**
	 * Creates the instances and intially positions them in a frame.
	 * Also sets an option, which preventes that the component is automatically scaled whe the parent is scaled.
	 */
	UFUNCTION(BlueprintCallable)
	virtual void CreateInstances();

	/**
	 * Calculates the dynamic frame thickness based on a given scale in x and y axis.
	 * 
	 * @param scaleX - A scale value for the x dimension.
	 * @param scaleY - A scale value for the y dimension.
	 */
	UFUNCTION(BlueprintCallable)
	virtual void calculateDynamicFrameThickness(float scaleX, float scaleY);

	/**
	 * Updates the camera position utilized in the projection during the dynamic adjustment process.
	 * If a HMD was detected at begin play, one camera perspective for each eye is updated and saved.
	 * Otherwise, the camera position of the first player is updated and saved.
	 * 
	 */
	UFUNCTION(BlueprintCallable)
	virtual void updateCameraLocations();

	/**
	 * Removes all instances and creates new ones by calling CreateInstances.
	 * 
	 */
	UFUNCTION(BlueprintCallable)
	virtual void resetInstances();

	/**
	 * The tickness of the frame. Only editable, when dynamic frame thickness is turned off.
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ClampMin = "0.0", UIMin = "0.0", EditCondition = "!dynamicFrameThickness"))
	float frameThickness;

	/**
	 * Indicator if debug information should be displayed.
	 */
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool showDebugInfo = false;

	/**
	 * Indicator if the frame thickness should be computed dynamically.
	 */
	UPROPERTY(VisibleAnywhere)
	bool dynamicFrameThickness;

	/**
	 * Indicator if the instances have been constructed.
	 */
	UPROPERTY(VisibleAnywhere)
	bool constructed = false;

private:
	/// The mesh that is used for creating the instances.
	UStaticMesh* mesh;

	/// The index of the lower Y mesh instance.
	int lowerYIndex;

	/// The index of the upper Y mesh instance.
	int upperYIndex;

	/// The index of the lower X mesh instance.
	int lowerXIndex;

	/// The index of the upper X mesh instance.
	int upperXIndex;

	/// The origin of the plane used for computing the projection intersections from the camera.
	FVector planeOrigin;
	
	/// The normal of the plane used for computing the projection intersections from the camera.
	FVector planeNormal;
	
	/// Holds the x values of the projected coordinates in the plane.
	TArray<float> xArray;
	
	/// Holds the Y values of the projected coordinates in the plane.
	TArray<float> yArray;

	/// The transfrom of the owner.
	FTransform ownerTransform;

	/// Local coordinates of the owners bounding box corners.
	TArray<FVector> boundingBoxLocalCoordinates;
	
	/// World coordinates of the owners bounding box corners.
	TArray<FVector> boxWorldCoordinates;
	
	/// Holds distances of the bounding box coordinates from the camera location.
	TArray<float> distances;

	/// The camera position.
	FVector cameraPosition;
	
	/// The eye positions. Used instead of camera position when using HMD.
	TArray<FVector> eyePositions;
	
	/// Indicator if a HMD device could be detected.
	bool HMDEnabled;
	
	/// Identifier of the HMD device.
	int32 deviceId;
};
