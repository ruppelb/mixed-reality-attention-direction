// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactable.h"
#include "InteractableManager.h"

#if WITH_EDITOR
void AInteractable::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{

	visualizeAttention(visualize);

	//set currentADComponent when ADComponent is selected in properties. ADComponent == NULL if None is selected
	if (IsValid(ADComponent) || ADComponent == NULL) {
		if (IsValid(currentADComponent)) {
			if (currentADComponent->GetClass() != ADComponent.Get()) {
				setUpADComponent();
			}
		}
		else {
			setUpADComponent();
		}
	}

	//set currentADHISMeshComponent when ADHISMeshComponent is selected in properties. ADHISMeshComponent == NULL if None is selected
	if (IsValid(ADHISMeshComponent.Get()) || ADHISMeshComponent.Get() == NULL) {
		if (IsValid(currentADHISMeshComponent)) {
			if (currentADHISMeshComponent->GetClass() != ADHISMeshComponent.Get()) {
				setUpADHISMeshComponent();
			}
		}
		else {
			setUpADHISMeshComponent();
		}
	}

	// Call the base class version
	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif

// Sets default values
AInteractable::AInteractable()
{
	//prevent construction when constructor is called for temporary instance https://forums.unrealengine.com/t/actor-constructor-called-twice/288825
	if (!HasAnyFlags(RF_ClassDefaultObject) && !IsTemplate(RF_Transient)) {

		// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
		PrimaryActorTick.bCanEverTick = true;

		visualizationMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName(FString::Printf(TEXT("VisualizationMesh_%s"), *this->GetName())));
		RootComponent = visualizationMesh;

		//set collision profile for hand collision
		visualizationMesh->SetCollisionProfileName(TEXT("BlockAllDynamic"));
		visualizationMesh->CanCharacterStepUpOn = ECB_No;

		//for mixed reality masking with varjo
		visualizationMesh->SetRenderCustomDepth(true);

		//get visualizationMesh material
		static ConstructorHelpers::FObjectFinder<UMaterialInstance>VisualizationMaterialAsset(TEXT("MaterialInstanceConstant'/Game/Materials/Unlit_Inst_Green_LowAlpha'"));
		loadedVisualizationMaterial = VisualizationMaterialAsset.Object;

		//get masking material
		static ConstructorHelpers::FObjectFinder<UMaterialInstance>MaterialAsset(TEXT("MaterialInstanceConstant'/Game/Materials/Unlit_Inst_Mask'"));
		loadedMaskingMaterial = MaterialAsset.Object;
	}
}

// Called when the game starts or when spawned
void AInteractable::BeginPlay()
{
	Super::BeginPlay();

	//initialize variables
	secondsSinceAttention = 0.0;
	directingAttention = false;
	secondsWithAttention = 0.0;
	currentlyDisplayedWidget = nullptr;

	//if material was not found previously
	if (!IsValid(loadedVisualizationMaterial)) {
		UE_LOG(LogTemp, Warning, TEXT("Material undefined."));
		loadedVisualizationMaterial = visualizationMesh->GetMaterial(0);
	}
	
	//create dynamic Material
	visualizationMaterial = UMaterialInstanceDynamic::Create(loadedVisualizationMaterial, this);
	
	//save start color value
	TArray<FMaterialParameterInfo> info;
	TArray<FGuid> id;
	loadedVisualizationMaterial->GetAllVectorParameterInfo(info,id);
	if (!info.IsEmpty()) {
		visualizationMaterial->GetVectorParameterValue(info[0], visualizationStartColor);
	}

	//set materials of visualization mesh according to visualize (In case visualize is true, now the dynamic material must be set)
	visualizeAttention(visualize);
	
	//set indication mesh invisible at start
	if (IsValid(currentADHISMeshComponent)) {
		currentADHISMeshComponent->SetVisibility(false);
	}
}

void AInteractable::PostLoad()
{
	Super::PostLoad();

	//set up ADHISMesh and ADComponent after object is loaded
	setUpComponents();
}

void AInteractable::PostActorCreated()
{
	Super::PostActorCreated();

	//attach to Interactable manager when creating Interactable
	attachToInteractableManager();

	//set up ADHISMesh and ADComponent after actor is created
	setUpComponents();
}

// Called every frame
void AInteractable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//check if we direct attention
	if (!directingAttention) {

		if (automaticDirectionAfterTimeSpan) {
			//if automatically directing over a given timespan track seconds since attention and trigger attention direction
			secondsSinceAttention += DeltaTime;
			if (secondsSinceAttention >= directAfterSeconds) {
				startAttentionDirection();
			}
			else {
				//change color with gradient over time if visualize is true
				if (visualize) {
					currentColor = FLinearColor();
					if (visualizationMaterial->GetVectorParameterValue(TEXT("Color"), currentColor)) {
						if (currentColor.R < 1.0) {
							currentColor.R += DeltaTime / directAfterSeconds;
						}
						if (currentColor.G > 0.0) {
							currentColor.G -= DeltaTime / directAfterSeconds;
						}
						if (currentColor.B > 0.0) {
							currentColor.B -= DeltaTime / directAfterSeconds;
						}
						visualizationMaterial->SetVectorParameterValue(TEXT("Color"), currentColor);
					}
					else {
						UE_LOG(LogTemp, Warning, TEXT("Cannot access parameter Color."));
					}
				}
			}
		}
	}
	else {
		
		secondsSinceAttentionDirectionTrigger += DeltaTime;
		//TODO: Add different attention direction levels based on secondsSinceAttentionDirectionTrigger

		//update attention direction mechanisms
		if (WasRecentlyRendered()) {
			//In view
			directAttention(level);
		}
		else {
			//Out of view
			directAttentionNotInView(level);
		}
	}

	//always update ADHISMeshComponent values during play
	if (IsValid(currentADHISMeshComponent) && currentADHISMeshComponent!=NULL) {
		updateADHISMeshComponent();
	}
}


void AInteractable::Destroyed()
{
	//properly disconnect from interactableManager if this is destroyed
	if (IsValid(interactableManager)) {
		interactableManager->unregisterInteractable(this);
		this->DetachFromActor(FDetachmentTransformRules::KeepRelativeTransform);
	}
	Super::Destroyed();
}

void AInteractable::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	//update ADHISMeshComponent frame every time interactable is moved in editor
	if (IsValid(currentADHISMeshComponent) && currentADHISMeshComponent != NULL) {
		updateADHISMeshComponent();
		currentADHISMeshComponent->rescaleInEditor();
	}
}

void AInteractable::PostDuplicate(EDuplicateMode::Type DuplicateMode)
{
	Super::PostEditImport();
	
	//since mesh component got bugged on duplication, create new mesh component and apply previous values 
	if (DuplicateMode == EDuplicateMode::Normal) {
		UStaticMesh* copiedMesh = visualizationMesh->GetStaticMesh();
		UMaterialInterface* copiedMaterial = visualizationMesh->GetMaterial(0);
		FTransform copiedTransform = GetActorTransform();
		FVector loc = copiedTransform.GetLocation();

		if (printDebug) {
			#if WITH_EDITOR
			UE_LOG(LogTemp, Warning, TEXT("%s Got location: X %f  Y %f  Z %f"), *GetActorLabel(), loc.X, loc.Y, loc.Z);
			#endif
		}
		
		visualizationMesh->DestroyComponent();
		visualizationMesh =	NewObject<UStaticMeshComponent>(this, FName(FString::Printf(TEXT("VisualizationMesh_%s"), *this->GetName())));
		if (IsValid(visualizationMesh))
		{
			visualizationMesh->CreationMethod = EComponentCreationMethod::Instance;
			//visualizationMesh->SetupAttachment(RootComponent);
			visualizationMesh->RegisterComponent();
			//this->FinishAndRegisterComponent(currentADHISMeshComponent);
			RootComponent = visualizationMesh;
			if (IsValid(copiedMesh)) {
				visualizationMesh->SetStaticMesh(copiedMesh);
			}
			if (IsValid(copiedMaterial)) {
				visualizationMesh->SetMaterial(0, copiedMaterial);
			}
			
			SetActorTransform(copiedTransform);

			if (IsValid(currentADHISMeshComponent)) {
				currentADHISMeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepWorldTransform);
				currentADHISMeshComponent->resetInstances();
				currentADHISMeshComponent->rescaleInEditor();
				
			}

			AttachToActor(interactableManager, FAttachmentTransformRules::KeepRelativeTransform);
		}
		
	}
}

void AInteractable::startAttentionDirection()
{
	if (printDebug) {
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, "Started directing attention");
	}

	//set up attention direction
	directingAttention = true;
	
	//if not automatically directing attention over timespan and visualize is true, set color of mesh manually to red, since there is no automatic gradient
	if (!automaticDirectionAfterTimeSpan && visualize) {
		visualizationMaterial->SetVectorParameterValue(TEXT("Color"), FColor::Red);
	}
	
	//update attention direction mechanisms
	if (WasRecentlyRendered()) {
		directAttention(level);
	}
	else {
		directAttentionNotInView(level);
	}
	
}

void AInteractable::tryStoppingAttentionDirection(float DeltaTime)
{
	if (directingAttention) {
		//if directing, accumulate time of interaction
		secondsWithAttention += DeltaTime;
		if (stopDirectionAfterSeconds <= secondsWithAttention) {
			// if time is greate than threshold stop attention direction
			secondsWithAttention = 0.0;
			stopAttentionDirection();
		}
	}
	else {
		//if not directing, reset attention state on interaction
		stopAttentionDirection();
	}
}

void AInteractable::stopAttentionDirection()
{
	if (printDebug) {
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Blue, "Stopped directing attention");
	}

	//reset values
	secondsSinceAttention = 0.0;
	secondsSinceAttentionDirectionTrigger = 0.0;
	directingAttention = false;

	//update components
	if (IsValid(currentADComponent)) {
		currentADComponent->stopDirectingAttention();
	}
	else {
		if (IsValid(currentADHISMeshComponent)) {
			currentADHISMeshComponent->SetVisibility(false);
		}
	}

	//update color
	visualizationMaterial->SetVectorParameterValue("Color",visualizationStartColor);
}

void AInteractable::directAttention(int32 levelIn)
{
	//remove widget if displayed
	if (IsValid(currentlyDisplayedWidget)) {
		interactableManager->playerHUD->UnregisterInteractableWidget(currentlyDisplayedWidget);
		currentlyDisplayedWidget = nullptr;
	}

	//update components
	if (IsValid(currentADComponent)) {
		currentADComponent->directAttention(levelIn);
	}
	else {
		if (IsValid(currentADHISMeshComponent)) {
			currentADHISMeshComponent->SetVisibility(true);
		}
	}
}

void AInteractable::directAttentionNotInView(int32 levelIn)
{
	//update components
	if (IsValid(currentADComponent)) {
		currentADComponent->directAttentionNotInView(levelIn);
	}
	else {
		//add widget if not already displayed
		if (!IsValid(currentlyDisplayedWidget)) {
			if (IsValid(indicatorWidget)) {
				if (IsValid(interactableManager)) {
					if (IsValid(interactableManager->playerHUD)) {
						currentlyDisplayedWidget = interactableManager->playerHUD->RegisterInteractableWidget(this);
					}	
				}
			}
		
		}
	}
}

void AInteractable::setUpADComponent()
{
	//check if world is valid, to prevent errors when changing components in BP, because then the components can not be registered
	if (IsValid(GetWorld())) {
		//if previous AD Component Added, remove it from this interactable
		if (IsValid(currentADComponent)) {
			currentADComponent->UnregisterComponent();
			currentADComponent->DestroyComponent();
		}

		//set up new AD Component
		if (IsValid(ADComponent)) {
			//create new component and register it
			currentADComponent = NewObject<UADComponent>(this, ADComponent.Get(), TEXT("ADComponent"));
			if (IsValid(currentADComponent))
			{
				currentADComponent->CreationMethod = EComponentCreationMethod::Instance;
				currentADComponent->RegisterComponent();
			}
		}
		else {
			//empty current selection, when no valid class is provided
			currentADComponent = NULL;
		}
	}
}

bool AInteractable::setADComponent(TSubclassOf<UADComponent> component)
{
	if (IsValid(component) || component == NULL) {
		//set the selected class here and then update component with setUpADComponent
		ADComponent = component;
		setUpADComponent();
		return true;
	}
	return false;
}

void AInteractable::setUpADHISMeshComponent()
{
	//check if world is valid, to prevent errors when changing components in BP, because then the components can not be registered
	if (IsValid(GetWorld())) {
		//if previous ADHISMesh Component Added, remove it from this interactable
		if (IsValid(currentADHISMeshComponent)) {
			currentADHISMeshComponent->UnregisterComponent();
			currentADHISMeshComponent->DestroyComponent();
		}

		//set up new ADHISMesh Component
		if (IsValid(ADHISMeshComponent.Get())) {
			//create new component and register it
			currentADHISMeshComponent = NewObject<UADHISMeshComponent>(this, ADHISMeshComponent.Get());
			if (IsValid(currentADHISMeshComponent))
			{
				currentADHISMeshComponent->CreationMethod = EComponentCreationMethod::Instance;
				currentADHISMeshComponent->SetupAttachment(RootComponent);
				currentADHISMeshComponent->RegisterComponent();
				currentADHISMeshComponent->SetRelativeLocation(FVector(0.0, 0.0, 0.2));
			}
		}
		else {
			//empty current selection, when no valid class is provided
			currentADHISMeshComponent = NULL;
		}

	}
}

bool AInteractable::setADHISMeshComponent(TSubclassOf<UADHISMeshComponent> component)
{
	if (IsValid(component) || component == NULL) {
		//set the selected class here and then update component with setUpADHISMeshComponent
		ADHISMeshComponent = component;
		setUpADHISMeshComponent();
		return true;
	}
	return false;
}

void AInteractable::visualizeAttention(bool state)
{
	//update visualization state
	visualize = state;
	if (IsValid(visualizationMesh)) {

		//set mesh material based on visualize
		if (visualize) {

			//if in editor just set material to loaded material, if in game set material to dynamic material instance
			if (IsValid(GWorld)) {
				if (GWorld->WorldType == EWorldType::PIE || GWorld->WorldType == EWorldType::Game) {
					//In game
					visualizationMesh->SetMaterial(0, visualizationMaterial);

					//set start color gradient according to secondsSinceAttention / directAfterSeconds
					float gradientValue = secondsSinceAttention / directAfterSeconds;
					FLinearColor startColor = visualizationStartColor;
					startColor.R = UKismetMathLibrary::FMin(1.0, startColor.R + gradientValue);
					startColor.G = UKismetMathLibrary::FMax(0.0, startColor.G - gradientValue);
					startColor.B = UKismetMathLibrary::FMax(0.0, startColor.B - gradientValue);

					visualizationMaterial->SetVectorParameterValue(TEXT("Color"), startColor);
				}
				else {
					//In editor
					visualizationMesh->SetMaterial(0, loadedVisualizationMaterial);
				}
			}
		}
		else {
			//set masking material if not visualizing attention
			visualizationMesh->SetMaterial(0, loadedMaskingMaterial);
		}
	}
}

void AInteractable::attachToInteractableManager()
{
	//prevent when constructor is called for temporary instance https://forums.unrealengine.com/t/actor-constructor-called-twice/288825
	if (!HasAnyFlags(RF_ClassDefaultObject) && !IsTemplate(RF_Transient)) {

		//check if InteractableManager exists
		TArray<AActor*> FoundActors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AInteractableManager::StaticClass(), FoundActors);

		//if AInteractableManager is existing
		if (!FoundActors.IsEmpty()) {
			//get valid manager
			for (AActor* actor : FoundActors) {
				AInteractableManager* manager = Cast<AInteractableManager>(actor);
				if (IsValid(manager) && manager->isValidInteractableManager) {
					#if WITH_EDITOR
					UE_LOG(LogTemp, Warning, TEXT("Found valid InteractableManager: %s"), *manager->GetActorLabel());
					#endif
					//attach this interactable to InteractableManager
					interactableManager = manager;
					break;
				}
			}

			//register at found manager, if its reference is valid, otherwise a new interactable manager is created
			if (IsValid(interactableManager)) {
				interactableManager->registerInteractable(this);
				this->AttachToActor(interactableManager, FAttachmentTransformRules::KeepRelativeTransform);
			}
			else {
				//did not find a valid manager => create one
				UE_LOG(LogTemp, Warning, TEXT("Found no valid InteractableManager, creating own one."));
				if (UWorld* World = GetWorld()) {
					FVector location = FVector(0, 0, 0);
					FRotator rotation = FRotator(0, 0, 0);
					interactableManager = World->SpawnActor<AInteractableManager>(location, rotation);
				}
			}
		}
		else {
			//did not find any manager => create one
			UE_LOG(LogTemp, Warning, TEXT("Found no InteractableManager, creating own one."));
			if (UWorld* World = GetWorld()) {
				FVector location = FVector(0, 0, 0);
				FRotator rotation = FRotator(0, 0, 0);
				interactableManager = World->SpawnActor<AInteractableManager>(location, rotation);
			}
		}
	}
}

void AInteractable::setInteractableManager(AInteractableManager* newInteractableManager)
{
	//detach from current interactable manager
	this->DetachFromActor(FDetachmentTransformRules::KeepRelativeTransform);

	//if the new interactable manager is valid, unregister this interactable
	if (IsValid(interactableManager) && interactableManager != newInteractableManager)
	{
		interactableManager->unregisterInteractable(this);
		#if WITH_EDITOR
		if (printDebug) {
			UE_LOG(LogTemp, Warning, TEXT("Removed %s from previous interactableManager: %s"), *this->GetActorLabel(), *interactableManager->GetActorLabel());
		}
		#endif
	}

	
	if (IsValid(newInteractableManager)) {
		//if the new interactable manager is valid, register at and attach to it
		interactableManager = newInteractableManager;
		interactableManager->registerInteractable(this);
		this->AttachToActor(interactableManager, FAttachmentTransformRules::KeepRelativeTransform);
		#if WITH_EDITOR
		if (printDebug) {
			UE_LOG(LogTemp, Warning, TEXT("Added %s to new interactableManager: %s"), *this->GetActorLabel(), *interactableManager->GetActorLabel());
		}
		#endif
	}else {
		//otherwise clear current interactable manager
		interactableManager = nullptr;
		#if WITH_EDITOR
		if (printDebug) {
			UE_LOG(LogTemp, Warning, TEXT("Set interactableManager of %s to nullptr"), *this->GetActorLabel());
			
		}
		#endif
	}
}

void AInteractable::setMesh(UStaticMesh* mesh)
{
	if (IsValid(mesh)) {
		//set the currnet visualization mesh
		visualizationMesh->SetStaticMesh(mesh);
	}
}

void AInteractable::updateADHISMeshComponent()
{
	if (!dynamicFrameThickness)
	{
		//update framethickness, if setting the thickness dynamically is disabled
		currentADHISMeshComponent->frameThickness = frameThickness;
	}

	//update if framethickness is set dynamically
	currentADHISMeshComponent->dynamicFrameThickness = dynamicFrameThickness;
}

bool AInteractable::findAndSetADHISMeshComponent()
{
	//search for any ADHISMeshComponent attached to this actor
	UADHISMeshComponent *foundADHISMeshComponent = FindComponentByClass<UADHISMeshComponent>();
	if (IsValid(foundADHISMeshComponent)) {
		//if we found one, update values according to it, reset its instances and return true
		currentADHISMeshComponent = foundADHISMeshComponent;
		ADHISMeshComponent = foundADHISMeshComponent->GetClass();
		currentADHISMeshComponent->resetInstances();
		return true;
	}

	return false;
}

bool AInteractable::findAndSetADComponent()
{
	//search for any ADComponent attached to this actor
	UADComponent* foundADComponent = FindComponentByClass<UADComponent>();
	if (IsValid(foundADComponent)) {
		//if we found one, update values according to it and return true
		currentADComponent = foundADComponent;
		ADComponent = foundADComponent->GetClass();
		return true;
	}
	return false;
}

void AInteractable::setUpComponents()
{
	//try finding already attached adComponent
	if (!findAndSetADComponent()) {
		//if none is found, try regisering new component according to ADComponent selection
		if (IsValid(ADComponent.Get())) {
			setUpADComponent();
		}
		else {
			//if current selection is not valid, clear values
			currentADComponent = NULL;
			ADComponent = NULL;
		}
	}

	//try finding already attached ADHISMeshComponent
	if (!findAndSetADHISMeshComponent()) {
		//if none is found, try registering new component according to ADHISMeshComponent selection
		if (IsValid(ADHISMeshComponent.Get())) {
			setUpADHISMeshComponent();
		}
		else {
			//if current selection is not valid, clear values
			currentADHISMeshComponent = NULL;
			ADHISMeshComponent = NULL;
		}
	}
}

