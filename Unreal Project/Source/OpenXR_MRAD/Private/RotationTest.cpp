// Fill out your copyright notice in the Description page of Project Settings.


#include "RotationTest.h"

// Sets default values
ARotationTest::ARotationTest()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(FName(FString::Printf(TEXT("Mesh_%s"), *this->GetName())));
	RootComponent = Mesh;
}

// Called when the game starts or when spawned
void ARotationTest::BeginPlay()
{
	Super::BeginPlay();
}

void ARotationTest::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	FVector UpVector = GetActorUpVector();
	FVector RightVector = GetActorRightVector();
	FVector ForwardVector = GetActorForwardVector();
	UKismetMathLibrary::Vector_Normalize(UpVector);
	DrawDebugDirectionalArrow(GetWorld(), GetActorLocation(), GetActorLocation() + UpVector * 200, 5.0, FColor::Blue, false, 1.0f, 0U, 2.0f);
	DrawDebugDirectionalArrow(GetWorld(), GetActorLocation(), GetActorLocation() + ForwardVector * 200, 10.0, FColor::Red, false, 1.0f, 0U, 2.0f);
	DrawDebugDirectionalArrow(GetWorld(), GetActorLocation(), GetActorLocation() + RightVector * 200, 10.0, FColor::Green, false, 1.0f, 0U, 2.0f);
}

// Called every frame
void ARotationTest::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ARotationTest::RotateToOrigin()
{
	FVector UpVector = GetActorUpVector();

	FVector NormalVector = RootComponent->GetComponentLocation();
	UKismetMathLibrary::Vector_Normalize(NormalVector);
	NormalVector *= -1;

	FVector RotationAxis = FVector::CrossProduct(UpVector, NormalVector);
	RotationAxis.Normalize();

	float DotProduct = FVector::DotProduct(UpVector, NormalVector);
	float RotationAngle = acosf(DotProduct);

	FQuat NewQuat = FQuat(RotationAxis, RotationAngle);
	FQuat CurrentQuat = GetActorQuat();

	FQuat resultQuat = NewQuat * CurrentQuat;

	SetActorRotation(resultQuat.Rotator());

	float height = 1;
	float width = 2;
	FVector RightVector = GetActorRightVector();
	RightVector = UKismetMathLibrary::InverseTransformDirection(GetActorTransform(), RightVector);

	FVector widthAxis = FVector(-1,0,0);
	UKismetMathLibrary::Vector_Normalize(widthAxis);
	float dotWidth = UKismetMathLibrary::Dot_VectorVector(widthAxis, RightVector);
	dotWidth = UKismetMathLibrary::Abs(dotWidth);

	FVector heightAxis = FVector(0,1,0);
	UKismetMathLibrary::Vector_Normalize(heightAxis);
	float dotHeight = UKismetMathLibrary::Dot_VectorVector(heightAxis, RightVector);
	dotHeight = UKismetMathLibrary::Abs(dotHeight);

	if (dotWidth >= dotHeight) {
		//RightVector/local Y axis is width
		SetActorScale3D(FVector(height, width, 1.0));
	}
	else {
		//RightVector/local Y axis is height
		SetActorScale3D(FVector(width, height, 1.0));
	}
}

void ARotationTest::RotateToOriginNew()
{
	
	FVector UpVector = GetActorUpVector();

	FVector NormalVector = RootComponent->GetComponentLocation();
	UKismetMathLibrary::Vector_Normalize(NormalVector);
	NormalVector *= -1;

	//use find between normals here, because the exat rotation axis does not matter
	FQuat test = FQuat::FindBetweenNormals(UpVector, NormalVector);
	
	FQuat CurrentQuat = GetActorQuat();

	FQuat resultQuat = test * CurrentQuat;

	SetActorRotation(resultQuat.Rotator());


	
	//adjust rotation to width height axis

	FVector RightVector = GetActorRightVector();

	FVector heightAxis = FVector(0, 0, 1);
	heightAxis.Normalize();

	FVector newHeight = UKismetMathLibrary::ProjectVectorOnToPlane(heightAxis, NormalVector);
	newHeight.Normalize();
	

	//here we must use the normal axis for rotation, because we do not want to change where the plane is facing, we only want to align the y axis width the height axis
	float DotProduct2 = FVector::DotProduct(RightVector, newHeight);
	float RotationAngle2 = acosf(DotProduct2);
	UE_LOG(LogTemp, Warning, TEXT("Angle: %f"), RotationAngle2);

	FVector RotationAxis2 = FVector::CrossProduct(RightVector, newHeight);
	RotationAxis2.Normalize();

	FQuat NewQuat2;

	//here we need to consider, that the rotation axis is 0, when both vectors are facing the opposite or the same direction. In this case we just use the plane normal as axis.
	if (RotationAxis2.Equals(FVector(0, 0, 0))) {
		NewQuat2 = FQuat(NormalVector, RotationAngle2);
		UE_LOG(LogTemp, Warning, TEXT("AXIS 0"));
	}
	else {
		NewQuat2 = FQuat(RotationAxis2, RotationAngle2);
		UE_LOG(LogTemp, Warning, TEXT("AXIS not 0"));
	}

	UE_LOG(LogTemp, Warning, TEXT("QUAT: X %f  Y %f  Z %f  W %f"), NewQuat2.X, NewQuat2.Y, NewQuat2.Z, NewQuat2.W);

	FQuat CurrentQuat2 = GetActorQuat();

	FQuat resultQuat2 = NewQuat2 * CurrentQuat2;

	SetActorRotation(resultQuat2.Rotator());



	float height = 1;
	float width = 2;

	//RightVector/local Y axis is height
	SetActorScale3D(FVector(width, height, 1.0));
}

