// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/MRSettings_Widget.h"

void UMRSettings_Widget::NativeConstruct()
{
	Super::NativeConstruct();
	
	if (IsValid(HorizontalBox)) {
		if (IsValid(Label)) {
			//initialize label textblock
			HorizontalBox->AddChildToHorizontalBox(Label);
			FSlateFontInfo font = Label->Font;
			font.Size = 16;
			Label->SetFont(font);
		}
		if (IsValid(Spacer)) {
			//initialize spacer
			HorizontalBox->AddChildToHorizontalBox(Spacer);
			UHorizontalBoxSlot* boxSlot = Cast<UHorizontalBoxSlot>(Spacer->Slot);
			if (IsValid(boxSlot)) {
				boxSlot->SetSize(FSlateChildSize(ESlateSizeRule::Fill));
			}
		}
		if (IsValid(Value)) {
			//initialize value textblock
			HorizontalBox->AddChildToHorizontalBox(Value);
			FSlateFontInfo font = Value->Font;
			font.Size = 16;
			Value->SetFont(font);
		}
	}

	//update labels on the widget components initialized above
	updateLabel();
}

void UMRSettings_Widget::NativeTick(const FGeometry& MyGeometry, float DeltaTime)
{
	Super::NativeTick(MyGeometry,DeltaTime);
	//update enabledstate every tick
	updateValue();
}

void UMRSettings_Widget::updateValue()
{
	//set text and color based on current enabledstate
	switch (getEnabledState()) {
	case Enabled:
		Value->SetText(FText::FromString(EnabledText));
		Value->SetColorAndOpacity(FSlateColor(EnabledColor));
		break;
	case Disabled:
		Value->SetText(FText::FromString(DisabledText));
		Value->SetColorAndOpacity(FSlateColor(DisabledColor));
		break;
	case Unsupported:
		Value->SetText(FText::FromString(UnsupportedText));
		Value->SetColorAndOpacity(FSlateColor(UnsupportedColor));
		break;
	}
}

EnabledState UMRSettings_Widget::getEnabledState()
{
	//request if MRSetting is supported and enabled from blueprint
	bool isSupported;
	bool isEnabled;
	getState(isSupported,isEnabled);

	//return EnabledState based in this information
	if (isSupported) {
		if (isEnabled) {
			return EnabledState::Enabled;
		}
		else {
			return EnabledState::Disabled;
		}
	}
	else {
		return EnabledState::Unsupported;
	}
}

void UMRSettings_Widget::updateLabel()
{
	//gets current toggle key from blueprint.
	FKey key = getToggleKey();
	if (UKismetInputLibrary::Key_IsValid(key)) {
		//when a key is assigned, update label with statename and togglekey
		FFormatNamedArguments Args;
		Args.Add("Label", FText::FromString(StateName));
		Args.Add("ToggleKey", key.GetDisplayName());

		FText FormattedText = FText::Format(FText::FromString("{Label} ({ToggleKey}):"),Args);
		Label->SetText(FormattedText);
	}
	else {
		//when no key is assigned, only update label with statename
		FFormatNamedArguments Args;
		Args.Add("Label", FText::FromString(StateName));

		FText FormattedText = FText::Format(FText::FromString("{Label}:"),Args);
		Label->SetText(FormattedText);
	}
}