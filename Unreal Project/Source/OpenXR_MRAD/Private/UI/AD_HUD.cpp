// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/AD_HUD.h"

void UAD_HUD::NativeConstruct()
{
	Super::NativeConstruct();

	//try to get slot of the overlay on the canvas
	UCanvasPanelSlot* CanvasSlot = Cast<UCanvasPanelSlot>(IndicatorOverlay->Slot);
	if (IsValid(CanvasSlot)) {
		//initialize overlay widget
		CanvasSlot->SetAnchors(FAnchors(0.5,0.5,0.5,0.5));
		CanvasSlot->SetPosition(FVector2D(0, 0));
		CanvasSlot->SetAlignment(FVector2D(0.5, 0.5));
		CanvasSlot->SetSize(FVector2D(700, 700));
	}

	//try to get slot of the debugbox on the canvas
	CanvasSlot = Cast<UCanvasPanelSlot>(DebugBox->Slot);
	if (IsValid(CanvasSlot)) {
		//initialize the vertical box
		CanvasSlot->SetAnchors(FAnchors(0.5, 0.5, 0.5, 0.5));
		CanvasSlot->SetPosition(FVector2D(0, 0));
		CanvasSlot->SetAlignment(FVector2D(0.5, 0.5));
		CanvasSlot->SetSize(FVector2D(700, 700));
		CanvasSlot->SetZOrder(1);
	}
}

UAD_Indicator_Widget* UAD_HUD::RegisterInteractableWidget(AInteractable* interactable)
{
	if (IsValid(interactable) && IsValid(interactable->indicatorWidget)) {
		//create the widget
		UUserWidget* widget = CreateWidget(this,interactable->indicatorWidget);
		UAD_Indicator_Widget* indicatorWidget = Cast < UAD_Indicator_Widget>(widget);

		//initialize variables
		indicatorWidget->interactable = interactable;
		indicatorWidget->playerCamera = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0);
		
		//add the widget to the overlay
		IndicatorOverlay->AddChildToOverlay(indicatorWidget);
		return indicatorWidget;
	}
	return nullptr;
}

void UAD_HUD::UnregisterInteractableWidget(UAD_Indicator_Widget* widget)
{
	if (IsValid(widget) && IsValid(IndicatorOverlay)) {
		//Remove the widget from the overlay and destroy it
		IndicatorOverlay->RemoveChild(widget);
		widget->SetVisibility(ESlateVisibility::Hidden);
		widget->ConditionalBeginDestroy();
	}
}

void UAD_HUD::RegisterWidgetOnDebugBox(UWidget* widget)
{
	if (IsValid(widget) && IsValid(DebugBox)) {
		//add widget to debugbox
		DebugBox->AddChildToVerticalBox(widget);
	}
}

void UAD_HUD::UnregisterWidgetOnDebugBox(UWidget* widget)
{
	if (IsValid(widget) && IsValid(DebugBox)) {
		if (DebugBox->HasChild(widget)) {
			//remove widget if existing
			DebugBox->RemoveChild(widget);
		}
	}
}
