// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/AD_Indicator_Widget.h"

void UAD_Indicator_Widget::NativeConstruct()
{
	Super::NativeConstruct();
	//try to get slot of the border on the canvas
	UCanvasPanelSlot* CanvasSlot = Cast< UCanvasPanelSlot >(border->Slot);

	if (IsValid(CanvasSlot)) {
		//initiate border widget
		CanvasSlot->SetAnchors(FAnchors(0.5, 0.5, 0.5, 0.5));
		CanvasSlot->SetPosition(FVector2D(0, 0));
		CanvasSlot->SetAlignment(FVector2D(0.5, 0.5));
		CanvasSlot->SetSize(FVector2D(700, 700));
		material = border->GetDynamicMaterial();
	}
}

void UAD_Indicator_Widget::NativeTick(const FGeometry& MyGeometry, float DeltaTime)
{
	Super::NativeTick(MyGeometry, DeltaTime);
	if (IsValid(interactable)) {
		//Update camera and interactable locations and other vectors
		FVector cameraLocation = playerCamera->GetCameraLocation();
		FVector cameraForward = playerCamera->GetActorForwardVector();
		FVector cameraUp = playerCamera->GetActorUpVector();
		FVector interactableLocation = interactable->GetActorLocation();

		//get direction from camera to interactable
		FVector direction = UKismetMathLibrary::GetDirectionUnitVector(cameraLocation,interactableLocation);

		//set width based on size of angle from forward Vector and direction to interactable location
		float indicatorWidth = UKismetMathLibrary::MapRangeClamped(UKismetMathLibrary::Dot_VectorVector(cameraForward, direction),-1.0,1.0, maxWidth, 0.0);

		//update parameter according to new width
		material->SetScalarParameterValue("Alpha", UKismetMathLibrary::FMax(0.0, indicatorWidth));
		
		//project direction Vector on camera plane to get 2D representation of 3D Vector on screen
		FVector projectedVector = UKismetMathLibrary::ProjectVectorOnToPlane(direction,cameraForward);
		UKismetMathLibrary::Vector_Normalize(projectedVector);

		//calculate angle on screen between 0 and 180 degrees
		double angle = UKismetMathLibrary::DegAcos(UKismetMathLibrary::Dot_VectorVector(UKismetMathLibrary::NegateVector(cameraUp),projectedVector));

		//dot product of cross product vector with camera forward vector, to get sign for mapping range to -180 to 180 degrees
		FVector cross = UKismetMathLibrary::Cross_VectorVector(UKismetMathLibrary::NegateVector(cameraUp),projectedVector);
		float sign = UKismetMathLibrary::Dot_VectorVector(cameraForward,cross);
		
		//apply sign
		if (sign < 0.0) {
			angle = -angle;
		}

		//map to 0-1 range, since material parameter takes 0 for up direction and 0.5 for down direction on screen
		float result = UKismetMathLibrary::MapRangeClamped(angle, -180, 180, 0.0, 1.0);
		material->SetScalarParameterValue("Angle",result);

		//TODO: optional change alphaIntensity of material based on attention direction level?

	}
	else {
		RemoveFromParent();
		SetVisibility(ESlateVisibility::Hidden);
		ConditionalBeginDestroy();
	}
}
