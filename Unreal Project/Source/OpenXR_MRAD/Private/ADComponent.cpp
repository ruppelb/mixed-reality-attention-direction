// Fill out your copyright notice in the Description page of Project Settings.


#include "ADComponent.h"

// Sets default values for this component's properties
UADComponent::UADComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

}


// Called when the game starts
void UADComponent::BeginPlay()
{
	Super::BeginPlay();
	
}


// Called every frame
void UADComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	/*
	if (directInView) {
		//perform attention direction within view here


	}
	if (directOutOfView) {
		//perform attention direction outside the visible field of view here


	}
	*/
}

void UADComponent::directAttention(int32 level)
{
	directInView = true;
	directOutOfView = false;
	this->ADLevel = level;
}

void UADComponent::directAttentionNotInView(int32 level)
{
	directInView = false;
	directOutOfView = true;
	this->ADLevel = level;
}

void UADComponent::stopDirectingAttention()
{
	directInView = false;
	directOutOfView = false;
	ADLevel = 0;
}

