// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractableManager.h"

// Sets default values
AInteractableManager::AInteractableManager()
{
	//prevent construction when constructor is called for temporary instance https://forums.unrealengine.com/t/actor-constructor-called-twice/288825
	if (!HasAnyFlags(RF_ClassDefaultObject) && !IsTemplate(RF_Transient))
	{
		// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
		PrimaryActorTick.bCanEverTick = true;
		RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneRoot"));
		RootComponent->SetMobility(EComponentMobility::Movable);

		//initialize variables
		visualize = false;
		playerHUD = nullptr;
	}

}

// Called when the game starts or when spawned
void AInteractableManager::BeginPlay()
{
	Super::BeginPlay();
	

	//set timer before getting playerHUD, because creation op widgets can take a few frames
	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AInteractableManager::initPlayerHUD, 2, false);
	
}

// Called every frame
void AInteractableManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AInteractableManager::PostLoad()
{
	Super::PostLoad();
	checkValidity();
}

void AInteractableManager::PostActorCreated()
{
	Super::PostActorCreated();
	checkValidity();
}

void AInteractableManager::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	//destroy this, if it is flagged invalid
	if (!isValidInteractableManager) {
		Destroy();
	}

	//lock actor to world origin while in in editor
	SetActorLocation(FVector(0,0,0));
}

void AInteractableManager::Destroyed()
{
	if (isValidInteractableManager)
	{
		if (printDebug) {
		#if WITH_EDITOR
			UE_LOG(LogTemp, Warning, TEXT("InteractableManager: Destroyed valid InteractableManager Label: %s"), *this->GetActorLabel());
		#endif
		}

		//reset interactable manager for all valid registered interactables
		if (!interactableList.IsEmpty()) {
			TArray<AInteractable*> interactablesToUpdate;
			//get all valid interactables
			for (AInteractable* interactable : interactableList) {
				if (IsValid(interactable)) {
					interactablesToUpdate.Add(interactable);
				}
			}
			//remove interactable manager reference
			if (!interactablesToUpdate.IsEmpty()) {
				for (AInteractable* interactable : interactablesToUpdate)
				{
					interactable->setInteractableManager(nullptr);
				}
			}
		}
		isValidInteractableManager = false;
	}
	else {
		if (printDebug) {
		#if WITH_EDITOR
			UE_LOG(LogTemp, Warning, TEXT("InteractableManager: Destroyed invalid InteractableManager Label: %s"), *this->GetActorLabel());
		#endif	
		}
	}
	GEngine->ForceGarbageCollection();
	Super::Destroyed();
}

void AInteractableManager::checkValidity()
{
	//prevent checking for validity when this method is called for temporary instance https://forums.unrealengine.com/t/actor-constructor-called-twice/288825
	if (!HasAnyFlags(RF_ClassDefaultObject) && !IsTemplate(RF_Transient))
	{
		//check for other interactableManagers, since only one should exist
		TArray<AActor*> FoundActors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AInteractableManager::StaticClass(), FoundActors);

		if (!FoundActors.IsEmpty())
		{
			//add all valid actors to managers list.
			TArray<AInteractableManager*> managers;
			for (AActor* actor : FoundActors) {
				if (IsValid(actor)) {
					AInteractableManager* manager = Cast<AInteractableManager>(actor);
					managers.Add(manager);
					if (printDebug) {
					#if WITH_EDITOR
						UE_LOG(LogTemp, Warning, TEXT("InteractableManager: Found InteractableManager Label: %s"), *manager->GetActorLabel());
						UE_LOG(LogTemp, Warning, TEXT("%s"), manager->isValidInteractableManager ? TEXT("valid") : TEXT("invalid"));
					#endif	
					}
				}
			}

			//remove self reference from list
			managers.Remove(this);

			//check list for a valid manager
			if (!managers.IsEmpty()) {
				isValidInteractableManager = true;
				for (AInteractableManager* manager : managers) {
					if (IsValid(manager) && manager->isValidInteractableManager) {
						//we found a valid manger, so this is invalid
						if (printDebug) {
						#if WITH_EDITOR
							UE_LOG(LogTemp, Warning, TEXT("InteractableManager: Found valid InteractableManager Label: %s"), *manager->GetActorLabel());
						#endif
						}
						isValidInteractableManager = false;
						break;
					}
				}
			}
			else {
				//No other managers found beside this. Therefore this is valid. 
				isValidInteractableManager = true;
			}
		}
		else {
			//Found no interactable managers(should never be the case, since at least the calling actor must be included). In any case, this would be valid.
			isValidInteractableManager = true;
		}
	}
	else {
		//A temporary instance is an invalid instance 
		isValidInteractableManager = false;
	}

	if (isValidInteractableManager)
	{
		attachAllInteractables();
	}
}

void AInteractableManager::resetInteractablesToPlayer()
{
	if (UWorld* World = GetWorld())
	{
		//get pawn position
		FVector pawnPos = World->GetFirstPlayerController()->GetPawn()->GetActorLocation();
		//set position of this to pawn position
		RootComponent.Get()->SetWorldLocation(pawnPos);
		//get pawn rotation
		FRotator pawnRotation = World->GetFirstPlayerController()->GetPawn()->GetActorRotation();
		//set rotation of this to pawn rotation
		RootComponent.Get()->SetWorldRotation(pawnRotation);
	}
}

void AInteractableManager::registerInteractable(AInteractable* interactable)
{
	interactableList.Add(interactable);
}

void AInteractableManager::unregisterInteractable(AInteractable* interactable)
{
	interactableList.Remove(interactable);
}

void AInteractableManager::attachAllInteractables()
{
	//get all Interactables in Scene
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AInteractable::StaticClass(), FoundActors);

	//only do something if any interactable exists
	if (!FoundActors.IsEmpty()) {

		//get already attached actors
		TArray<AActor*> attachedActors;
		this->GetAttachedActors(attachedActors);

		
		//remove childActors, that are already attached
		if (!attachedActors.IsEmpty()) {
			for (AActor* actor : attachedActors) {
				if (IsValid(actor)) {

					if (printDebug) {
					#if WITH_EDITOR
						UE_LOG(LogTemp, Warning, TEXT("Removed Interactable from list: %s"), *actor->GetActorLabel());
					#endif	
					}

					if (FoundActors.Contains(actor)) {
						FoundActors.Remove(actor);
					}
				}
			}
		}
		
		
	
		//attach remaining
		for (AActor* actor : FoundActors) {
			AInteractable* interactable = Cast<AInteractable>(actor);
			if (IsValid(interactable)) {
				if (printDebug) {
				#if WITH_EDITOR
					UE_LOG(LogTemp, Warning, TEXT("Got Interactable: %s"), *interactable->GetActorLabel());
				#endif	
				}
				//attach this interactable to InteractableManager
				interactable->setInteractableManager(this);
			}
		}
	}
}

void AInteractableManager::visualizeAttention()
{
	//only do something if any interactable is registered
	if (!interactableList.IsEmpty())
	{
		//get inverse of current value
		visualize = !visualize;
		for (AInteractable* interactable : interactableList)
		{
			if (IsValid(interactable)) {
				interactable->visualizeAttention(visualize);
			}
		}
	}
}

void AInteractableManager::initPlayerHUD()
{
	//get active pawn
	APawn* playerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (IsValid(playerPawn)) {
		//get widget component of pawn that contains hud
		auto component = playerPawn->GetComponentByClass(UWidgetComponent::StaticClass());

		if (IsValid(component)) {
			UWidgetComponent* widget = Cast<UWidgetComponent>(component);
			if (IsValid(widget)) {
				if (printDebug) {
					UE_LOG(LogTemp, Warning, TEXT("Got valid widgetcomponent"));
					UE_LOG(LogTemp, Warning, TEXT("Got widget class %s"), *widget->GetWidgetClass()->GetName());
				}
				//get the hud widget from widget component
				playerHUD = Cast<UAD_HUD>(widget->GetWidget());

				if (IsValid(playerHUD)) {
					if (printDebug) {
						UE_LOG(LogTemp, Warning, TEXT("Got valid component for UAD_HUD"));
					}
				}
				else {
					if (printDebug) {
						UE_LOG(LogTemp, Warning, TEXT("Got no valid component for UAD_HUD"));
					}
				}
			}
			else {
				if (printDebug) {
					UE_LOG(LogTemp, Warning, TEXT("Got no valid widgetcomponent"));
				}
			}

		}
	}
}

