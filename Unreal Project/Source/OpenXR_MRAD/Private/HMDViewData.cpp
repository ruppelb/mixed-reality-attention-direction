// Fill out your copyright notice in the Description page of Project Settings.


#include "HMDViewData.h"

// Sets default values for this component's properties
UHMDViewData::UHMDViewData()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UHMDViewData::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UHMDViewData::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHMDViewData::getHMDViewData()
{
	IHeadMountedDisplay::MonitorInfo info;
	if (GEngine->XRSystem->GetHMDDevice()->GetHMDMonitorInfo(info)) {
		monitorSizeX = info.WindowSizeX;
		monitorSizeY = info.WindowSizeY;
	}
	else {
		monitorSizeX = 0;
		monitorSizeY = 0;
	}
	
}

