// Fill out your copyright notice in the Description page of Project Settings.


#include "MRAD_FrameVisualizer.h"

// Sets default values
AMRAD_FrameVisualizer::AMRAD_FrameVisualizer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	frameMesh = CreateDefaultSubobject<UStaticMeshComponent>(FName(FString::Printf(TEXT("VisualizationMesh_%s"), *this->GetName())));
	RootComponent = frameMesh;

	//get visualization material
	static ConstructorHelpers::FObjectFinder<UMaterial>VisualizationMaterialAsset(TEXT("Material'/Game/Materials/FrameVisualizuation'"));
	frameMaterial = VisualizationMaterialAsset.Object;

	/*
	frameMaterial = NewObject<UMaterial>();
	frameMaterial->SetShadingModel(EMaterialShadingModel::MSM_Unlit);
	//create texture parameter node
	UMaterialExpressionTextureSampleParameter2D* frameBaseTexture = NewObject<UMaterialExpressionTextureSampleParameter2D>(frameMaterial);
	frameBaseTexture->SetParameterName(TEXT("frameBaseTexture"));
	frameBaseTexture->SetDefaultTexture();
	frameMaterial->EmissiveColor.Connect(0, colorFrameBaseTexture);
	frameMaterial->Expressions.Add(frameBaseTexture);
	//create and add texture coordinates node
	//UMaterialExpressionTextureCoordinate* TexCoords = NewObject<UMaterialExpressionTextureCoordinate>(frameMaterial);
	//frameMaterial->Expressions.Add(TexCoords);
	//frameBaseTexture->Coordinates.Expression = TexCoords;

	//TMap<FName, TArray<UMaterialExpression*> > ParameterTypeMap;
	//frameMaterial->AddExpressionParameter(frameBaseTexture, ParameterTypeMap);
	//UE_LOG(LogTemp, Warning, TEXT("Parameter name: %s"), *frameBaseTexture->GetParameterExpressionId().ToString());
	*/
}

// Called when the game starts or when spawned
void AMRAD_FrameVisualizer::BeginPlay()
{

	Super::BeginPlay();

	//create Material for displaying received data
	//create dynamic Material
	frameMaterialDynamic = UMaterialInstanceDynamic::Create(frameMaterial, this);
	if (IsValid(frameMesh->GetStaticMesh())) {
		frameMesh->SetMaterial(0, frameMaterialDynamic);
	}
	

	//find IPCDataImporter
	APawn* playerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (IsValid(playerPawn)) {
		auto component = playerPawn->GetComponentByClass(UMRAD_IPCDataImporter::StaticClass());
		if (IsValid(component)) {
			UMRAD_IPCDataImporter* importer = Cast<UMRAD_IPCDataImporter>(component);
			if (IsValid(importer)) {
				dataImporter = importer;
			}
		}
	}

	if (IsValid(frameMesh->GetStaticMesh())) {
		if (displayDepthFrame) {
			//start own thread to prevent delay in main thread
			concurrentlyGetDepthFrame = true;
			depthFrameThread = std::thread(&AMRAD_FrameVisualizer::acquireLatestDepthFrame, this);
		}
	}

	latestDepthFrame = std::make_tuple(false, 0, 0, 0, std::vector<uint8_t>());
}

void AMRAD_FrameVisualizer::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (IsValid(frameMesh->GetStaticMesh())) {
		if (displayDepthFrame) {
			//stop depth frame thread
			concurrentlyGetDepthFrame = false;
			if (depthFrameThread.joinable()) {
				depthFrameThread.join();
			}
		}
	}
}

// Called every frame
void AMRAD_FrameVisualizer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsValid(frameMesh->GetStaticMesh())) {
		if (displayColorFrame) {
			drawColorFrame();
		}
		else if (displayDepthFrame) {
			drawDepthFrame();
		}
	}
}

void AMRAD_FrameVisualizer::drawColorFrame()
{
	//only draw left image for now
	//bool valid = false;
	//int width, height;
	//size_t rowStride;
	//std::vector<uint8_t> dataBuffer;
	auto[valid,width, height, colorChannels, dataBuffer] = dataImporter->getColorFrame(displayRightChannel);

	if (valid) {
		//GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, *FString::Printf(TEXT("Reveived Size: %zu, WIdth: %i, height: %i, channels: %i"), dataBuffer.size(), width, height, colorChannels));

		//BYTE* colorFrameBuffer = new BYTE[Width * Height * 4];
		colorFrameTexture = UTexture2D::CreateTransient(width, height, EPixelFormat::PF_R8G8B8A8);

		/*
		FTexture2DMipMap* MipMap = &colorFrameTexture->PlatformData->Mips[0];
		//FUntypedBulkData* ImageData = &MipMap->BulkData;
		void* RawImageData = MipMap->BulkData.Lock(LOCK_READ_WRITE);
		FMemory::Memcpy(RawImageData, dataBuffer.data(), width * height * 4);
		MipMap->BulkData.Unlock();
		colorFrameTexture->UpdateResource();
		*/
		
		
		uint8_t* MipData = static_cast<uint8_t*>(colorFrameTexture->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE));
		FMemory::Memcpy(MipData, dataBuffer.data(), width * height * colorChannels);
		colorFrameTexture->PlatformData->Mips[0].BulkData.Unlock();
		
		colorFrameTexture->UpdateResource();

		frameMaterialDynamic->SetTextureParameterValue(FName("colorFrameBaseTexture"), colorFrameTexture);
		
		//UE_LOG(LogTemp, Warning, TEXT("Revieved frame width: %i"), width);
		//UE_LOG(LogTemp, Warning, TEXT("Second Pixel values:  R: %i  G: %i  B: %i  A: %i"), MipData[4],MipData[5],MipData[6],MipData[7]);
	}
}

void AMRAD_FrameVisualizer::drawDepthFrame()
{
	std::lock_guard<std::mutex> streamLock(depthFrameMutex);
	auto [valid, width, height, colorChannels, dataBuffer] = latestDepthFrame;
	if (valid) {
		//GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, *FString::Printf(TEXT("Reveived Size: %zu, WIdth: %i, height: %i, channels: %i"), dataBuffer.size(), width, height, colorChannels));
		depthFrameTexture = UTexture2D::CreateTransient(width, height, EPixelFormat::PF_R8G8B8A8);

		uint8_t* MipData = static_cast<uint8_t*>(depthFrameTexture->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE));
		FMemory::Memcpy(MipData, dataBuffer.data(), width * height * colorChannels);
		depthFrameTexture->PlatformData->Mips[0].BulkData.Unlock();

		depthFrameTexture->UpdateResource();

		frameMaterialDynamic->SetTextureParameterValue(FName("colorFrameBaseTexture"), depthFrameTexture);
	}
}

void AMRAD_FrameVisualizer::acquireLatestDepthFrame()
{
	while (concurrentlyGetDepthFrame) {
		if (dataImporter->IPCConnectorClientInitialized) {
			auto [valid, width, height, colorChannels, dataBuffer] = dataImporter->getDepthFrame(displayRightChannel);
			std::lock_guard<std::mutex> streamLock(depthFrameMutex);
			latestDepthFrame = std::make_tuple(valid, width, height, colorChannels, dataBuffer);
		}
	}
}

