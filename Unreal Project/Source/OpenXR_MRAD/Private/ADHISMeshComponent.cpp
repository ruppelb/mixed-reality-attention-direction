// Fill out your copyright notice in the Description page of Project Settings.

#include "ADHISMeshComponent.h"


UADHISMeshComponent::UADHISMeshComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;

	//initialize variables
	frameThickness = 1.0;
	dynamicFrameThickness = true;
}

void UADHISMeshComponent::BeginPlay()
{
	Super::BeginPlay();

	
	//reset transform, in case it is set from previous play
	ownerTransform = FTransform();

	//check if HMD is connected
	if (GEngine) {
		HMDEnabled = GEngine->XRSystem.IsValid() && GEngine->XRSystem->GetHMDDevice() && GEngine->XRSystem->GetHMDDevice()->IsHMDConnected();

		//save device id
		if (HMDEnabled) {
			deviceId = GEngine->XRSystem.Get()->HMDDeviceId;
		}
	}

	//clear bounding box list in case mesh changed in editor between two play sessions
	boundingBoxLocalCoordinates.Empty();
}

void UADHISMeshComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime,TickType,ThisTickFunction);
	//only update component, when it is visible
	if (IsVisible()) {

		auto Owner = GetOwner();
		if (IsValid(Owner))
		{
			//get current camera location
			updateCameraLocations();

			//set rotation to face this component to camera
			FRotator newRotation = UKismetMathLibrary::FindLookAtRotation(Owner->GetActorLocation(), cameraPosition);
			
			//get normal for at the rotaton position
			planeNormal = UKismetMathLibrary::GetForwardVector(newRotation);

			//adjust Rotation because of mesh orientation
			FRotator adjustedRotation = FRotator(newRotation.Pitch - 90.0, newRotation.Yaw, newRotation.Roll);

			//update rotation
			SetWorldRotation(adjustedRotation);

			//get bounding box coordinates of owner
			UStaticMeshComponent* meshComponent = Owner->FindComponentByClass<UStaticMeshComponent>();
			if (IsValid(meshComponent)) {
				//if actor transform changed or no bounding box coordinates have been calculated previously, calculate new bounding box coordinates
				if (!Owner->GetActorTransform().Equals(ownerTransform) || boundingBoxLocalCoordinates.IsEmpty()) {
					//get min and max values of mesh
					FVector min;
					FVector max;
					meshComponent->GetLocalBounds(min, max);

					boundingBoxLocalCoordinates.Empty();

					boundingBoxLocalCoordinates.Add(FVector(max.X, max.Y, max.Z));
					boundingBoxLocalCoordinates.Add(FVector(max.X, max.Y, min.Z));
					boundingBoxLocalCoordinates.Add(FVector(max.X, min.Y, max.Z));
					boundingBoxLocalCoordinates.Add(FVector(max.X, min.Y, min.Z));
					boundingBoxLocalCoordinates.Add(FVector(min.X, max.Y, max.Z));
					boundingBoxLocalCoordinates.Add(FVector(min.X, max.Y, min.Z));
					boundingBoxLocalCoordinates.Add(FVector(min.X, min.Y, max.Z));
					boundingBoxLocalCoordinates.Add(FVector(min.X, min.Y, min.Z));

					ownerTransform = Owner->GetActorTransform();
				}

				//empty arrays
				boxWorldCoordinates.Empty();
				distances.Empty();

				//get minimal distance bounding box coordinate from camera position and set it as plane origin
				for (FVector boxLocalCoordinate : boundingBoxLocalCoordinates) {
					FVector boxWorldCoordinate = ownerTransform.TransformPosition(boxLocalCoordinate);
					boxWorldCoordinates.Add(boxWorldCoordinate);
					distances.Add(UKismetMathLibrary::Vector_Distance(cameraPosition, boxWorldCoordinate));
				}
				int minIndex;
				float minValue;
				UKismetMathLibrary::MinOfFloatArray(distances, minIndex, minValue);
				planeOrigin = boxWorldCoordinates[minIndex];

				//get transform of plane in front of object
				FTransform transform = UKismetMathLibrary::MakeTransform(planeOrigin, GetComponentRotation(), GetComponentScale());

				//empty arrays
				xArray.Empty();
				yArray.Empty();

				//project bounding box coordinates on plane in front of parent object
				for (FVector boxWorldCoordinate : boxWorldCoordinates) {

					if (showDebugInfo) {
						DrawDebugPoint(GetWorld(), boxWorldCoordinate, 10.0f, FColor(0, 255, 255), false, 0.1f);
					}

					if (HMDEnabled && !eyePositions.IsEmpty()) {
						FColor color = FColor::Purple;

						//project each bounding box point for both eye positions
						for (FVector eyePosition : eyePositions) {
							//calculate intersection point with plane in front of object
							FVector intersection;
							float t;
							if (UKismetMathLibrary::LinePlaneIntersection_OriginNormal(eyePosition, boxWorldCoordinate, planeOrigin, planeNormal, t, intersection))
							{
								if (showDebugInfo) {
									DrawDebugPoint(GetWorld(), intersection, 10.0f, color, false, 0.3f);
								}
								
								//transform intersection point into a plane relative coordinate (this makes the comparison of the maximal bounding values a lot more easy, since only x and y values must be considered)
								FVector planeRelativeCoordinate = UKismetMathLibrary::InverseTransformLocation(transform, intersection);
								//save x and y coordinates ( z not relevant, because the target is a plane)
								xArray.Add(planeRelativeCoordinate.X);
								yArray.Add(planeRelativeCoordinate.Y);
							}
							color = FColor::Emerald;
						}
					}
					else {
						//calculate intersection point with plane in front of object
						FVector intersection;
						float t;
						if (UKismetMathLibrary::LinePlaneIntersection_OriginNormal(cameraPosition, boxWorldCoordinate, planeOrigin, planeNormal, t, intersection))
						{
							if (showDebugInfo) {
								DrawDebugPoint(GetWorld(), intersection, 10.0f, FColor::Purple, false, 0.3f);
							}
							//transform intersection point into a plane relative coordinate (this makes the comparison of the maximal bounding values a lot more easy, since only x and y values must be considered)
							FVector planeRelativeCoordinate = UKismetMathLibrary::InverseTransformLocation(transform, intersection);
							//save x and y coordinates ( z not relevant, because the target is a plane)
							xArray.Add(planeRelativeCoordinate.X);
							yArray.Add(planeRelativeCoordinate.Y);
						}
					}
				}

				//get maximal and minimal x and y values
				float maxX;
				int32 maxXIndex;
				UKismetMathLibrary::MaxOfFloatArray(xArray, maxXIndex, maxX);
				float minX;
				int32 minXIndex;
				UKismetMathLibrary::MinOfFloatArray(xArray, minXIndex, minX);
				float maxY;
				int32 maxYIndex;
				UKismetMathLibrary::MaxOfFloatArray(yArray, maxYIndex, maxY);
				float minY;
				int32 minYIndex;
				UKismetMathLibrary::MinOfFloatArray(yArray, minYIndex, minY);

				//adjust world location of plane, so that scale values scale the plane equal in each direction (-x,x)
				//calculate center between max and min
				FVector center = FVector(maxX, maxY, 0.0) / 2.0 + FVector(minX, minY, 0.0) / 2.0;
				//set this component to new world location
				SetWorldLocation(UKismetMathLibrary::TransformLocation(transform, center));

				//get x and y min/max offsets from new location for correct scale
				maxX -= center.X;
				minX -= center.X;
				maxY -= center.Y;
				minY -= center.Y;

				//calculate new scale
				FVector scale = FVector((maxX - minX) / 100.0, (maxY - minY) / 100.0, 1.0);

				//avoid setting scale to 0 and values smaller than 0 since this can bug the mesh
				if (scale.X > 0.0f && scale.Y > 0.0f){
					if (dynamicFrameThickness) {
						calculateDynamicFrameThickness(scale.X, scale.Y);
					}
						rescale(maxX, minX, maxY, minY, scale);
				}


				if (showDebugInfo) {
					DrawDebugPoint(GetWorld(), cameraPosition, 10.0f, FColor::Yellow, false, 0.3f);
					DrawDebugPoint(GetWorld(), planeOrigin, 10.0f, FColor::Yellow, false, 0.3f);
					DrawDebugPoint(GetWorld(), UKismetMathLibrary::TransformLocation(transform, center), 10.0f, FColor::White, false, 0.3f);
					DrawDebugLine(GetWorld(), UKismetMathLibrary::TransformLocation(transform, center), cameraPosition, FColor::White, false, 0.3f, 0U, 0.3f);
					DrawDebugLine(GetWorld(), UKismetMathLibrary::TransformLocation(transform, center), UKismetMathLibrary::TransformLocation(transform, center) + 1500 * planeNormal, FColor::Blue, false, 0.3f, 0U, 0.3f);
				}
				
			}
		}
	}
}

void UADHISMeshComponent::rescaleInEditor()
{
	//create instances on first execution
	if (!constructed) {
		CreateInstances();
	}
	auto Owner = GetOwner();
	if (IsValid(Owner) && Owner != nullptr) {
		//get owner scale
		ownerTransform.SetScale3D(Owner->GetActorScale3D());

		//update frame thickness according to scale, if enabled
		if (dynamicFrameThickness) {
			calculateDynamicFrameThickness(ownerTransform.GetScale3D().X, ownerTransform.GetScale3D().Y);
		}

		//get owner bounds
		//try getting bounds through static mesh component
		UStaticMeshComponent* meshComponent = Owner->FindComponentByClass<UStaticMeshComponent>();
		if (IsValid(meshComponent)) {
			if (IsValid(meshComponent->GetStaticMesh())) {

				FBox box = meshComponent->GetStaticMesh()->GetBoundingBox();
				FVector boxExtent = box.GetExtent();
				boxExtent = boxExtent * Owner->GetActorScale();

				//rescale with new values
				rescale(boxExtent.X, -boxExtent.X, boxExtent.Y, -boxExtent.Y, ownerTransform.GetScale3D());
			}
		}
	}
}

void UADHISMeshComponent::rescale(float MaxX, float MinX, float MaxY, float MinY, FVector scale)
{
	FTransform instanceTransform;

	//set scale and location of lower Y instance
	if (GetInstanceTransform(lowerYIndex, instanceTransform)) {
		//set location
		FVector location = instanceTransform.GetLocation();
		location.Set(location.X, MinY, location.Z);
		instanceTransform.SetLocation(location);

		//set rotation again to initial value (avoid reset of rotation when passing through plane mesh with camera)
		FRotator rotation = FRotator(0.0, 180.0, 0.0);
		instanceTransform.SetRotation(FQuat4d::MakeFromRotator(rotation));


		//set scale
		FVector newScale = FVector(scale.X, frameThickness, scale.Z);
		instanceTransform.SetScale3D(newScale);

		//apply new transform
		UpdateInstanceTransform(lowerYIndex, instanceTransform, false);
	}

	//set scale and location of upper Y instance
	if (GetInstanceTransform(upperYIndex, instanceTransform)) {
		//set location
		FVector location = instanceTransform.GetLocation();
		location.Set(location.X, MaxY, location.Z);
		instanceTransform.SetLocation(location);

		//set rotation again to initial value (avoid reset of rotation when passing through plane mesh with camera)
		FRotator rotation = FRotator(0.0, 0.0, 0.0);
		instanceTransform.SetRotation(FQuat4d::MakeFromRotator(rotation));

		//set scale
		FVector newScale = FVector(scale.X, frameThickness, scale.Z);
		instanceTransform.SetScale3D(newScale);

		//apply new transform
		UpdateInstanceTransform(upperYIndex, instanceTransform, false);
	}

	//set scale and location of lower X instance
	if (GetInstanceTransform(lowerXIndex, instanceTransform)) {
		//set location
		FVector location = instanceTransform.GetLocation();
		location.Set(MinX, location.Y, location.Z);
		instanceTransform.SetLocation(location);
		
		//set rotation again to initial value (avoid reset of rotation when passing through plane mesh with camera)
		FRotator rotation = FRotator(0.0, 90.0, 0.0);
		instanceTransform.SetRotation(FQuat4d::MakeFromRotator(rotation));

		//set scale
		FVector newScale = FVector(scale.Y, frameThickness, scale.Z);
		instanceTransform.SetScale3D(newScale);

		//apply new transform
		UpdateInstanceTransform(lowerXIndex, instanceTransform, false);
	}

	//set scale and location of upper X instance
	if (GetInstanceTransform(upperXIndex, instanceTransform)) {
		//set location
		FVector location = instanceTransform.GetLocation();
		location.Set(MaxX, location.Y, location.Z);
		instanceTransform.SetLocation(location);

		//set rotation again to initial value (avoid reset of rotation when passing through plane mesh with camera)
		FRotator rotation = FRotator(0.0, -90.0, 0.0);
		instanceTransform.SetRotation(FQuat4d::MakeFromRotator(rotation));

		//set scale
		FVector newScale = FVector(scale.Y, frameThickness, scale.Z);
		instanceTransform.SetScale3D(newScale);

		//apply new transform
		UpdateInstanceTransform(upperXIndex, instanceTransform, false);
	}
}

void UADHISMeshComponent::CreateInstances()
{
	mesh = this->GetStaticMesh();
	if (IsValid(mesh)) {
		//add lower Y instance
		FVector translation = FVector(0.0,-49.0,0.0);
		FRotator rotation = FRotator(0.0, 180.0,0.0);
		FVector scale = FVector(1.0, 1.0, 1.0);
		FTransform transform = FTransform(rotation,translation,scale);
		lowerYIndex = AddInstance(transform);

		//add upper Y instance
		translation = FVector(0.0, 49.0, 0.0);
		rotation = FRotator(0.0, 0.0, 0.0);
		scale = FVector(1.0, 1.0, 14.0);
		transform = FTransform(rotation, translation, scale);
		upperYIndex = AddInstance(transform);

		//add lower X instance
		translation = FVector(-49.0, 0.0, 0.0);
		rotation = FRotator(0.0, 90.0, 0.0);
		scale = FVector(1.0, 1.0, 1.0);
		transform = FTransform(rotation, translation, scale);
		lowerXIndex = AddInstance(transform);

		//add upper X instance
		translation = FVector(49.0, 0.0, 0.0);
		rotation = FRotator(0.0, -90.0, 0.0);
		scale = FVector(1.0, 1.0, 1.0);
		transform = FTransform(rotation, translation, scale);
		upperXIndex = AddInstance(transform);

		//disable scaling when owner is scaled, since we want to apply our own approach when owner is scaled.
		this->SetAbsolute(false, false, true);

		constructed = true;
	}
}

void UADHISMeshComponent::calculateDynamicFrameThickness(float scaleX, float scaleY)
{
	//prevent that the framethickness gets negative
	if (scaleX > 0.0 && scaleY > 0.0) {
		//the overall framethickness is always set to the minimum of x and y scale
		float min = std::min(scaleX, scaleY);
		//deployed an e function to decelerate the decrease of the frametickness at small scale values.
		float e = exp(-4.0 * min - 2.5);
		frameThickness = min + e;
	}
	else {
		frameThickness = 0.0f;
	}
	
}

void UADHISMeshComponent::updateCameraLocations()
{
	//get camera location
	if (UWorld* World = GetWorld())
	{

		//check if hmd is enabled and if application can get XRSystem (if project gets opened with hmd connected and before begin play hmd gets disconneceted, XRSystem.Get() is null)
		if (HMDEnabled && GEngine->XRSystem.Get() != nullptr) {
			//if hmd is enabled, get a camera position per eye

			//get hmd position
			FQuat rotDevice;
			FVector posDevice;
			if (GEngine->XRSystem.Get()->GetCurrentPose(deviceId, rotDevice, posDevice)) {
				//additionally save camera position
				cameraPosition = posDevice;
				//clear old values
				eyePositions.Empty();

				FQuat rot;
				FVector pos;

				//get left eye relative position
				if (GEngine->XRSystem.Get()->GetRelativeEyePose(deviceId, 0, rot, pos)) {
					//rotate relative position to get eye position in the direction the hmd is facing
					pos = rotDevice.RotateVector(pos);

					//add this position to cameraPositions
					eyePositions.Add(posDevice + pos);

					if (showDebugInfo) {
						DrawDebugPoint(GetWorld(), posDevice + pos, 10.0f, FColor::Cyan, false, 0.3f);
					}
				}

				//get right eye relative position
				if (GEngine->XRSystem.Get()->GetRelativeEyePose(deviceId, 1, rot, pos)) {
					//rotate relative position to get eye position in the direction the hmd is facing
					pos = rotDevice.RotateVector(pos);
					//add this position to cameraPositions
					eyePositions.Add(posDevice + pos);
					
					if (showDebugInfo) {
						DrawDebugPoint(GetWorld(), posDevice + pos, 10.0f, FColor::Red, false, 0.3f);
					}
					
				}
			}
		}
		else {
			// if no hmd is connected just get the first player camera Location
			APlayerCameraManager* playerCamera = World->GetFirstPlayerController()->PlayerCameraManager;
			if (playerCamera)
			{
				cameraPosition = playerCamera->GetCameraLocation();
			}
		}
	}
}

void UADHISMeshComponent::resetInstances()
{
	//clear instances
	RemoveInstance(upperXIndex);
	RemoveInstance(lowerXIndex);
	RemoveInstance(upperYIndex);
	RemoveInstance(lowerYIndex);

	//create new instances
	CreateInstances();
}



