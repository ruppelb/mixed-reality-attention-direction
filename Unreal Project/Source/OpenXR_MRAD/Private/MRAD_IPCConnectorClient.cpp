// Fill out your copyright notice in the Description page of Project Settings.


#include "MRAD_IPCConnectorClient.h"

#include <string>
#include <algorithm>
#include <vector>

MRAD_IPCConnectorClient::MRAD_IPCConnectorClient()
{
    //reset variables storing latest received data
    m_LatestVSTFrame = VSTFrame();
    m_LatestPointCloudSnapshotContent = PointCloudSnapshotContent();
    m_LatestInstrumentData = std::vector<InstrumentLocal>();
}

MRAD_IPCConnectorClient::~MRAD_IPCConnectorClient()
{
    
    //wait for threads to end

    importVSTData = false;
    //wait for thread to join, before memory segments get destroyed
    if (importVSTThread.joinable()) {
        importVSTThread.join();
    }

    importPointCloud = false;
    //wait for thread to join, before memory segments get destroyed
    if (importPointCloudThread.joinable()) {
        importPointCloudThread.join();
    }

    //wait for threads to end
    importInstrument = false;
    //wait for thread to join, before memory segments get destroyed
    if (importInstrumentThread.joinable()) {
        importInstrumentThread.join();
    }
}

void MRAD_IPCConnectorClient::importVSTFrame()
{
    try {
        //open semaphores
        //semaphore that handels that only one process at a time can access our 
        named_semaphore VSTMutexSemaphore(open_only_t(), MRMUTEXSEMAPHORE);
        //semaphore that holds the number of items that have been emptied from shared memory
        named_semaphore VSTEmptySemaphore(open_only_t(), MREMPTYSEMAPHORE);
        //semaphore that holds the number of items currently stored in the shared memory
        named_semaphore VSTStoredSemaphore(open_only_t(), MRSTOREDSEMAPHORE);
        UE_LOG(LogTemp, Display, TEXT("VST Semaphores opened"));

        //open VST Memory Segment
        managed_shared_memory VSTMemorySegment(open_only_t(), MRSHAREDMEMORY);
        UE_LOG(LogTemp, Display, TEXT("VST Shared Memory Segment opened"));

        while (importVSTData) {

            if (VSTStoredSemaphore.try_wait()) {
                if (VSTMutexSemaphore.try_wait()) {

                    //Find frame info object
                    VSTFrameInfo* info = VSTMemorySegment.find<VSTFrameInfo>("VSTFrameInfo").first;

                    //Find the array with the left eye frame data using the c-string name
                    std::pair<uint8_t*, std::size_t> VSTArray_l = VSTMemorySegment.find<uint8_t>("VSTDataArray_l");

                    //Find the array with the right eye frame data using the c-string name
                    std::pair<uint8_t*, std::size_t> VSTArray_r = VSTMemorySegment.find<uint8_t>("VSTDataArray_r");

                    //check if we found something
                    if (info != 0 && VSTArray_l.first != 0 && VSTArray_r.first != 0) {

                        //Prevent acces on m_LatestVSTFrame from different thread
                        std::lock_guard<std::mutex> streamLock(VSTFrameMutex);

                        //update current frame

                        //copy frame info
                        m_LatestVSTFrame.info = VSTFrameInfo(*info);

                        //copy left data
                        if (VSTArray_l.second > 0) {
                            //copy vector at this point, to prevent long intervals between frames due to further processing of the received data.
                            m_LatestVSTFrame.data_l = std::vector<uint8_t>(VSTArray_l.first, VSTArray_l.first + VSTArray_l.second);
                        }
                        else {
                            //keep old data?
                        }

                        //copy right data
                        if (VSTArray_r.second > 0) {
                            //copy vector at this point, to prevent long intervals between frames due to further processing of the received data.
                            m_LatestVSTFrame.data_r = std::vector<uint8_t>(VSTArray_r.first, VSTArray_r.first + VSTArray_r.second);
                        }
                        else {
                            //keep old data?
                        }
                    }

                    //When done, destroy the arrays and info from the segment
                    VSTMemorySegment.destroy<uint8_t>("VSTDataArray_l");
                    VSTMemorySegment.destroy<uint8_t>("VSTDataArray_r");

                    VSTMemorySegment.destroy<VSTFrameInfo>("VSTFrameInfo");

                    //We do not extract the info of the depth and normal array. Nevertheless, we need to destroy the memory segments or we get an error if the host tries to write to one of these.
                    VSTMemorySegment.destroy<uint32_t>("DepthDataArray_l");
                    VSTMemorySegment.destroy<uint32_t>("DepthDataArray_r");

                    VSTMemorySegment.destroy<uint8_t>("NormalDataArray_l");
                    VSTMemorySegment.destroy<uint8_t>("NormalDataArray_r");

                    VSTMutexSemaphore.post();
                    VSTEmptySemaphore.post();
                }
                else {
                    //post Stored Semaphore again, so next loop we can check for both conditions
                    VSTStoredSemaphore.post();
                }
            }
        }
    }
    catch (boost::interprocess::interprocess_exception e) {
        importVSTData = false;
        UE_LOG(LogTemp, Warning, TEXT("Error when importing VST Frame. Stopping import. Exception: %s"), e.what());
    }
}

void MRAD_IPCConnectorClient::importPointCloudData()
{
    try {
        //open semaphores
        //semaphore that handels that only one process at a time can access our 
        named_semaphore PointCloudMutexSemaphore(open_only_t(), DEPTHMUTEXSEMAPHORE);
        //semaphore that holds the number of items that have been emptied from shared memory
        named_semaphore PointCloudEmptySemaphore(open_only_t(), DEPTHEMPTYSEMAPHORE);
        //semaphore that holds the number of items currently stored in the shared memory
        named_semaphore PointCloudStoredSemaphore(open_only_t(), DEPTHSTOREDSEMAPHORE);
        UE_LOG(LogTemp, Display, TEXT("PointCloud Semaphores opened"));

        //open PointCLoud Memory Segment
        managed_shared_memory PointCloudMemorySegment(open_only_t(), DEPTHSHAREDMEMORY);
        UE_LOG(LogTemp, Display, TEXT("PointCloud Shared Memory Segment opened"));

        while (importPointCloud) {
            if (PointCloudStoredSemaphore.try_wait()) {
                if (PointCloudMutexSemaphore.try_wait()) {

                    //Find the array with the pointcloud data using the c-string name
                    std::pair<PointCloudPoint*, std::size_t> PointCloudArray = PointCloudMemorySegment.find<PointCloudPoint>("PointCloudDataArray");

                    //check if we found something
                    if (PointCloudArray.first != 0) {

                        //Prevent acces on m_LatestPointCloudSnapshotContent from different thread
                        std::lock_guard<std::mutex> streamLock(pointCloudMutex);

                        // update snapshot content

                        //copy vector at this point, to prevent long intervals between point cloud data due to further processing of the received data.
                        if (PointCloudArray.second > 0) {
                            m_LatestPointCloudSnapshotContent.points = std::vector<PointCloudPoint>(PointCloudArray.first, PointCloudArray.first + PointCloudArray.second);
                        }
                        else {
                            //keep old points?
                        }

                        m_LatestPointCloudSnapshotContent.pointCount = PointCloudArray.second;
                    }

                    //When done, destroy the vector from the segment
                    PointCloudMemorySegment.destroy<PointCloudPoint>("PointCloudDataArray");

                    PointCloudMutexSemaphore.post();
                    PointCloudEmptySemaphore.post();
                }
                else {
                    //post Stored Semaphore again, so next loop we can check for both conditions
                    PointCloudStoredSemaphore.post();
                }
            }
        }
    }
    catch (boost::interprocess::interprocess_exception e) {
        importPointCloud = false;
        UE_LOG(LogTemp, Warning, TEXT("Error when importing Point Cloud data. Stopping import. Exception: %s"), e.what());
    }
}

void MRAD_IPCConnectorClient::importInstrumentData()
{
    try {
        //open semaphores
        //semaphore that handels that only one process at a time can access our 
        named_semaphore InstrumentMutexSemaphore(open_only_t(), INSTRUMENTMUTEXSEMAPHORE);
        //semaphore that holds the number of items that have been emptied from shared memory
        named_semaphore InstrumentEmptySemaphore(open_only_t(), INSTRUMENTEMPTYSEMAPHORE);
        //semaphore that holds the number of items currently stored in the shared memory
        named_semaphore InstrumentStoredSemaphore(open_only_t(), INSTRUMENTSTOREDSEMAPHORE);
        UE_LOG(LogTemp, Display, TEXT("Instrument Semaphores opened"));

        //open instrument Memory Segment
        managed_shared_memory InstrumentMemorySegment(open_only_t(), INSTRUMENTSHAREDMEMORY);
        UE_LOG(LogTemp, Display, TEXT("Instrument Shared Memory Segment opened"));

        while (importInstrument) {
            if (InstrumentStoredSemaphore.try_wait()) {
                if (InstrumentMutexSemaphore.try_wait()) {

                    //Find the vector with the instrument data using the c-string name
                    InstrumentDataVector* InstrumentVector = InstrumentMemorySegment.find<InstrumentDataVector>("InstrumentDataVector").first;

                    //check if we found something
                    if (InstrumentVector != 0) {
                        //Prevent acces on m_LatestInstrumentData from different thread
                        std::lock_guard<std::mutex> streamLock(instrumentMutex);


                        //copy instrument data
                        if (!InstrumentVector->empty()) {
                            //clear previous to prevent overflow
                            m_LatestInstrumentData.clear();

                            //copy vector at this point, to prevent long intervals between frames due to further processing of the received data.
                            for (int i = 0; i < InstrumentVector->size(); i++) {
                                m_LatestInstrumentData.push_back(InstrumentLocal(InstrumentVector->at(i).name.c_str(), InstrumentVector->at(i).centerX, InstrumentVector->at(i).centerY, InstrumentVector->at(i).centerZ, InstrumentVector->at(i).topX, InstrumentVector->at(i).topY, InstrumentVector->at(i).topZ, InstrumentVector->at(i).leftX, InstrumentVector->at(i).leftY, InstrumentVector->at(i).leftZ, InstrumentVector->at(i).rightX, InstrumentVector->at(i).rightY, InstrumentVector->at(i).rightZ, InstrumentVector->at(i).bottomX, InstrumentVector->at(i).bottomY, InstrumentVector->at(i).bottomZ, InstrumentVector->at(i).normalX, InstrumentVector->at(i).normalY, InstrumentVector->at(i).normalZ, InstrumentVector->at(i).camPosX, InstrumentVector->at(i).camPosY, InstrumentVector->at(i).camPosZ));

                            }
                        }
                        else {
                            //keep old data?
                        }

                        //When done, destroy the vector from the segment
                        InstrumentMemorySegment.destroy<InstrumentDataVector>("InstrumentDataVector");
                    }

                    InstrumentMutexSemaphore.post();
                    InstrumentEmptySemaphore.post();
                }
                else {
                    //post Stored Semaphore again, so next loop we can check for both conditions
                    InstrumentStoredSemaphore.post();
                }
            }
        }
    }
    catch (boost::interprocess::interprocess_exception e) {
        importInstrument = false;
        UE_LOG(LogTemp, Warning, TEXT("Error when importing instrument data. Stopping import. Exception: %s"), e.what());
    }
}


void MRAD_IPCConnectorClient::SetVSTFrameImport(bool status)
{
    if (status && !importVSTData) {
        //start importing vst data

        //set vst import bool
        importVSTData = status;

        //start VST import thread
        importVSTThread = std::thread(&MRAD_IPCConnectorClient::importVSTFrame, this);
    }
    else if (!status && importVSTData) {
        //stop importing vst data

        //set vst import bool
        importVSTData = status;

        //join thread if joinable
        if (importVSTThread.joinable()) {
            importVSTThread.join();
        }
    }
}

void MRAD_IPCConnectorClient::SetPointCloudDataImport(bool status)
{
    if (status && !importPointCloud) {
        //start importing pointcloud data


        //set pointcloud import bool
        importPointCloud = status;

        //start pointcloud import thread
        importPointCloudThread = std::thread(&MRAD_IPCConnectorClient::importPointCloudData, this);        
    }
    else if (!status && importPointCloud) {
        //stop importing pointcloud data

        //set pointcloud import bool
        importPointCloud = status;

        //join thread if joinable
        if (importPointCloudThread.joinable()) {
            importPointCloudThread.join();
        }
    }
}

void MRAD_IPCConnectorClient::SetInstrumentDataImport(bool status)
{
    if (status && !importInstrument) {
        //start importing instrument data

        //set instrument import bool
        importInstrument = status;

        //start instrument import thread
        importInstrumentThread = std::thread(&MRAD_IPCConnectorClient::importInstrumentData, this);
    }
    else if (!status && importInstrument) {
        //stop importing instrument data

        //set instrument import bool
        importInstrument = status;

        //join thread if joinable
        if (importInstrumentThread.joinable()) {
            importInstrumentThread.join();
        }
    }
}


bool MRAD_IPCConnectorClient::getColorFrame(int ch, std::pair<int, int>& resolution, int& channels,std::vector<uint8_t>& data, double& principalPointX, double& principalPointY, double& focalLengthX, double& focalLengthY, double* extrinsics, double* HMDPose)
{
    //return when not importing
    if (!importVSTData) {
        return false;
    }

    //Prevent access on m_LatestVSTFrame from different thread
    std::lock_guard<std::mutex> VSTLock(VSTFrameMutex);

    //return when data is empty
    if (m_LatestVSTFrame.data_l.empty() || m_LatestVSTFrame.data_r.empty()) {
        return false;
    }

    if (ch == 0) {
        //return left image
        resolution = std::pair<int, int>(m_LatestVSTFrame.info.width_l, m_LatestVSTFrame.info.height_l);
        channels = m_LatestVSTFrame.info.channels_l;
        data = m_LatestVSTFrame.data_l;
        principalPointX = m_LatestVSTFrame.info.intrinsics_l.principalPointX;
        principalPointY = m_LatestVSTFrame.info.intrinsics_l.principalPointY;
        focalLengthX = m_LatestVSTFrame.info.intrinsics_l.focalLengthX;
        focalLengthY = m_LatestVSTFrame.info.intrinsics_l.focalLengthY;
        std::copy(m_LatestVSTFrame.info.extrinsics_l, m_LatestVSTFrame.info.extrinsics_l + 16, extrinsics);
        std::copy(m_LatestVSTFrame.info.HMDPose_l, m_LatestVSTFrame.info.HMDPose_l + 16, HMDPose);
        return true;
    }
    else if(ch == 1) {
        //return right image
        resolution = std::pair<int, int>(m_LatestVSTFrame.info.width_r, m_LatestVSTFrame.info.height_r);
        channels = m_LatestVSTFrame.info.channels_r;
        data = m_LatestVSTFrame.data_r;
        principalPointX = m_LatestVSTFrame.info.intrinsics_r.principalPointX;
        principalPointY = m_LatestVSTFrame.info.intrinsics_r.principalPointY;
        focalLengthX = m_LatestVSTFrame.info.intrinsics_r.focalLengthX;
        focalLengthY = m_LatestVSTFrame.info.intrinsics_r.focalLengthY;
        std::copy(m_LatestVSTFrame.info.extrinsics_r, m_LatestVSTFrame.info.extrinsics_r + 16, extrinsics);
        std::copy(m_LatestVSTFrame.info.HMDPose_r, m_LatestVSTFrame.info.HMDPose_r + 16, HMDPose);
        return true;
    }
    return false;
}

bool MRAD_IPCConnectorClient::getPointCloudPoints(TArray<FVector3f>& normals, TArray<FColor>& colors, TArray<FVector3f>& positions, TArray<float>& radii, TArray<uint32_t>& ids, TArray<uint32_t>& confidences)
{
    //return when not importing
    if (!importPointCloud) {
        return false;
    }


    PointCloudSnapshotContent content;
    {
        //Prevent acces on m_LatestPointCloudSnapshotContent from different thread
        std::lock_guard<std::mutex> PointCloudLock(pointCloudMutex);

        //return when data is empty
        if (m_LatestPointCloudSnapshotContent.points.empty()) {
            return false;
        }

        content = m_LatestPointCloudSnapshotContent;
    }

    //empty arrays
    normals.Empty();
    colors.Empty();
    positions.Empty();
    radii.Empty();
    ids.Empty();
    confidences.Empty();
    
    for (PointCloudPoint p : content.points) {
        //unpack point data
        FVector2f normalXY = unpackHalf2x16(p.normalXY);
        FVector2f normalZcolorR = unpackHalf2x16(p.normalZcolorR);
        FVector2f colorGB = unpackHalf2x16(p.colorBG);
        FVector2f positionXY = unpackHalf2x16(p.positionXY);
        FVector2f positionZradius = unpackHalf2x16(p.positionZradius);
        
        uint32_t confidence = (p.indexConfidence & 0xFF);
        uint32_t id = (p.indexConfidence >> 8);

        //save normal
        normals.Add(FVector3f(normalXY.X, normalXY.Y, normalZcolorR.X));
        //save color
        colors.Add(FColor(normalZcolorR.Y, colorGB.X, colorGB.Y));
        //save position
        positions.Add(FVector3f(positionXY.X, positionXY.Y, positionZradius.X));
        //save radius
        radii.Add(positionZradius.Y);
        //save id
        ids.Add(id);
        //save confidence
        confidences.Add(confidence);
    }

    return true;
}

bool MRAD_IPCConnectorClient::getInstrumentData(TArray<FString>& names, TArray<FVector>& centers, TArray<FVector>& tops, TArray<FVector>& lefts, TArray<FVector>& rights, TArray<FVector>& bottoms, TArray<FVector>& normals, TArray<FVector>& camPositions)
{
    //return when not importing
    if (!importInstrument) {
        return false;
    }

    //Prevent acces on m_LatestInstrumentData from different thread
    std::lock_guard<std::mutex> streamLock(instrumentMutex);

    //return when data is empty
    if (m_LatestInstrumentData.empty()) {
        return false;
    }

    //empty arrays
    names.Empty();
    centers.Empty();
    tops.Empty();
    lefts.Empty();
    rights.Empty();
    bottoms.Empty();
    normals.Empty();
    camPositions.Empty();


    for (InstrumentLocal instrument : m_LatestInstrumentData) {
        //save name
        names.Add(FString(instrument.name));
        //save center coord
        centers.Add(instrument.center);
        //save top direction
        tops.Add(instrument.top);
        //save left direction
        lefts.Add(instrument.left);
        //save right direction
        rights.Add(instrument.right);
        //save bottom direction
        bottoms.Add(instrument.bottom);
        //save normal
        normals.Add(instrument.normal);
        //save camera position
        camPositions.Add(instrument.camPos);
    }

    //clear instrument array to only return new instruments next time
    m_LatestInstrumentData.clear();

    return true;
}

///////////////////////////////////////////////////////////
//The code below is extracted from the glm library to unpack unit32 to two floats (func_packing.hpp, type_half.hpp).
///////////////////////////////////////////////////////////

FVector2f MRAD_IPCConnectorClient::unpackHalf2x16(unsigned int v)
{
    union
    {
        unsigned int in;
        int16  out[2];
    } u;

    u.in = v;

    return FVector2f(
        toFloat32(u.out[0]),
        toFloat32(u.out[1]));
}

float MRAD_IPCConnectorClient::toFloat32(short value)
{
    int s = (value >> 15) & 0x00000001;
    int e = (value >> 10) & 0x0000001f;
    int m = value & 0x000003ff;

    if (e == 0)
    {
        if (m == 0)
        {
            //
            // Plus or minus zero
            //

            uif32 result;
            result.i = static_cast<unsigned int>(s << 31);
            return result.f;
        }
        else
        {
            //
            // Denormalized number -- renormalize it
            //

            while (!(m & 0x00000400))
            {
                m <<= 1;
                e -= 1;
            }

            e += 1;
            m &= ~0x00000400;
        }
    }
    else if (e == 31)
    {
        if (m == 0)
        {
            //
            // Positive or negative infinity
            //

            uif32 result;
            result.i = static_cast<unsigned int>((s << 31) | 0x7f800000);
            return result.f;
        }
        else
        {
            //
            // Nan -- preserve sign and significand bits
            //

            uif32 result;
            result.i = static_cast<unsigned int>((s << 31) | 0x7f800000 | (m << 13));
            return result.f;
        }
    }

    //
    // Normalized number
    //

    e = e + (127 - 15);
    m = m << 13;

    //
    // Assemble s, e and m.
    //

    uif32 Result;
    Result.i = static_cast<unsigned int>((s << 31) | (e << 23) | m);
    return Result.f;
}



