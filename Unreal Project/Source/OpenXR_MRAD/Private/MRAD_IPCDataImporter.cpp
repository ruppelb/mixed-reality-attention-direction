// Fill out your copyright notice in the Description page of Project Settings.


#include "MRAD_IPCDataImporter.h"

// Sets default values for this component's properties
UMRAD_IPCDataImporter::UMRAD_IPCDataImporter()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	FPlatformProcess::CloseProc(MRAndDepthExportHandle);
    FPlatformProcess::CloseProc(ObjectDetectionHandle);

    //get blueprint which is used for instruments

    const ConstructorHelpers::FObjectFinder<UClass> InstrumentInteractableBlueprint(TEXT("Class'/Game/Interactables/Instrument.Instrument_C'"));
    if (InstrumentInteractableBlueprint.Object) {
        InstrumentInteractable = InstrumentInteractableBlueprint.Object;
    }
    else {
        InstrumentInteractable = AInteractable::StaticClass();
    }
}


// Called when the game starts
void UMRAD_IPCDataImporter::BeginPlay()
{
	Super::BeginPlay();

    //delay input setup, so owner can properly set up his input first
    FTimerHandle TimerHandle;
    GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &UMRAD_IPCDataImporter::setupInput, 2.f, false);

    //init transform matrix from Varjo right handed coordinate system to unreal left handed system

    TransformVarjoToUnreal = FMatrix(FPlane(0, 0, -1, 0), FPlane(1, 0, 0, 0), FPlane(0, 1, 0, 0), FPlane(0, 0, 0, 1));
    TransformVarjoToUnreal = TransformVarjoToUnreal.GetTransposed();
}

void UMRAD_IPCDataImporter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	m_IPCConnectorClient.reset();
	IPCConnectorClientInitialized = false;

    //End Thread
    processPipes = false;
    if (processPipesThread.joinable()) {
        processPipesThread.join();
    }

	//End process
    FPlatformProcess::TerminateProc(ObjectDetectionHandle);
	FPlatformProcess::TerminateProc(MRAndDepthExportHandle);
	//Clear process handle
    FPlatformProcess::CloseProc(ObjectDetectionHandle);
	FPlatformProcess::CloseProc(MRAndDepthExportHandle);
}


// Called every frame
void UMRAD_IPCDataImporter::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (IPCConnectorClientInitialized)  {
        manageNewInstruments();
	}
}

void UMRAD_IPCDataImporter::setupInput()
{

    UInputComponent* c = GetOwner()->InputComponent;
    if (c != nullptr) {
        c->BindAction("AutomaticInteractablePlacementToggle", IE_Released, this, &UMRAD_IPCDataImporter::manageInput);
        //UE_LOG(LogTemp, Warning, TEXT("Sucessfully set up input"));
    }

}

void UMRAD_IPCDataImporter::manageInput()
{
    if (canReceiveInput) {
        //only do something if no input event is currently managed

        if (automaticallyPlaceInteractables) {
            GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, "Ended automatic placement");
            //UE_LOG(LogTemp, Warning, TEXT("Disabled import"));
            //previously enabled. Now stop import processes
            canReceiveInput = false;

            m_IPCConnectorClient.reset();
            IPCConnectorClientInitialized = false;

            //End Thread
            processPipes = false;
            if (processPipesThread.joinable()) {
                processPipesThread.join();
            }

            //End process
            FPlatformProcess::TerminateProc(ObjectDetectionHandle);
            FPlatformProcess::TerminateProc(MRAndDepthExportHandle);
            //Clear process handle
            FPlatformProcess::CloseProc(ObjectDetectionHandle);
            FPlatformProcess::CloseProc(MRAndDepthExportHandle);

            canReceiveInput = true;
            automaticallyPlaceInteractables = false;
        }
        else {
            //UE_LOG(LogTemp, Warning, TEXT("Enabled import"));
            GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, "Started automatic placement");
            //previously disabled. Now start import processes
            canReceiveInput = false;

            //wait for the unreal project to initialize all openxr related stuff, then start export process
            FTimerHandle TimerHandle;
            GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &UMRAD_IPCDataImporter::startExportProcess, 5, false);

            automaticallyPlaceInteractables = true;
        }
    }
    
}

void UMRAD_IPCDataImporter::initIPCConnectorClient()
{
    m_IPCConnectorClient = std::make_unique<MRAD_IPCConnectorClient>();
    //m_IPCConnectorClient.get()->SetVSTFrameImport(true);
    //m_IPCConnectorClient.get()->SetPointCloudDataImport(true);
    m_IPCConnectorClient->SetInstrumentDataImport(true);

    IPCConnectorClientInitialized = true;

    //enable ability to toggle automatic placement of interactables again
    canReceiveInput = true;
}

void UMRAD_IPCDataImporter::startExportProcess()
{
    //set up IPC Connection
    const FString& RelPath = "../../varjo-sdk-experimental/build/bin/MRAndDepthExport.exe";//"../additionalFiles/MRAndDepthExport.exe";
    const FString& BasePath = FPaths::GetProjectFilePath();//FPaths::RootDir();
    const FString& FullPath = FPaths::ConvertRelativePathToFull(BasePath, RelPath);

    MRAndDepthExportHandle = FPlatformProcess::CreateProc(*FullPath, nullptr, true, true, false, nullptr, 0, nullptr, nullptr);

    //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, MRAndDepthExportHandle.IsValid()? TEXT("VALID") : TEXT("INVALID"));
    //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, *FullPath);

    //wait for the varjo sdk process IPCConnectorHost to set up shared memory and then start object detection
    FTimerHandle TimerHandle;
    GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &UMRAD_IPCDataImporter::startObjectDetection, 5, false);
}

void UMRAD_IPCDataImporter::startObjectDetection()
{
    //set up IPC Connection
    const FString& RelPath = "../../varjo-sdk-experimental/build/bin/ObjectDetection.exe";//"../additionalFiles/ObjectDetection.exe";
    const FString& BasePath = FPaths::GetProjectFilePath();//FPaths::RootDir();
    const FString& FullPath = FPaths::ConvertRelativePathToFull(BasePath, RelPath);

    const FString& WorkingDir = "../../varjo-sdk-experimental/build/bin/";//"../additionalFiles/";
    const FString& WorkingDirFull = FPaths::ConvertRelativePathToFull(BasePath, WorkingDir);

    //creates pipes for reading output from object detection
    FPlatformProcess::CreatePipe(ReadPipe, WritePipe);

    ObjectDetectionHandle = FPlatformProcess::CreateProc(*FullPath, nullptr, true, false, false, nullptr, 0, *WorkingDirFull, WritePipe);

    //start thread for output processing
    processPipes = true;
    processPipesThread = std::thread(&UMRAD_IPCDataImporter::processPipeOutput, this);

    //wait for the object detection process to set up shared memory and then init client
    FTimerHandle TimerHandle;
    GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &UMRAD_IPCDataImporter::initIPCConnectorClient, 10, false);
}

void UMRAD_IPCDataImporter::processPipeOutput()
{
    while (processPipes) {
        if (ObjectDetectionHandle.IsValid()) {
            if (ReadPipe) {
                FString output = FPlatformProcess::ReadPipe(ReadPipe);

                TArray<FString> LogLines;

                output.ParseIntoArray(LogLines, TEXT("\n"), false);

                for (int32 LogIndex = 0; LogIndex < LogLines.Num(); ++LogIndex)
                {
                    UE_LOG(LogTemp, Warning, TEXT("ObjectDetection Out: %s"), *LogLines[LogIndex]);
                    //GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, *LogLines[LogIndex]);
                }
            }
        }
    }
}

std::tuple<bool, int, int, int, std::vector<uint8_t>> UMRAD_IPCDataImporter::getColorFrame(int channel)
{
    if (IPCConnectorClientInitialized) {
        std::pair<int, int> resolution;
        std::vector<uint8_t> dataBuffer;
        int colorChannels;
        double principalPointX;
        double principalPointY;
        double focalLengthX;
        double focalLengthY;
        double extrinsics[16];
        double HMDPose[16];
        if (m_IPCConnectorClient.get()->getColorFrame(channel, resolution, colorChannels, dataBuffer, principalPointX, principalPointY, focalLengthX, focalLengthY, &extrinsics[0], HMDPose)) {
            return std::make_tuple(true, resolution.first, resolution.second, colorChannels, dataBuffer);
        }
    }
    return std::make_tuple(false, 0, 0, 0, std::vector<uint8_t>());
}

void UMRAD_IPCDataImporter::getPointCloudPoints(bool& valid, TArray<FVector3f>& normals, TArray<FColor>& colors, TArray<FVector3f>& positions, TArray<float>& radii)
{
	if (IPCConnectorClientInitialized) {
        //do not export ids and confidence since this is not necessary for now and if called from a Blueprint, uint32 values are not supported

        TArray<uint32_t> ids;
        TArray<uint32_t> confidence;
		if (m_IPCConnectorClient.get()->getPointCloudPoints(normals, colors, positions, radii,ids,confidence)) {
			valid = true;
		}
		else {
			normals = TArray<FVector3f>();
			colors = TArray<FColor>();
			positions = TArray<FVector3f>();
			radii = TArray<float>();
			valid = false;
		}
	}
	else {
		normals = TArray<FVector3f>();
		colors = TArray<FColor>();
		positions = TArray<FVector3f>();
		radii = TArray<float>();
		valid = false;
	}
}

/**
 * A 3D point with an id and a surface normal.
 */
struct IdentifiedPoint3D {
    uint32_t id;
    FVector point3D;

public:
    IdentifiedPoint3D(uint32_t idIn, FVector pointIn)
        :id(idIn), point3D(pointIn)
    {}
};

/**
 * An id of a pointcloud point with a distance from the camera.
 */
struct IdAndDepth {
    uint32_t id;
    float dist;

public:
    IdAndDepth(uint32_t idIn, float distIn)
        :id(idIn), dist(distIn)
    {}
    IdAndDepth()
        :id(std::numeric_limits<uint32_t>::max()), dist(0.0)
    {
        //setting id default to maximal uint32 value, because the maximal used id from varjo struct is uint32 >> 8 and so this value can't be reached when the id is valid
    }
};

std::tuple<bool, int, int, int, std::vector<uint8_t>> UMRAD_IPCDataImporter::getDepthFrame(int channel)
{
    if (IPCConnectorClientInitialized) {
        std::pair<int, int> resolution;
        std::vector<uint8_t> dataBuffer;
        int colorChannels;
        double principalPointX;
        double principalPointY;
        double focalLengthX;
        double focalLengthY;
        double extrinsics[16];
        double HMDPose[16];
        if (m_IPCConnectorClient.get()->getColorFrame(channel, resolution, colorChannels, dataBuffer, principalPointX, principalPointY, focalLengthX, focalLengthY, extrinsics,HMDPose)) {
            float maxDist = std::numeric_limits<float>::min();
            float minDist = std::numeric_limits<float>::max();

            //build extrinsic matrix
            FPlane firstRow = FPlane(extrinsics[0], extrinsics[4], extrinsics[8], extrinsics[12]);
            FPlane secondRow = FPlane(extrinsics[1], extrinsics[5], extrinsics[9], extrinsics[13]);
            FPlane thirdRow= FPlane(extrinsics[2], extrinsics[6], extrinsics[10], extrinsics[14]);
            FPlane fourthRow = FPlane(extrinsics[3], extrinsics[7], extrinsics[11], extrinsics[15]);
            FMatrix extrinsicsMat = FMatrix(firstRow,secondRow,thirdRow,fourthRow);

            extrinsicsMat = extrinsicsMat.GetTransposed();

            //build intrinsic matrix
            FPlane firstRowInt = FPlane(focalLengthX,0.0, 0, 0.0);
            FPlane secondRowInt = FPlane(0.0, focalLengthY, 0, 0.0);
            FPlane thirdRowInt = FPlane(0.0, 0.0, 1.0, 0.0);
            FPlane fourthRowInt = FPlane(0.0, 0.0, 0.0, 1.0);
            FMatrix intrinsicsMat = FMatrix(firstRowInt, secondRowInt, thirdRowInt, fourthRowInt);

            intrinsicsMat = intrinsicsMat.GetTransposed();

            //get PointCloud Coordinates
            TArray<uint32_t> ids;
            TArray<uint32_t> confidences;
            TArray<FVector3f> positions;
            TArray<FColor> colors;
            TArray<FVector3f> normals;
            TArray<float> radii;
            if (m_IPCConnectorClient.get()->getPointCloudPoints(normals, colors, positions, radii, ids, confidences)) {

                //get camera center point direction (in varjo camera space negative z goes forward, so in this case it would just be (0,0,-1))
                FVector center = FVector(0, 0, -1.0);

                std::vector<IdentifiedPoint3D> pointCameraCoordinates;

                for (int i = 0; i < positions.Num(); i++) {
                  
                    FVector4 vec4P(positions[i].X, positions[i].Y, positions[i].Z, 1);
                    
                    //convert to camera coordinates
                    FVector4 cameraP = extrinsicsMat.TransformFVector4(vec4P);
                    FVector vec3P = FVector(cameraP.X / cameraP.W, cameraP.Y / cameraP.W, cameraP.Z / cameraP.W);

                    //compute dot product with camera vector to sort out points behind the camera/that can't be in view
                    float d = UKismetMathLibrary::Dot_VectorVector(center, vec3P);

                    if (d >= 0) {
                        //keep point
                        pointCameraCoordinates.push_back(IdentifiedPoint3D(ids[i], vec3P));
                    }
                }

                //project the rest of the points onto the camera plane and check if resulting u v values are within the width and height specified by the image resultion
                //set value of resulting pixel to distance of the point from camera (clamped between minimal and maximal depth testing range)
                //if multiple values result in the same pixel, then keep value of closest point (~like a z buffer)

                //structure to save per pixel depth values
                std::vector<std::vector<IdAndDepth>> pixelToDepth(resolution.second, std::vector <IdAndDepth>(resolution.first));

                for (IdentifiedPoint3D p : pointCameraCoordinates) {

                    //project 3D point coordinates on the camera plane
                    FVector4 projP = intrinsicsMat.TransformFVector4(FVector4(p.point3D,1));
                    FVector projP3D = FVector(projP);

                    //use -z coordinate because coordinate system has negative z direction as forward axis
                    FVector2d projP2D = FVector2d(projP3D.X / -projP3D.Z, projP3D.Y / -projP3D.Z);

                    //calculate corresponding pixel coordinate
                    FIntVector2 pixel;
                    pixel.X = FMath::Floor(((projP2D.X + 1.0) * 0.5 * resolution.first) - 0.5);
                    pixel.Y = FMath::Floor(((1.0 - projP2D.Y) * 0.5 * resolution.second) - 0.5);

                    if (pixel.X >= 0 && pixel.X < resolution.first && pixel.Y >= 0 && pixel.Y < resolution.second) {
                        //if point is within our defined width and height, keep value

                        //compute distance to camera (in camera coordinate system the camera is in the origin)
                        float dist = UKismetMathLibrary::Vector_Distance(FVector(0, 0, 0), p.point3D);
                            
                        //save overall min and max distances for clamping the range between 0.0 and 1.0 later
                        if (dist > maxDist) {
                            maxDist = dist;
                        }

                        if (dist < minDist) {
                            minDist = dist;
                        }

                        //save distance value in array at pixel location
                        if (pixelToDepth[pixel.Y][pixel.X].id != std::numeric_limits<uint32_t>::max()) {
                            //there is already a point projected to this pixel, select closest of both
                            float currentDist = pixelToDepth[pixel.Y][pixel.X].dist;

                            if (dist < currentDist) {
                                //save new depth value
                                pixelToDepth[pixel.Y][pixel.X] = IdAndDepth(p.id, dist);
                            }
                            else {
                                //keep old depth and id
                            }
                        }
                        else {
                            //save id to pixel
                            pixelToDepth[pixel.Y][pixel.X] = IdAndDepth(p.id, dist);
                        }
                    }
                }

                std::vector<uint8_t> data;

                //transform distance values to depth values between 0.0 and 1.0 and save depth image data
                for (std::vector<IdAndDepth> vec : pixelToDepth) {
                    for (IdAndDepth pixel : vec) {
                        if (pixel.id != std::numeric_limits<uint32_t>::max()) {
                            //pixel value was set in the procedure above. At least one pointcloud point could be projected to the pixe
                            
                            //calculate depth value between 0.0 an 1.0
                            float color = (pixel.dist - minDist) / (maxDist - minDist);

                            //invert color
                            color = 1.0 - color;

                            if (color <= 1.0 && color >= 0.0) {
                                //convert to int32_t
                                uint8_t convertedColor;
                                if (color >= 1.0) {
                                    convertedColor = std::numeric_limits<uint8_t>::max();
                                }
                                else {
                                    convertedColor = floor(color * 256);
                                }

                                data.push_back(convertedColor);
                                data.push_back(convertedColor);
                                data.push_back(convertedColor);
                                data.push_back(std::numeric_limits<uint8_t>::max());
                            }
                            else {
                                //invalid value. set pixel to black
                                data.push_back(0);
                                data.push_back(255);
                                data.push_back(0);
                                data.push_back(std::numeric_limits<uint8_t>::max());
                            }
                        }
                        else {
                            //no pixel value was set above. No pointcloud point could be projected to the pixel. Set color to black/0
                            data.push_back(255);
                            data.push_back(0);
                            data.push_back(0);
                            data.push_back(std::numeric_limits<uint8_t>::max());
                        }
                    }
                }
                return std::make_tuple(true, resolution.first, resolution.second, 4, data);
            }
            else {
            GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, *FString::Printf(TEXT("Invalid pointcloud data")));
            }
        }
    }
    return std::make_tuple(false, 0, 0, 0, std::vector<uint8_t>());
}

void UMRAD_IPCDataImporter::manageNewInstruments()
{
    if (IPCConnectorClientInitialized) {
        TArray<FString> names;
        TArray<FVector> centers;
        TArray<FVector> tops;
        TArray<FVector> lefts;
        TArray<FVector> rights;
        TArray<FVector> bottoms;
        TArray<FVector> normals;
        TArray<FVector> camPositions;
        if (m_IPCConnectorClient->getInstrumentData(names, centers, tops, lefts, rights, bottoms, normals, camPositions)) {

            UE_LOG(LogTemp, Display, TEXT("Got instruments: %i"), names.Num());
            for (int i = 0; i < names.Num(); i++) {

                //convert coordinates from varjos right handed coordinate system in unreals left handed. Also convert from M to cm
                FVector centerT = TransformVarjoToUnreal.TransformFVector4(FVector4(centers[i], 1))*100;
                FVector camPosT = TransformVarjoToUnreal.TransformFVector4(FVector4(camPositions[i], 1)) * 100;
                //dont scale the following because they are direction vectors
                FVector topT = TransformVarjoToUnreal.TransformFVector4(FVector4(tops[i], 1)); //* 100;
                FVector leftT = TransformVarjoToUnreal.TransformFVector4(FVector4(lefts[i], 1)); //* 100;
                FVector rightT = TransformVarjoToUnreal.TransformFVector4(FVector4(rights[i], 1)); //* 100;
                FVector bottomT = TransformVarjoToUnreal.TransformFVector4(FVector4(bottoms[i], 1));// *100;
                FVector normalT = TransformVarjoToUnreal.TransformFVector4(FVector4(normals[i], 1));// *100;

                //update position, rotation and scale or create new interactable
                AInteractable** ptr = mappedInstruments.Find(names[i]);

                if (ptr != nullptr) {
                    //instrument already mapped -> update position
                    (*ptr)->SetActorLocation(centerT);
                }
                else {
                    //instrument is not mapped -> create interactable

                    FActorSpawnParameters param = FActorSpawnParameters();
                    param.Name = FName(*names[i]);
                    AInteractable* newInteractable = GetWorld()->SpawnActor<AInteractable>(InstrumentInteractable, centerT, FRotator(0, 0, 0), param);
                    if (IsValid(newInteractable)) {
                        ptr = &newInteractable;
                        //newInteractable->SetActorLabel(names[0]);
                        //map new instrument
                        mappedInstruments.Add(names[i], newInteractable);
                    }
                }

                if (ptr != nullptr && *ptr != nullptr) {
                    //update rotation
                    FVector upVector = (*ptr)->GetActorUpVector();

                    //Get quaternion representing the rotation from up vector to the new plane normal. We make use of findBetweenNormals here, because the rotation axis does not matter.
                    FQuat newQuat = FQuat::FindBetweenNormals(upVector, normalT);

                    //get current quat
                    FQuat currentQuat = (*ptr)->GetActorQuat();

                    //compute new quat from both multiplied
                    FQuat resultQuat = newQuat * currentQuat;

                    //set new rotation
                    (*ptr)->SetActorRotation(resultQuat.Rotator());

                    //project directions to plane defined with normal and center point, to get width and height
                    FVector projTop = computeIntersectionOnPlane(centerT, normalT, camPosT, topT);
                    FVector projLeft = computeIntersectionOnPlane(centerT, normalT, camPosT, leftT);
                    FVector projRight = computeIntersectionOnPlane(centerT, normalT, camPosT, rightT);
                    FVector projBottom = computeIntersectionOnPlane(centerT, normalT, camPosT, bottomT);

                    DrawDebugPoint(GetWorld(), centerT, 10.0f, FColor::White, false, 0.2f);
                    DrawDebugPoint(GetWorld(), projTop, 10.0f, FColor::Blue, false, 0.2f);
                    DrawDebugPoint(GetWorld(), projLeft, 10.0f, FColor::Red, false, 0.2f);
                    DrawDebugPoint(GetWorld(), projRight, 10.0f, FColor::Green, false, 0.2f);
                    DrawDebugPoint(GetWorld(), projBottom, 10.0f, FColor::Magenta, false, 0.2f);
                    DrawDebugPoint(GetWorld(), camPosT, 10.0f, FColor::Orange, false, 0.2f);

                    //calculate scale
                    float width = UKismetMathLibrary::Vector_Distance(projLeft, projRight) / 100;
                    float height = UKismetMathLibrary::Vector_Distance(projTop, projBottom) / 100;

                    //Here, we adapt the rotation so that we can correctly apply the height scale in the y axis of the interactable.
                    
                    //get local y axis
                    FVector rightVector = (*ptr)->GetActorRightVector();

                    //get height axis of instrument
                    FVector heightAxis = projTop - projBottom;
                    UKismetMathLibrary::Vector_Normalize(heightAxis);

                    //project the new height axis on the interactable plane in case it the axis is not in the plane.
                    FVector newHeightAxis = UKismetMathLibrary::ProjectVectorOnToPlane(heightAxis, normalT);
                    newHeightAxis.Normalize();

                    //Get angle between interactable height axis and projected height axis of the instrument
                    float dotHeight = UKismetMathLibrary::Dot_VectorVector(heightAxis, rightVector);
                    float rotationAngle = acosf(dotHeight);

                    //calculate axis of rotation.
                    FVector rotationAxis = FVector::CrossProduct(rightVector, newHeightAxis);
                    rotationAxis.Normalize();

                    //Create quaternion representing rotation around axis with the angle.
                    FQuat adjustmentQuat;
                    //here we need to consider, that the rotation axis is 0 when both vectors are facing the opposite or the same direction. In this case we just use the plane normal as axis.
                    if (rotationAxis.Equals(FVector(0, 0, 0))) {
                        adjustmentQuat = FQuat(normalT, rotationAngle);
                    }
                    else {
                        adjustmentQuat = FQuat(rotationAxis, rotationAngle);
                    }

                    //update current quat
                    currentQuat = (*ptr)->GetActorQuat();

                    //calculate new quad
                    resultQuat = adjustmentQuat * currentQuat;

                    //set adjusted rotation
                    (*ptr)->SetActorRotation(resultQuat.Rotator());

                    //Now that the rotation is adjusted, we can apply the height scale for the y axis.
                    (*ptr)->SetActorScale3D(FVector(width, height, 1.0));
                }
            }
        }
    }
    
}

FVector UMRAD_IPCDataImporter::computeIntersectionOnPlane(FVector planeCenter, FVector planeNormal, FVector start, FVector direction)
{
    float depth = UKismetMathLibrary::Vector_Distance(start, planeCenter);
    FVector lineEnd = start + 10 * depth * direction;

    float t;
    FVector intersect;
    //calculate width and height
    UKismetMathLibrary::LinePlaneIntersection_OriginNormal(start, lineEnd, planeCenter, planeNormal, t, intersect);

    return intersect;
}
