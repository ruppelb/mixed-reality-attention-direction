// Fill out your copyright notice in the Description page of Project Settings.

using System.IO;
using UnrealBuildTool;

public class OpenXR_MRAD : ModuleRules
{
	
	private string ThirdPartyPath
    {
        get { return Path.GetFullPath( Path.Combine( ModuleDirectory, "../../ThirdParty/" ) ); }
    }
	
	public OpenXR_MRAD(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay"});

		PrivateDependencyModuleNames.AddRange(new string[] {  });

		// Uncomment if you are using Slate UI
		PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
		
		//add boost dependencies
		PublicIncludePaths.Add(Path.Combine(ThirdPartyPath, "BoostInternalLibrary", "includes"));
		PublicSystemIncludePaths.Add(Path.Combine(ThirdPartyPath, "BoostInternalLibrary", "includes"));

		bEnableExceptions = true;
		
		//for boost
		//bEnableUndefinedIdentifierWarnings = false;
		//bUseRTTI = true;
	}
}
