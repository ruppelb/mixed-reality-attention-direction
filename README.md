# Mixed Reality Attention Direction

This project contains a library for enabling a guidance of attention in mixed reality environments. In our project, we leverage the capabilities of the Varjo XR-3 to allow for attention detection based on hand and eye tracking data as well as attention direction in extended reality. The areas in which we want to detect attention can be automatically placed in a 3D world with the help of an object detection algorithm and depth data and video see-through images acquired with the Varjo XR-3. Within our work, we employed an object detection on analog flight instruments developed by another group. This project aims to support the OpenXR standard for platform independence and application on various head-mounted displays. Nevertheless, the automatic placement of areas of interest is only supported with the Varjo XR-3.

The executable version provided in this repository only works when the complete requirements are met.

## Structure
The whole project is structured in two parts: The Unreal Engine project part and the Varjo SDK part.  
In the first part, attention detection and direction are implemented. This is integrated with the OpenXR standard and can be executed on various devices. For attention detection, a device must support hand and eye tracking through OpenXR. The Unreal Project also contains a plugin used to make some of the Varjo XR-3's features available in Unreal Engine through OpenXR.  
The second part implements the process for automatically placing areas of interest. Because the Varjo SDK is utilized, this part of our project is only executable with Varjo devices and only the Varjo XR-3 is currently supported.  
The Varjo SDK part contains two separate Visual Studio projects. The MRAndDepthExport project accesses the video see-through frames and point cloud data from the Varjo XR-3, preprocesses the frames for object detection, generates normal and depth maps for each frame, and then sends the results to the ObjectDetection project. Latter receives the frames and feeds them to an object detection algorithm. The received pixel positions are deprojected to 3D space using the depth and normal maps and then handed to the Unreal Engine project part for placing virtual objects representing areas of interest at their corresponding real-world positions.  
Each part has different processes running simultaneously, and we employ shared memory IPC to transfer data between those.

## Concepts for attention direction
The concept of an area of interest is implemented in the _interactable_ C++ class within the Unreal Engine project. This supports eye and hand interactions and allows directing attention if attention is required, which can be determined by a period without attention set in a variable or automatically through external events. The areas of interest can be approximated with two kinds of meshes, a plane mesh or a cube mesh. Additionally, we allow visualizing the attention state with green color, meaning no attention is required and red, indicating that the user's attention is needed. We define two attention direction mechanisms and allow further definition through the _ADComponent_ class.  
The attention within the user's view can be directed to an interactable with an _ADHISMeshComponent_ attached to it. This enframes the from the HMD visible parts of the interactable when the attention is guided to it.  
The attention outside the user's view can be directed to an interactable when a class of _ADIndicator_ is set in a variable of the interactable. The indicator displays a 2D directional indicator from the HMD position to an interactable within the user's field of view.

## Requirements
In the following, the requirements for the library are listed. Things required for complete functionality (with the automatic placement of areas of interest) are printed in bold text.
### Execution
##### Hardware
- **Cuda capable GPU (NVIDIA)**
- **Varjo XR-3** or any other HMD
##### Software
- **Cuda**
- **Python Version 3.7.8 (Anaconda is not working at the moment)**
- **Varjo Runtime (usually installed with Varjo Base)**
- Microsoft Windows operating system

### Additional for Development
- CMake
- Unreal Engine 5
- Visual Studio 2022


## Installation
The following steps are required for every use case:
1. Download and install any software requirements mentioned above depending on your use case.
2. Clone this repository:
```
git clone https://gitlab.com/uofm-mixed-reality-flight-augmentation-system/mixed-reality-attention-direction.git
```
If installing for development, additionally perform the following tasks:

3. Go into the "Unreal Project" folder.
4. Either open the OpenXR_MRAD.uproject file or right-click on it, select "Generate Visual Studio project files", and following this, open and build the visual studio project.
5. Go back to the root folder and open the "varjo-sdk-experimental" folder.
6. Open CMake and configure and build the Varjo SDK project with the "projects" folder as the source folder and the "build" folder as the build folder.
7. Open the generated Visual Studio project, select the **release** configuration, and build the solution.
8. At last, go back to the root folder and install the python requirements found in the "requirements.txt" file.
```
cd mixed-reality-attention-direction
pip3 -install -r requirements.txt
```

## Usage
### Starting the application
How to start the application depends on the use case.
##### Execution
1. Go into the "Executable" folder.
2. Open the "Windows" folder and run the "OpenXR_MRAD.exe" file.
##### Development
Since the Unreal project automatically starts the executables of the Varjo SDK part projects, we only need to start to play in the Unreal Editor:
1. Open the Unreal Engine project.
2. Select a level from the "maps" folder.
3. Play the level preferably with VR preview.
### When the application is running
Several input events are configured when the application is running. Functions printed in bold text are only available when the Varjo XR-3 is employed.

- ESC - Quit game. It takes long period in the executable version until the application window closes.
- Left CTRL - Toggle display of the debug panel. On this panel, the status of different MR settings is displayed.
- H - Toggle the hand tracking visualization.
- G - Toggle the eye tracking visualization.
- **B - Toggle the mixed reality background.**  
- **T - Toggle the depth testing.**
- **E - Toggle the environment depth estimation.**
- **R - Toggle the depth testing range.**
- **Up - Increase the far plane of depth testing range.**
- **Down - Decrease the far plane of depth testing range.**
- **Left - Decrease the near plane of depth testing range.**
- **Right - Increase the near plane of depth testing range.**
- **I - Toggle the automatic placement of areas of interest based on objects detected by an object detection algorithm. In our case, analog flight instruments.**

### Within Unreal Editor
For development purposes and when the complete requirements are not met, areas of interest can be manually placed within the Unreal Editor. In the "interactables" folder, a blueprint base class of an interactable can be found, and a subclass of this can be created for defining a preset. These can be dragged into the level editor to spawn them and then be manually placed in the virtual world to fit in specific real-world locations.

## Examples

### Analog Flight Instruments
Sample pictures when running the application with complete requirements to place areas of instruments at detected analog flight instruments.  
- Before Placement:

    <img src="/assets/images/InstrumentsBeforePlacement.png" width="300" height="300" align="middle">

- After Placement with visualization:

    <img src="/assets/images/InstrumentsAfterPlacement.png" width="300" height="300" align="middle">

- After Placement during attention direction within view:

    <img src="/assets/images/InstrumentsAfterPlacementFrame.png" width="300" height="300" align="middle">

### MR settings Debug Panel
- Depiction of the debug panel where the MR settings are displayed:

    <img src="/assets/images/UEMRSettingsPanel.png" width="400" height="300" align="middle">
